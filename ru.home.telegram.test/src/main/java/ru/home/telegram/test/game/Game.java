package ru.home.telegram.test.game;

import java.util.Arrays;
import java.util.Date;
import java.util.List;


public abstract class Game extends TelegramService implements Persistable {

	protected final long Chat_Id;
	private Date expiryDate;
	protected boolean isFinished;
	protected int count;
	
	protected static List <String> exitWords = Arrays.asList(
			"выход", "exit", "закончить", "завершаем", "заканчиваем", "пока!", 
			"до свидания!", "стоп!", "stop!", "ВЫХОД ИЗ ИГРЫ  🙋");
	
	protected static List <String> rulesWords = Arrays.asList(
			"правила", "rules", "правила игры", "покажи правила", "show rules", "get rules", "правил", "условия", 
			"условия игры", "Правила   📚");
	
	
	abstract String initialMsg ();
	abstract String getRulesMsg ();
	
	public static Persistable getInstance(long chatId, String inputMsg)  {

		Persistable goldHunt = GoldHuntGame.getInstance(chatId, inputMsg);
		if (goldHunt!=null) return goldHunt;
		
		Persistable lingvoMania = LingvoManiaGame.getInstance(chatId, inputMsg, Dictionary.instance("Yandex"));
		if (lingvoMania!=null) return lingvoMania;
		
		// ... other games to add
		
		Persistable notSpecified = GameNotSpecified.getInstance(chatId, inputMsg);
		if (notSpecified!=null) return notSpecified;
		
		return null;
	}
	
		
	@Override
	public long getChatId () {
		return this.Chat_Id;
	};
	
	@Override
	public Date getExpiryDate () {
		return this.expiryDate;
	};
	
	@Override
	public boolean isFinished () {
		return this.isFinished;
	};
	
	protected Game (long chatId) {
		this.isFinished=false;
		this.Chat_Id = chatId;
		this.expiryDate = new Date(System.currentTimeMillis() + 10*60*1000);
	}

	
	protected String getGeneralMsg (String inputMsg) {
		
		this.expiryDate = new Date(System.currentTimeMillis() + 10*60*1000);
		
		if (this.count == 0) {
			count++;
			return this.initialMsg();
		}

		String msg = userEnds(inputMsg);
		if (msg != null)
			return msg;
		
		msg = userAskRules(inputMsg);
		if (msg != null)
			return msg;

		return null;
	} 
	

	protected String userAskRules (String inputMsg) {
		
		String msg="";
		
		// the user should provide the whole phrase to exit the game
		if (isCalled (inputMsg.toLowerCase(),rulesWords)) {
			msg+="ПРАВИЛА: "+"\n" + getRulesMsg();
				return msg;
		}
		return null;
	}
	
	// FINAL MESSAGE AND FINALISATION LOGIC

	
	protected String userEnds (String inputMsg) {
		
		String msg="";
		
		// the user should provide the whole phrase to exit the game
		if (isCalled (inputMsg.toLowerCase(),exitWords)) {
				msg += "Вы решили закончить игру. Увидимся в следующий раз!" + "\n";
				msg += fin();
				return msg;
		}
		return null;
	}
	
	protected String fin() {
		String finMsg ="Игра окончена.";
		this.isFinished=true;
		return finMsg;
	}
	
}
