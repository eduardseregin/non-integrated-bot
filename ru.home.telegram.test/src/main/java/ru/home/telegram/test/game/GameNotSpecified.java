package ru.home.telegram.test.game;

import java.util.Arrays;
import java.util.List;

public class GameNotSpecified extends Game implements Persistable {

	static List <String> classCalls = Arrays.asList("игр","play","gam", "игра", "играть", 
			"game", "gaming","gamble", "поиграем", "сыграем", "играю");

	
	public static GameNotSpecified getInstance (long chatId, String inputMsg) {
		if (isCalled(inputMsg, classCalls)) return new GameNotSpecified (chatId);
		return null;
	}
	

	protected GameNotSpecified(long chatId) {
		super(chatId);
		isFinished=true;
	}

	
	@Override
	public String getMsg(long chatId, String inputMsg) {
		String msg = getGeneralMsg (inputMsg);
		if (msg!=null) return msg;
		return null;
	}


	@Override
	String initialMsg() {
		String msg="";
		msg+="Доступные игры:" + "\n";
		msg+=" * Замок с приведениями (Напишите: 'замок' или 'привид')" + "\n";
		msg+=" * Лингвомания (Напишите: 'лингв' или 'мания')" + "\n";
	return msg;
	}
	
	
	protected String getRulesMsg () {
		return "";
	}
}
