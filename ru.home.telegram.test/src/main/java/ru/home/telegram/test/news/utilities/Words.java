package ru.home.telegram.test.news.utilities;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

// Utility for different operations with a String
public class Words {
	
	private Words () {
		throw new AssertionError ();
	}

	public static String onlyLettersNumbers (String text, char [] allowed) {
		
		if (text == null) return null;
		
		if (text.contains("#")) text = extractLine (text);
		
		List <Character> allow = new ArrayList <> (); allow.add (' ');
		if (allowed!=null) for (char c : allowed) allow.add (c);
		
		String result = "";
		
		for (int i = 0; i < text.length(); i++) {
			char chara = text.charAt(i);
			
			if (Character.isLetter(chara) || Character.isDigit(chara) || allow.contains (chara)) result+=""+chara;
			else result+=" ";
			
		}
		
			return result;
	}
	
	public static List <String> decomposeWords (String text) {
		
		if (text == null) return null;
	
		
		List <String> words = new ArrayList<>();
		
		while (text.contains(" ") && text.indexOf(" ") < text.length() - 1) {
			String word = text.substring(0, text.indexOf(" "));
			
			word = word.trim();
			
			if (word.length()>0)
				words.add(removeExtraDigits (word));
			
			text = text.substring(text.indexOf(" ") + 1);
			
		}
		text = text.trim();

		if (text.length() > 0)
			words.add(removeExtraDigits (text));	
		
		return words;
		
	}
	
	public static String extractLine(String line) { // removes indication to the language and reference to webpage

		if (line.contains("-#-"))
			line = line.substring(line.indexOf("-#-") + 3);
		if (line.contains("("))
			line = line.substring(0, line.lastIndexOf("("));

		return line;
	}
	
	public static boolean isDuplicating (String first, String second) { // checks with the default percent of duplication (60%)
		return isDuplicating (first, second, 75);
	}
	
	public static boolean isDuplicating (String first, String second, int percent) {
		if (first == null || second == null || percent < 20) return false;
		if (first.length () < 5 || second.length () < 5 ) return false;
		
		first = onlyLettersNumbers (first, null);
		second = onlyLettersNumbers (second, null);
		
		List <String> firstWords = decomposeWords (first);
		List <String> secondWords = decomposeWords (second);
		
		int count = 0;
		
		int length = Math.min (firstWords.size(),secondWords.size()) * 2;
		
		for (String f : firstWords) if (secondWords.contains (f)) count++;
		for (String s : secondWords) if (firstWords.contains (s)) count++;	
		
		if (count >= length * percent / 100) return true;
		return false;
	}
	
	
	private static String removeExtraDigits (String word) {
		
		List<Character> allowed = Arrays.asList( ' ', '.', ',', '(', ')', '%', '\\', '|', '/' , '\"', '$');
		
		boolean onlyDigits = true;
				
		for (int i = 0; i < word.length(); i++)			{
			
			if (i!=0 && ((word.charAt(i)=='-' || word.charAt(i)=='d' || word.charAt(i)=='t'))) {
				onlyDigits = true;
				return word;
			}
			
			if (!Character.isDigit(word.charAt(i)) && !allowed.contains(word.charAt(i))) {
				onlyDigits = false;	 
			}
			if (i>=6) onlyDigits = false;
		}
		
		if (onlyDigits) return word;
		
		
			String result = "";

		for (int i = 0; i < word.length(); i++)
			if (!Character.isDigit(word.charAt(i)))
				result += "" + word.charAt(i);

		return result;
	}
	

	public static boolean isContainedInLongLine (String string, Collection <String> list) {
		if (list==null) return false;
		if (list.size()==0) return false;
		
		String s = string.toLowerCase ();
		return list.stream().map(l->l.toLowerCase ()).anyMatch(l->s.contains(l));
		
	}
	
	
	public static boolean isEqualtoOneInWordInList (String s, List <String> list) {
		if (list==null) return false;
		if (list.size()==0) return false;
		
		return list.stream().map(l->l.toLowerCase ()).anyMatch(l->l.equals(s.toLowerCase ()));
		
	}
	
	@SuppressWarnings("unlikely-arg-type")
	public static boolean isEqualsCompareWords (List <String> words, List <String> list) {
		if (list==null) return false;
		if (list.size()==0) return false;
		
	
		for (int i = 0; i < Math.max (words.size(), list.size()); i++) {
			
			String word = words.get (Math.min(i,words.size()-1)).toLowerCase ();
			String li = list.get (Math.min(i,list.size()-1)).toLowerCase ();
		
			if (words.equals(li) || list.equals(word)) {
				return true;
			}
		}
		
		return false;
	}
	
	public static boolean isDuplicatingInList (String s, List <String> list) {
		
		return list.stream().anyMatch(l->Words.isDuplicating (s,l));
		
	}
	
	public static int wordsCount (String s) {
		if (s.contains("#")) s = extractLine(s);
		s = onlyLettersNumbers(s, null);
		
		return decomposeWords(s).stream().distinct().filter (x->x.length()>1).collect(Collectors.counting()).intValue();
		
	}
	
	public static int parseInt (String s) {
		int result = 0;
		try {
			result = Integer.parseInt(s);			
		} catch (Exception e) {
			
		}
		return result;
	}
}

