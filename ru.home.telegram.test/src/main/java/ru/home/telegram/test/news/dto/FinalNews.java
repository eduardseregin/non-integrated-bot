package ru.home.telegram.test.news.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import ru.home.telegram.test.news.NewsService;
import ru.home.telegram.test.news.utilities.Disk;
import ru.home.telegram.test.news.utilities.DiskPaths.ReadWrite;
import ru.home.telegram.test.news.utilities.Words;



public class FinalNews implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static List <FinalNews> finalNews = SaveLoadFinalList.load ();
	
	private int orderNumber;
	private String main;
	private String urlWithBrackets;
	
	
	public List <String> getNews () {
		
		if (finalNews ==null) return null;
		if (finalNews.size()==0) return null;
		
		int limit = 1; 
		int count = 1;
		while (finalNews.get(count).getOrderNumber()==limit+1) {
			limit++;
			count++;
		}
		
		return finalNews.stream().limit(limit).map (x->x.toString ()).collect(Collectors.toList());
	}
	
	public static void addNews (List <NewsRate> newsRates, int numberOfNews) {
		
		if (newsRates!=null && numberOfNews!=0) {

			Disk.saveLines(ReadWrite.LOGS_SECOND, newsRates.stream().sorted ().map(x->x.toString()).collect(Collectors.toList()));
			System.out.println("News before checking for distinct: " + newsRates.size()  + " pcs.");
			// ensures news are sorted based on ratings in NewsRates
			newsRates = newsRates.stream ().sorted ().distinct ().collect (Collectors.toList()); 
			
			// converts newsRates into finalNews, checks for duplication with the previous news and after limits the news
			List <FinalNews> probe = newsRates.stream()
					.map(x->new FinalNews (x))
				//	.filter(e -> (finalNews.stream().filter(d -> d.equals(e)).count()) < 1)
					.collect(Collectors.toList());
			
			if (probe.size ()<numberOfNews) {
			  numberOfNews = probe.size ();
			  System.out.println("LIST IS NOT COMPLETE!!! Only " + numberOfNews + " news has been formed");
		  }
		
			List <FinalNews> checkForDuplication = probe;
			checkForDuplication.addAll(finalNews);
			
			checkForDuplication = checkForDuplication.stream()
					//.distinct()
					.filter(x->x.orderNumber==0)
					.limit(numberOfNews)
					.collect(Collectors.toList());
			
			List <FinalNews> formingNews = new ArrayList <>();
			
			formingNews.addAll (checkForDuplication);
			// sets correct numbering to fresh news
			IntStream.range(0, numberOfNews).forEach (i->formingNews.get(i).setOrderNumber(i+1)); 
			
			formingNews.addAll(finalNews);
			finalNews = formingNews;
			
			SaveLoadFinalList.save();
			
			System.out.println("-----------------");
			System.out.println("NEWS UPDATE IS FINALISED");
			System.out.println("-----------------");
			
			System.out.println("Saved news: +" + numberOfNews + " pcs.");
			finalNews.stream()
					.limit(numberOfNews/3)
					.forEach(x->System.out.println(x));
			
		}

		NewsService.reset();
		
	}
	
	public FinalNews () {}
	
	private FinalNews(NewsRate newsRate) {
		
		String newsLine = newsRate.getLine();
		
		this.orderNumber = 0;
		this.main = Words.extractLine (newsLine);
		this.urlWithBrackets = newsLine.substring(newsLine.lastIndexOf("("));
	}
	
	private FinalNews(int orderNumber, String main, String url) {
		
		this.orderNumber = orderNumber;
		this.main = main;
		this.urlWithBrackets = url;
	}
	
	
	
	public int getOrderNumber() {
		return orderNumber;
	}
	

	protected void setOrderNumber(int orderNumber) {
		this.orderNumber = orderNumber;
	}
	

	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((main == null) ? 0 : main.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FinalNews other = (FinalNews) obj;
		if (main == null) {
			if (other.main != null)
				return false;
		} else if (!Words.isDuplicating(this.main, other.main))
			return false;
		return true;
	}

	@Override
	public String toString () {
		return orderNumber + ". " + main + urlWithBrackets;
	}
	
	
	static class SaveLoadFinalList {

	private static List <FinalNews> load () {
		List <FinalNews> result = new ArrayList <>();
		List<String> lines = Disk.loadLines (ReadWrite.NEWS);
		
		for (String newsLine : lines ) {
			result.add(instance (newsLine));
		}
		
		return result;
	}
	
	private static void save () {
		Disk.saveLines (ReadWrite.NEWS, finalNews.stream().map(x->x.toString()).collect(Collectors.toList()));
	}

	private static FinalNews instance (String fromFile) {
		
	if (!fromFile.contains(".")) return null;
	
	String number = "";
	for (int i=0; i<fromFile.indexOf(".");i++) {
		if (Character.isDigit(fromFile.charAt(i))) number+="" + fromFile.charAt(i);
	}
	int num=0;
	if (number.length()>0) num = Integer.parseInt(number);
	
	String mainElement = fromFile.substring(fromFile.indexOf(".")+2, fromFile.lastIndexOf("("));
	String url = fromFile.substring(fromFile.lastIndexOf("("));
	
	return new FinalNews (num, mainElement, url);
}
	}
}
