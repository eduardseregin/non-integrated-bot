package ru.home.telegram.test.game;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class SaveScore implements ActionListener {

	final static String SCORE_FILE = Score.SCORE_FILE;
	final static long SAVE_SEPARATOR = Score.SAVE_SEPARATOR;

	private Map <Long, Score> playersScore;
	private long globalPlayed;
	private long globalAnswered;
	private long globalTwoAttempt;
	private long globalFourAttempt;
	
	
	
	public SaveScore (Map<Long, Score> playersScore, long globalPlayed, 
			long globalAnswered, long globalTwoAttempt, long globalFourAttempt) {

		this.playersScore = playersScore;
		this.globalPlayed = globalPlayed;
		this.globalAnswered = globalAnswered;
		this.globalTwoAttempt = globalTwoAttempt;
		this.globalFourAttempt = globalFourAttempt;
	}


	@Override
	public void actionPerformed(ActionEvent e) {

				 
		
				List<Long> listAll = new ArrayList<>();
				
				listAll.add(SAVE_SEPARATOR); 
				listAll.add(globalPlayed);
				listAll.add(globalAnswered);
				listAll.add(globalTwoAttempt);
				listAll.add(globalFourAttempt);
				listAll.add(SAVE_SEPARATOR);
				
				Set <Long> keys = playersScore.keySet();
				
				for (long k : keys) {
					Score s = playersScore.get(k);
					listAll.add(s.getChat_Id());
					listAll.add(s.getTotalPlayed());
					listAll.add(s.getTotalAnswered ());
					listAll.add(s.getTwoAttemptAnswered());
					listAll.add(s.getFourAttemptAnswered());
					
					for (long l : s.getAnswered()) {
						listAll.add(l);
					}
					listAll.add(SAVE_SEPARATOR);
				}
				// now list for recording is prepared
				
				
				System.out.println("Saved score successfully: "+saveToFile (listAll, SCORE_FILE));
			}
			
			
			private static boolean saveToFile (List <Long> allList, String path)  {
				
				if (allList==null) return false;
				if (allList.size()<6) return false;
				boolean isOk=false;
				
				try {
				try (FileOutputStream fos = new FileOutputStream(path); 
						ObjectOutputStream oos = new ObjectOutputStream(fos)) {
					
					for (long l : allList) {
						oos.writeLong (l); 
					}
					oos.close();	isOk=true;
				}} catch (Exception e) {}
				
				return isOk;
			}
			
}
