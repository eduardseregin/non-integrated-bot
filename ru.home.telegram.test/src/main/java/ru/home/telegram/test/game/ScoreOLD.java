package ru.home.telegram.test.game;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;


public class ScoreOLD {

	static final long SAVE_SEPARATOR = 99999;
	static final String SCORE_FILE = "C:\\Temp\\LingvomaniaScore.txt";
	static Date nextWritingtime;
	static Date maxWritingtime;
	static boolean timerNotStarted = true;
	
	static Map <Long, ScoreOLD> playersScore;
	static long globalPlayed;
	static long globalAnswered;
	static long globalTwoAttempt;
	static long globalFourAttempt;
	
	
	
	private final long Chat_Id;
	private Set <Long> answered;
	private long totalPlayed;
	private long totalAnwered;
	private long twoAttemptAnswered;
	private long fourAttemptAnswered;
	
	
	public static ScoreOLD instance (long chatId) {
		if (playersScore == null) {
			boolean notDownloaded=true;
try {
			downloadScore (SCORE_FILE);
notDownloaded=false;
} catch (FileNotFoundException e) {
e.printStackTrace();
} catch (IOException e) {
e.printStackTrace();
}
			if (notDownloaded) playersScore=new HashMap<>();
		}
		ScoreOLD sc = playersScore.get(chatId);
		if (sc==null) sc = new ScoreOLD (chatId);
		return sc;
	}
	
	private ScoreOLD (long chatId) {
		Chat_Id = chatId;
		this.answered = new TreeSet<>();
		this.totalPlayed = 0;
		this.totalAnwered = 0;
		this.twoAttemptAnswered = 0;
		this.fourAttemptAnswered = 0;
		playersScore.put(chatId,this);
		
	}
	
	public String getScoreMsg () {
		String msg="";
		msg+="Правильных ответов: " + perCent (totalAnwered, totalPlayed) +"%,"+"\n";
		msg+=" в т.ч. с первых двух попыток: " + perCent (twoAttemptAnswered, totalPlayed) +"%,"+"\n";
		msg+=" и с первых четырёх попыток: ещё  " + perCent (fourAttemptAnswered, totalPlayed) +"%."+"\n";
	
		
		if (globalPlayed<5 || totalPlayed+2>=globalPlayed) {msg+=" ** Глобальный рейтинг ещё не сформирован. Ваше место в нём будет понятно чуть позже";}
		
		else {
		
			msg+=" ** В рейтинге Вы ";
			int rating = getPercentToGlobal ();
		
			if (rating > 0 ) msg+="на "+rating + "% выше других участников. "+praisalWords();
			if (rating < 0 ) msg+="на "+Math.abs(rating) + "% не дотягиваете до других участников." +"\n"+ encourageWords();;
			if (rating == 0 ) msg+="отвечаете одинаково с другими участниками. " + equalityWords();
		
		}
		return msg;
	}
	

	public void addResults (String indAnsString, int count) {
		
		int indexAnswered = parseIndex(indAnsString);
		
		if (indexAnswered>=0) answered.add((long) indexAnswered);
		
		globalPlayed++; totalPlayed++; 
		
		if (count>0) {
			globalAnswered++; totalAnwered++;
			if (count <=2) {globalTwoAttempt++;twoAttemptAnswered++;}
			else if (count<=4) {globalFourAttempt++;fourAttemptAnswered++;}
		}

		nextWritingtime = new Date(System.currentTimeMillis() + 15*60*1000);
		if (maxWritingtime==null) {maxWritingtime = new Date(System.currentTimeMillis() + 30*60*1000);}
		if (timerNotStarted) { timerNotStarted = false; startSaveTimer ();}
	}
	


	public void clearScore () {
		totalPlayed=0;
		totalAnwered=0;
		twoAttemptAnswered=0;
		fourAttemptAnswered=0;
	}
	
	public boolean isAlreadyAnswered (int index, int sizeOfDictionary) {

		// int index=parseIndex (indexString);
		
		if (index==-1) return false;
		
		if (answered.size()+60>sizeOfDictionary) {answered = new TreeSet<> (); return false; }
		return answered.stream().map (x->(long) x).anyMatch(x->x == index);
		
	}
	
	private int parseIndex (String indexString) {
		int ind=-1;
		try {ind=Integer.parseInt(indexString);}
		catch (Exception e) {}
		
		return ind;
	}
	

	// 70%  20% 30% = 60 + 60 + 70 = 190
	// 65%  30% 20% = 90 + 40 + 65 = 195
	
 private int getPercentToGlobal () {
		long globalScore = 3 * perCent (globalTwoAttempt, globalPlayed) + 2 * perCent (globalFourAttempt, globalPlayed)+perCent (globalAnswered, globalPlayed);
		long localScore = 3 * perCent (twoAttemptAnswered, totalPlayed) + 2 * perCent (fourAttemptAnswered, totalPlayed) + perCent (totalAnwered, totalPlayed);
		
		
		long result = localScore - globalScore;
		
		return perCent (result, globalScore);
	}
	
	private int perCent (long portion, long total) {
		if (total==0) return 0;
		int percent = (int) (portion * 20 / total);
		return Math.round(percent)*5;
	}

	// 99999 GP GA GTA GFA 99999 ChatId tP TA tAA fAA answered1 ... answeredN 99999 ChatId2... 
	private static boolean downloadScore (String path) throws FileNotFoundException, IOException {
		
		File f = new File(path);
		if (!f.exists()) {setGlobals (); return false;}
	
		List<Long> listAll = new ArrayList<>();
				
			try (FileInputStream istream = new FileInputStream(f)) {
				try (ObjectInputStream ois = new ObjectInputStream(istream)) {
					while (ois.available() > 0) {
						listAll.add(ois.readLong());
					}
					ois.close();
				}
				istream.close();
			}
		
			
			// Global level
			if (listAll.size()<6 || listAll.get(0)!=SAVE_SEPARATOR) {setGlobals (); return false;}
			setGlobals (listAll.get(1),listAll.get(2),listAll.get(3),listAll.get(4));
			
			
			// Players level
			
			int indSepar = 5;
			int pieceInd = 0;
			
		while (indSepar+1<listAll.size()) {
			ScoreOLD player=null;

			for (int i=indSepar + 1; i<listAll.size();i++ ) {
				if (listAll.get(i) == SAVE_SEPARATOR) {indSepar=i; pieceInd = 0; break;}
				if (pieceInd==0) {
					System.out.println("Creating player: " + listAll.get(i));
					player=new ScoreOLD (listAll.get(i));}
				
				
					if (pieceInd==1) {player.totalPlayed=listAll.get(i);}
					if (pieceInd==2) {player.totalAnwered=listAll.get(i);}
					if (pieceInd==3) {player.twoAttemptAnswered=listAll.get(i);}
					if (pieceInd==4) {player.fourAttemptAnswered=listAll.get(i);}
					
					if (pieceInd>4) {player.answered.add(listAll.get(i));}
					pieceInd++;
				
			}
		}
		
		
		return true;
	}
	
	// 99999 GP GA GTA GFA 99999 ChatId tP TA tAA fAA answered1 ... answeredN 99999 ChatId2... 
	private static boolean saveResults(String path) throws FileNotFoundException, IOException {
		// Prepares list to write
		
		List<Long> listAll = new ArrayList<>();
		
		listAll.add(SAVE_SEPARATOR); 
		listAll.add(globalPlayed);
		listAll.add(globalAnswered);
		listAll.add(globalTwoAttempt);
		listAll.add(globalFourAttempt);
		listAll.add(SAVE_SEPARATOR);
		
		Set <Long> keys = playersScore.keySet();
		
		for (long k : keys) {
			ScoreOLD s = playersScore.get(k);
			listAll.add(s.Chat_Id);
			listAll.add(s.totalPlayed);
			listAll.add(s.totalAnwered);
			listAll.add(s.twoAttemptAnswered);
			listAll.add(s.fourAttemptAnswered);
			
			for (long l : s.answered) {
				listAll.add(l);
			}
			listAll.add(SAVE_SEPARATOR);
		}
		// now list for recording is prepared
		
		
		return saveToFile (listAll, path);
	}
	
	private static void setGlobals (long played, long answered, long twoAttempt, long fourAttempt) {
	
		playersScore = new HashMap<>();
		globalPlayed= played;
		globalAnswered= answered;
		globalTwoAttempt= twoAttempt; 
		globalFourAttempt= fourAttempt;
		
	}
	
	
	private static void setGlobals () {
		
		playersScore = new HashMap<>();
		globalPlayed= 0;
		globalAnswered= 0;
		globalTwoAttempt= 0; 
		globalFourAttempt= 0;
		
	}
	
	private static boolean saveToFile (List <Long> allList, String path) throws FileNotFoundException, IOException {
		
		if (allList==null) return false;
		if (allList.size()<6) return false;
		boolean isOk=false;
		try (FileOutputStream fos = new FileOutputStream(path); 
				ObjectOutputStream oos = new ObjectOutputStream(fos)) {
			
			for (long l : allList) {
				oos.writeLong (l); 
			}
			oos.close();	isOk=true;
		}
		
		return isOk;
	}
	
	private String praisalWords () {
		
		List <String> words = Arrays.asList("ЗДОРОВО!", "ОТЛИЧНО!", "СУПЕР!", 
				"ТАК ДЕРЖАТЬ!", "ВОСХИТИТЕЛЬНО!", "Это просто супер!", "Вот это результат!",
				"Вас сложно догнать!", "Впечатляет!", "Вы большой молодец!", "Здорово!!!", "Супер!!", "Белиссимо!");
		
		return words.get((int) (Math.random()*words.size()));
		
	}
	
	
	
	private String encourageWords () {
		
		List <String> words = Arrays.asList("НЕПЛОХО, НО ЕСТЬ КУДА СТРЕМИТЬСЯ!!", "Догоните?!", "Впечатлите в следующий раз?!", 
				"Победа - не главное... ИЛИ?!", "ЕЩЕ НЕ ВЕЧЕР!", "ЕЩЁ НАГОНИМ!", "Ваш результат - впереди!",
				"Впечатлим в следующем раунде?!", "Не в словах - счастье! :)", "Форте, дамы и господа!");
		
		return words.get((int) (Math.random()*words.size()));
		
	}

	private String equalityWords () {
		
		List <String> words = Arrays.asList("В НОГУ!", "Великолепно!", "Лучше не бывает... Или?!", 
				"Отличный результат!", "Вполне достойно!");
		
		return words.get((int) (Math.random()*words.size()));
		
	}
	
	private static void waitAndSave () throws InterruptedException {
		if (nextWritingtime!=null && maxWritingtime!=null) {
			Date now = new Date ();
			boolean isFirst=true;
			
			while (now.compareTo(nextWritingtime)<0 && now.compareTo(maxWritingtime)<0) {
				if (isFirst) {System.out.println(nextWritingtime + " && " + maxWritingtime); isFirst=false;}
				Thread.sleep(6*1000);
				now=new Date ();

			}
			
			try {
				saveResults (SCORE_FILE);
			} catch (FileNotFoundException e) {

				e.printStackTrace();

			} catch (IOException e) {

				e.printStackTrace();
			}
			
			System.out.println("Saved Scores to file");
			timerNotStarted=true;
			nextWritingtime=null;
			maxWritingtime=null;
		}
	}
	
	private static void startSaveTimer () {
	Runnable task = () -> {try {
		waitAndSave ();
	} catch (InterruptedException e) {
		
		e.printStackTrace();
	}};
	Thread thread = new Thread(task);
	thread.start();
	}
}
