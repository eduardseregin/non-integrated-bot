package ru.home.telegram.test.game;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public abstract class Dictionary {

	enum Word {
		LineNumber, First, Second, Third, Fourth, Hint_Lat, Hint_Cyrilic, English, Russian, FirstSyn,
		SecondSyn, ThirdSyn,FourthSyn
	}
	
	protected static Dictionary instance;
	

	public static Dictionary instance (String name)  {
		if (name!=null) if (name=="Yandex") {
			if (instance==null) {instance = new DictionaryYandexCategory ();}
			return instance;}
		return null;
	}
	
	public abstract String getDescription ();
	public abstract Map <Word, String> getSample (int line);
	public abstract int getDictSize ();
	public abstract List <String> getCategories ();
	
	public String getEnglishDef (String text) throws IOException {
		
		String result = "Определение недоступно";
		
		try {
		String url = "https://dictionary.cambridge.org/ru/%D1%81%D0%BB%D0%BE%D0%B2%D0%B0%D1%80%D1%8C/%D0%B0%D0%BD%D0%B3%D0%BB%D0%B8%D0%B9%D1%81%D0%BA%D0%B8%D0%B9/";
		
		Document doc = Jsoup.connect(url +text).ignoreContentType(true).get(); 
		Elements metaTags = doc.getElementsByTag("meta");
		result = metaTags.stream().map(x->x.attr("content")).filter(x->x.contains(text)).findFirst().orElse("Not found");
		if (result.contains("Узнать больше"))
		result = result.substring(result.indexOf(".")+2, result.indexOf("Узнать больше"));
		}
		catch (Exception e) {}
		
		return result;
	}
	
	public List<String> getRuSynonyms(String word) {

		List<String> synonims = new ArrayList<>();

		try {
			String url = "https://text.ru/synonym/" + word;
			Document doc = Jsoup.connect(url).get();

			Elements metaTags = doc.getElementsByTag("meta");

			String content = "";
			for (Element metaTag : metaTags) {
			  String name = metaTag.attr("name");
			  if (name.contains("description"))
			  content += metaTag.attr("content");
			  }

			if (content.contains(" — ")) content = content.substring(content.indexOf(" — ")+3); 
			if (content.contains(":")) content = content.substring(content.indexOf(":")+2);
			
			while (content.contains(" — ") ) {
				String nextWord = "";
				nextWord = content.substring(0,content.indexOf(" — "));
				content = content.substring(content.indexOf(" — ")+3);

				if (isEndingOk (nextWord)) synonims.add(nextWord);
				
				}
		
			content = content.substring(0, content.indexOf("."));
			
			while (content.contains(",")) {
				String nextWord = "";
				nextWord = content.substring(0,content.indexOf(","));
				content = content.substring(content.indexOf(",")+2);

				if (isEndingOk (nextWord)) synonims.add(nextWord);
			}

			if (isEndingOk(content)) synonims.add(content);
		
		} catch (Exception e) {		}

		return synonims;
	}
	
	private boolean isEndingOk (String word) {
		List<String> wrongEndings = Arrays.asList("й", "ся", "ть");
		
		for (String wE : wrongEndings) {
			if (word.endsWith(wE)) return false;}
		return true;
	}
	
//	public List<String> getRuSynonymsOLD (String word) {
//
//		List<String> wrongEndings = Arrays.asList("й", "ся", "ть");
//		List<String> synonims = new ArrayList<>();
//
//		try {
//			String url = "https://sinonimus.ru/sinonim_k_slovy_" + word;
//			Document doc = Jsoup.connect(url).get();
//
//			Elements list = doc.select(".text").first().select("section").first().select("ol").first().select("li");
//
//			for (Element e : list) {
//				String nextWord = e.text().toLowerCase();
//
//				boolean isOk = true;
//				for (String wE : wrongEndings) {
//					if (nextWord.endsWith(wE))
//						isOk = false;
//				}
//
//				if (isOk && !synonims.contains(nextWord))
//					synonims.add(nextWord);
//			}
//		} catch (Exception e) {		}
//
//		return synonims;
//	}
}
