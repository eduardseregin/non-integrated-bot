package ru.home.telegram.test.game;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import ru.home.telegram.test.game.Dictionary.Word;

public class LingvoManiaGame extends Game {

	private Dictionary dictionaryClass;
	private Map<Word, String> currentSample;
	private Score score;
	private Word guessType;
	private List <String> synonims;
	private List <String> categories;
	private String category;
	
	static List <String> classCalls = Arrays.asList("мани", "линг", "mani", "lingv", "лингва",
			"слов","word", "иностр", "foreig");
	
	
	public static LingvoManiaGame getInstance (long chatId, String inputMsg, Dictionary dictionary) {

		if (isCalled(inputMsg, classCalls)) return new LingvoManiaGame (chatId, dictionary);
		return null;
	}
	
		
	private LingvoManiaGame(long chatId, Dictionary dictionaryClass) {
		super(chatId);
		this.dictionaryClass=dictionaryClass;
		this.categories=dictionaryClass.getCategories();
		this.score=Score.instance(chatId);
		this.currentSample=getNextSample ();
		this.guessType=Word.Russian;
		this.synonims=new ArrayList <> ();
	}

	// plus synonims
	private Map<Word, String> getNextSample () {

		int line=0;
		while (true) {
			line = (int) (Math.random() * dictionaryClass.getDictSize());
			if (!score.isAlreadyAnswered(line, dictionaryClass.getDictSize())) break;
			
		}	
		Map<Word, String> attmpt = dictionaryClass.getSample(line);
		if (guessType==Word.Russian) synonims=dictionaryClass.getRuSynonyms(attmpt.get(Word.Russian));
		category="";
		if (categories!=null) if (categories.size()>line) {category = categories.get(line);}
		
		return attmpt;
	}


	@Override
	public String getMsg(long chatId, String inputMsg) {
		
		System.out.println("Monitoring count: " + count);
		
		String msg = getGeneralMsg (inputMsg);
		if (msg!=null) return msg;

		inputMsg=inputMsg.toLowerCase();
		if (inputMsg.contains("50") || inputMsg.equals("мой счёт 50   🧮")) {return score.getScoreMsg();}
		
		if (inputMsg.contains("100")) {
				msg="Вы приняли решение сменить язык отгадываемого слова с ";
				if (guessType==Word.Russian) {guessType=Word.English; msg+="русского на английский язык." + "\n";}
				else {guessType=Word.Russian; msg+="английского на русский язык." + "\n";}
				msg+=getFirstWords();
				return msg;
		}
		
		if (inputMsg.contains("сброс")) {score.clearScore(); return "Ваши текущие результаты (счёт) обнулёны по Вашей просьбе";}
		
		String inLangSyn = getInLangSyn(inputMsg);
		if (inLangSyn!=null) return inLangSyn;
		
		String correctWord = currentSample.get(guessType).toLowerCase(); 
		if (inputMsg.contains(correctWord)) {
			return localWinMsg ();
		}
		
		if (count>=5) return localLostMsg ();
		
		msg = wrongMsg (inputMsg,correctWord);
				//"Не совсем: Попробуйте ещё раз"+"\n";
		
		if (getHint ()!=null) msg+=getHint();
		count++;
		if (msg==null) System.out.println("Im in LingvoMania - > gets null message");
		return msg;
	}

	private String localWinMsg () {
		String msg="";
		msg+="Поздравляем, ответ '" + currentSample.get(guessType)+"' - правильный!" +"\n" +"Решено с";
		if (count==2) msg+="о";
		msg+=" " + count+"-го хода!" + "\n"+"\n";
		score.addResults(currentSample.get(Word.LineNumber), count);
		msg+=getFirstWords();
		return msg;
	}
	
	private String localLostMsg () {
		String msg="";
		msg+="В этот раз не получилось. "+"\n"+"Правильный ответ: '" + currentSample.get(guessType)+"'. "
				+ "Попробуйте ещё раз!"+"\n";
		score.addResults(currentSample.get(Word.LineNumber), 0);
		msg+=getFirstWords();
		return msg;
	}
	
	private String getInLangSyn (String inputMsg) {
		
		if (inputMsg.contains("100")) return null;
		if (!inputMsg.contains("1") && !inputMsg.contains("2") && !inputMsg.contains("3") && !inputMsg.contains("4")) return null;
		
		String msg = " Подсказка - синоним на запрошенном иностранном языке: " + "\n";
			   msg+= "   >>>> " + inputMsg + ". "+"\n";
		if (inputMsg.contains("1")) msg+=currentSample.get(Word.FirstSyn);
		if (inputMsg.contains("2")) msg+=currentSample.get(Word.SecondSyn);
		if (inputMsg.contains("3")) msg+=currentSample.get(Word.ThirdSyn);
		if (inputMsg.contains("4")) msg+=currentSample.get(Word.FourthSyn);
		msg+=".\n";
		
		if (msg.contains("недоступен")) {
			String availableOthers="";
			if (!currentSample.get(Word.FirstSyn).contains("синоним_недоступен") && !inputMsg.contains("1")) availableOthers+="1,";
			if (!currentSample.get(Word.SecondSyn).contains("синоним_недоступен") && !inputMsg.contains("2")) availableOthers+="2,";
			if (!currentSample.get(Word.ThirdSyn).contains("синоним_недоступен") && !inputMsg.contains("3")) availableOthers+="3,";
			if (!currentSample.get(Word.FirstSyn).contains("синоним_недоступен") && !inputMsg.contains("4")) availableOthers+="4,";
			
			if (availableOthers!="") {
				availableOthers=availableOthers.substring(0, availableOthers.lastIndexOf(","));
				msg+="Доступные варианты: " + availableOthers +"\n";
			}
			else msg+="К сожалению, в этот раз нет доступных синонимов ни на одном указанном языке."+"\n";
			
		}
		
		return msg;
	}

	
	private String getHint () {
	
		if (count == 1) {return getWordPattern (1);}
		if (count == 2) {return getWordPattern (2) +"\n" + "Подсказка 1: " + currentSample.get(Word.Hint_Lat)+"\n";}
		if (count == 3) {return  getEnglishDef() + getWordPattern (3);}
		if (count == 4) {
			
			if (guessType==Word.Russian)
			return getWordPattern (4) +"\n" + "Последняя подсказка: " + "\n" + currentSample.get(Word.Hint_Cyrilic)+".\n" + "Английское слово: " + currentSample.get(Word.English)+".\n";
			else return getWordPattern (4) +"\n" + "Последняя подсказка: " + "\n" + currentSample.get(Word.Hint_Cyrilic)+"\n" + "Русское слово: " + currentSample.get(Word.Russian)+".\n";
		}
			
		return null;
	}
	
	private String getWordPattern (int qty) {
		
		// 1 - middle letter; 2 - last letter; 3 - 1st letter; 4 - second letter
		
		String word = currentSample.get(guessType);
		String msg="";
		int indMiddle = word.length()/2;
		
		int startIndex = 0;
		
		if (qty>=3) {msg+=word.charAt(0)+" "; startIndex++;}; 
		if (qty>=4) {msg+=word.charAt(1)+" "; startIndex++;}; 
		
		for (int i = startIndex; i<word.length(); i++) {
			boolean notOpenLetter = true; 
			if (qty>0 && i==indMiddle) {msg+=word.charAt(indMiddle)+" "; notOpenLetter=false;}
			if (qty>=2 && i==word.length()-1) {msg+=word.charAt(word.length()-1)+" "; notOpenLetter=false;}
			if (notOpenLetter) msg+="_ ";
		}
		msg+="\n";
		return msg;
	}
	
	@Override
	String initialMsg() {
		String string = "";
		string+="ИГРА 'ЛИНГВОМАНИЯ!' Задача - угадать слово, по его переводу "+"\n";
		string+="   / Игра реализована с помощью сервиса «API «Яндекс.Словарь»   /"+"\n"
				+ "/(http://api.yandex.ru/dictionary);                          /"+"\n";
		string+=" / подсказка в виде части определения слов на английском языке/"+"\n"
			  + " |реализована с помощью"+"\n                                  |"+
		        "/кембриджского словаря определений: dictionary.cambridge.org; |"+"\n";
		string+="|синонимы к слову на русском языке приводятся по источнику:   |"
			  + "|                                           текст.рф (text.ru)|."+"\n";
		string+="   --------------------------------------------------------"+"\n";
		string+="ИГРАЕТЕ В ПЕРВЫЙ РАЗ? ---> Введите 'Правила', чтобы прочитать их"+"\n"+"\n";
		
		string+=guessLangSpecific();
		
		string+="!!! Напишите 'Выход', если закончили !!! "+"\n"+"\n";
		string+=" - > Посмотреть Ваши успехи (счет) - запросите 50.  "+"\n"+"\n";
		
		string+=getFirstWords();
		
		return string;
	}

private String guessLangSpecific () {

	String string="";
	if (guessType==Word.Russian) {
	string+="Вводите буквы русского слова в качестве ответа."+"\n";
	string+="Введите 100, чтобы отгадывать английское слово."+"\n";}
	
	else {
		string+="Вводите буквы английского слова в качестве ответа."+"\n";
		string+="Введите 100, чтобы отгадывать русское слово."+"\n";}
	return string;
}
	
protected String getRulesMsg () {
		
		String string="";
		string+=dictionaryClass.getDescription() +"\n";
		string+="Загаданные слова относятся к категориями: Жизненные события, Люди (профессии и статусы), " + "\n"
				+ "Природа и окружение, Институты и экономика, Юриспруденция и Техника."
				+ "\n";
		string+="На втором ходе будет подсказка по количеству букв и откроется средняя буква."+"\n";
		string+="На третьем - Подсказка 1, откроется последняя буква. На четвертом - " + "\n"
				+ "откроется первая буква и появится определение на английском языке."+"\n";
		string+= "На последнем ходе - Подсказка 2."+"\n";
		string+="Если после этого Вы не угадаете слово - раунд будет проигран."+"\n";
		string+=" >>> Программа проверяет синонимы (подсказывает, но правильным ответом не является), " + "\n"
				+ "а также совпадение в ответе 3х и более последовательных букв (т.е. целесообразно давать даже неточные ответы)."+"\n";
		string+="Если значение слова не удается угадать, можно написать 'не знаю', но лучше дать хоть какой-то вариант!" + "\n";
		string+="**ПОДСЧЁТ БАЛЛОВ:**"+"\n";
		string+="> Счёт показывает процент успешно угаданных слов, и в т.ч. со второй или четвертой попытки."+"\n";
		string+=" >> При сравнении с другими игроками - угаданные со второй попытки слова "+"\n";
		string+="    принесут в 3 раза больше, а с четвертой - в 2 раза больше баллов, чем с пятой. " + "\n"
				+ "Неугаданные слова баллов не дают."+"\n";
		string+=" - >>> Обнулить Ваши результаты (счет) - запросите Сброс. Удачи!!! "+"\n"+"\n";
		return string;
	}

private String getFirstWords () {
	
	this.currentSample=getNextSample ();
	count=1;
	
	String msg="";
	
	msg+="Какое это слово, если на иностранных языках оно звучит следующим образом (ответ - существительное):"+"\n";
	msg+=" 1. " + currentSample.get(Word.First) + "\n";
	msg+=" 2. " + currentSample.get(Word.Second) + "\n";
	msg+=" 3. " + currentSample.get(Word.Third) + "\n";
	msg+=" 4. " + currentSample.get(Word.Fourth) + "\n";
	if (category!="") msg+="\n "+"Слово относится к следующей теме: " + category + ".\n";
	
	return msg;
}	

private String getEnglishDef () {
	
	String msg="";
	
	try {
		msg+="Данное слово определяется в кембриджском словаре следующим образом (начало определения):" + "\n";
		msg+=dictionaryClass.getEnglishDef(currentSample.get(Word.English))+"\n"+"\n";
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	return msg;
}

private String wrongMsg (String inputMsg, String guessWord) {
	
	if (synonims.contains(inputMsg.toLowerCase())) return "Вы угадали значение. Подумайте над синонимами: ответ - другой синоним." + "\n";
	
	int numbSimilar = similarCombination (inputMsg, guessWord).length ();
	String match;
	if (numbSimilar>=5) match= "Совпадает " + numbSimilar + " последовательных букв в ответе и в слове." + "\n";
	else match= "Совпадает " + numbSimilar + " последовательных буквы в ответе и в слове." + "\n";
	
	if (numbSimilar==0) return wrongAnswerWords() + "\n";
	if (numbSimilar<(guessWord.length()+2)/2) return someRightAnswerWords () + "\n" +  match;
	
	return closeAnswerWords () + "\n" +  match;
}


private String similarCombination (String input, String word) {
	
	String similarity = "";
	
	final int start=0; final int min = 3;

	
	for (int noLetters = word.length()-1; noLetters>=min; noLetters--) {
		
		for (int i=start; i<=word.length()-noLetters; i++) {
			String combination = word.substring(i,i+noLetters);
			if (input.contains(combination)) {similarity=combination; break;} 
		}
		
		if (similarity.length()>=noLetters) break;
	}
	
	return similarity;
}
	private String wrongAnswerWords () {
	
	List <String> words = Arrays.asList("Ответ неверный! Следующая попытка", "Не в точку. Попробуйте ещё раз.", "Лучше ... бывает... ", 
			"Лингвисты, соберитесь!", "Можно попробовать ещё раз", "Упс..", "Не факт. Давайте ещё подумаем", "Так нельзя. А ещё?", 
			"Вот совсем не факт, что это так, даже прямо - это - не так.", "Ещё варианты?", "Попробуем ещё раз?"
			, "А если додумать?! :)", "Сложно, но можно предложить что-то ещё.", "Над альтернативами - не думали?", "На Вашем месте есть смысл попробовать ещё раз.");
	
	return words.get((int) (Math.random()*words.size()));
}
	
	private String someRightAnswerWords () {
		
	List <String> words = Arrays.asList("В Вашем ответе укадывается несколько правильных букв.", "Много похожего. Направление мысли правильное!", 
			"Ответ не сильно далеко от загаданного слова.", 
			"В ответе много общего с загаданным словом.", "Часть ответа однозначно совпадает!", "Точно правильное направление мысли!", "Есть смысл подобрать ещё пару букв.",
			"Легкая схожесть. Усилите?!", 
			"Часть ответа - верная! :)", "Предлагаем добавить ещё несколько правильных букв!", "Однозначно верных ход рассуждений!"
			, "Немного ещё правильных букв...", "Подсказки помогут отгадать ещё пару букв.", "Над частью фразы Вы хорошо поработали. Пробуете ещё?!", 
			"На Вашем месте есть смысл попробовать отгадать ещё несколько правильных букв.");
	
	return words.get((int) (Math.random()*words.size()));
}
	
	private String closeAnswerWords () {
		
	List <String> words = Arrays.asList("Почти! Подумайте над синонимами!", "Очень близкое попадание!", "Почти отгадали. Чего-то не хватает...", 
			"Класс, большая часть фразы совпадает!", "Компьютер почти проиграл.. Остаётся добавить пару букв!", "Гениально! Но для точного ответа чуть-чуть не хватает.");
	
	return words.get((int) (Math.random()*words.size()));
}
	
}
