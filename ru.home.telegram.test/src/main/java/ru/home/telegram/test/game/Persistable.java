package ru.home.telegram.test.game;

import java.util.Date;

public interface Persistable {

	Date getExpiryDate ();
	long getChatId ();
	String getMsg (long chatId, String inputMsg);
	boolean isFinished ();
}
