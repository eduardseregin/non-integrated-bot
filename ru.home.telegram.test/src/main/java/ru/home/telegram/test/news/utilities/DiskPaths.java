package ru.home.telegram.test.news.utilities;

import java.util.Arrays;
import java.util.List;

public class DiskPaths {
	
	public enum Read {
		
		URLS (0), 
		SEARCH_WORDS (1), 
		PREPOSITIONS (2), 
		NEGATIVE (3), 
		POSITIVE (4), 
		SUPERNEGATIVE (5), 
		TOPICS (6);
		
			
			private int index;
			private List <String> paths = Arrays.asList (
					
					"C:\\Temp\\News\\url\\urlsList_Final.txt",  // URLS
					"C:\\Temp\\News\\url\\searchWords.txt",  // SEARCH_WORDS
					"C:\\Temp\\News\\rating\\prepositions.txt",	 // PREPOSITIONS
					"C:\\Temp\\News\\rating\\negativeWords.txt",  // NEGATIVE
					"C:\\Temp\\News\\rating\\positiveWords.txt",	 // POSITIVE
					"C:\\Temp\\News\\rating\\superNegative.txt",	 // SUPERNEGATIVE
					"C:\\Temp\\News\\rating\\topicsRating.txt"	 // TOPICS
					
					);	
			
			Read (int index) {
				this.index = index;
			}
			
			@Override
			public String toString () {
				return this.paths.get (this.index);
			}
		}
		
		public enum ReadWrite {
			
		NEWS (0), 
		USERS (1),
		LOGS_BULK (2),
		LOGS_BULK_OLD (3),
		LOGS_FIRST (4),
		LOGS_SECOND (5),
		MICROSOFT_CHARS (6),
		LAST_UPDATE (7),
		SAVE_SERVICE (8);
			
			
			
			private int index;
			private List <String> paths = Arrays.asList (
					"C:\\Temp\\News\\news.txt",  // NEWS
					"C:\\Temp\\News\\users.txt",  // USERS
					"C:\\Temp\\News\\logs\\bulkNews.txt",  // LOGS_BULK
					"C:\\Temp\\News\\logs\\bulkNewsOld.txt",  // LOGS_BULK_OLD
					"C:\\Temp\\News\\logs\\notTranslatedSorted.txt",  // LOGS_FIRST
					"C:\\Temp\\News\\logs\\translatedSorted.txt",	 // LOGS_SECOND
					"C:\\Temp\\News\\logs\\microsoftChars.txt",	 // MICROSOFT_CHARS
					"C:\\Temp\\News\\logs\\lastUpdate.txt",	 // LAST UPDATE
					"C:\\Temp\\SaveService\\SavedList.txt" // SAVE_SERVICE
					);	
			
			ReadWrite (int index) {
				this.index = index;
			}	
			
			@Override
			public String toString () {
				return this.paths.get (this.index);
			}
		}
}