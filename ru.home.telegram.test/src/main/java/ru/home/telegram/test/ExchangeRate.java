package ru.home.telegram.test;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ru.home.telegram.copy.Parser;


public class ExchangeRate {

	
	private Map <String, Double> exchangeRates;
	private String establishedDate;
	
	
	public static String getEurGbpUsdMsg () throws IOException {
		String msg = "";
		ExchangeRate rates = new ExchangeRate();
		msg+="������ ������ ����������� ����� c " + rates.establishedDate + "\n";
		msg+="  EUR - " + rates.exchangeRates.get("EUR") + "���."+ "\n";
		msg+="  GBP - " + rates.exchangeRates.get("GBP") + "���."+ "\n";
		msg+="  USD - " + rates.exchangeRates.get("USD") + "���."+ "\n";
		return msg;
	}
	
	public static String getSpecificRateToUpperCase (String curr) throws IOException {
		curr=curr.toUpperCase();
		ExchangeRate rates = new ExchangeRate();
		return "C "+ rates.establishedDate +": " + curr + " - " + rates.exchangeRates.get(curr);
		
	}
	
	private ExchangeRate () throws IOException {
		
		// gets inf from cbr.ru site
		   List <String> lines = new Parser.Builder("Exchange Rates")
		    		.url("https://cbr.ru/currency_base/daily/")
		    		.elem("data")
		    		.build().getElements();

		   // prepares the line and Map
		    String cbRFRates = lines.get(0);
		    cbRFRates=cbRFRates.substring(posEnglCapitalLetterFirst (cbRFRates));
		    exchangeRates = new HashMap <>();
		    
		    // fills the map with data
		    while (posEnglCapitalLetterFirst (cbRFRates)!=0) {

		    	String individualLine = cbRFRates.substring(0, posEnglCapitalLetterFirst (cbRFRates)-1);
		    	
		    	exchangeRates.put(individualLine.substring(0, 3), extractRate(individualLine));
		    	cbRFRates=cbRFRates.substring(posEnglCapitalLetterFirst (cbRFRates));
		   }
		   // adds-up the last line of the inf 
		    if (cbRFRates.length()!=0) exchangeRates.put(cbRFRates.substring(0, 3), extractRate(cbRFRates+" 1"));
			// DONE    
		    
		    this .establishedDate=getEstablishedDate();
	}
	
	
	private int posEnglCapitalLetterFirst(String r) {

		int pos = r.length() - 1;

		for (int i = 3; i < r.length(); i++) {
			if (pos == r.length() - 1) {
				for (int y = 65; y < 91; y++) {
					if (r.charAt(i) == ((char) (y))) {
						pos = i;
						break;
					}
				}
			}
		}

		if (pos != r.length() - 1)
			return pos;

		return 0;
	}
    
	private double extractRate(String s) {

		String rate = s.substring(0, s.lastIndexOf(" ") - 1);
		rate = rate.substring(rate.lastIndexOf(" ") + 1);
		rate = rate.replace(',', '.');

		String qtyString = s.substring(s.indexOf(" ") + 1, s.indexOf(" ", s.indexOf(" ") + 1));

		int qty = 1;

		try {
			qty = Integer.parseInt(qtyString);
		} catch (Exception e) {
		}

		try {
			int increasedDouble = (int) (Math.round(Double.parseDouble(rate) * 100 / qty));
			return (double) increasedDouble / 100;

		} catch (Exception e) {
			return 0;
		}
	}

	private static String getEstablishedDate () throws IOException {
		
	    List <String> date = new Parser.Builder("for Date")
	    		.url("https://cbr.ru/currency_base/daily/")
	    		.elem("datepicker-filter")
	    		.attr("data-default-value")
	    		.build().getElements();
	    
	    return date.get(0);
	}
}
