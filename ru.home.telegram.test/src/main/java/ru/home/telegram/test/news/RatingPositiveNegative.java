package ru.home.telegram.test.news;


import java.util.List;

import ru.home.telegram.test.news.utilities.Disk;
import ru.home.telegram.test.news.utilities.DiskPaths.Read;
import ru.home.telegram.test.news.dto.NewsRate;
import ru.home.telegram.test.news.utilities.Words;

public class RatingPositiveNegative implements RatingGiver {

	public static final int POSITIVE_WEIGHT = 300;
	public static final int NEGATIVE_WEIGHT = -200;
	public static final int SUPERNEGATIVE_WEIGHT = - 800;
	public static final int END_PREPOSITION_WEIGHT = -800;
	public static final int LESS_FIVE_WEIGHT = -1500;
	public static final int LESS_SEVEN_WEIGHT = -300;
	public static final int ESTONIA_WEIGHT = 1150;
	public static final int RUSSIA_WEIGHT = 200;
	public static final int CHINA_WEIGHT = -350;
	
	private static List<String> negative;
	private static List<String> superNegative;
	
	private static List<String> positive;
	private static List<String> preposition;

	
	
	public RatingPositiveNegative() {
		if (negative == null || positive == null ) {
			negative = Disk.loadLines (Read.NEGATIVE);
			superNegative = Disk.loadLines (Read.SUPERNEGATIVE);
			positive = Disk.loadLines (Read.POSITIVE);
			preposition = Disk.loadLines (Read.PREPOSITIONS);
		}
	}

	@Override
	public List<NewsRate> giveRate(List<NewsRate> newsRates) {
		
			
		for (NewsRate newsRate : newsRates) {
			newsRate.addRating (getPositRate (newsRate.getWords ()));
			
//			System.out.println(newsRate.getLine ());
//			System.out.println(getPositRate (newsRate.getWords ()));
		}
		
		return newsRates;
	}

	private int getPositRate (List <String> words) {

		if (words == null) return 0;
		if (words.size()<3) return 0;
		
//		words.stream().forEach (x->System.out.println(x));
		
		int rate = 0;
		
		if (words.size ()<6) rate+=LESS_FIVE_WEIGHT ;
		// System.out.println(rate + " / less 5");
		
		if (words.size ()>=6 && words.size ()<8) rate+=LESS_SEVEN_WEIGHT ;
		// System.out.println(rate + " / less 5");
		
		if (Words.isEqualtoOneInWordInList (words.get(words.size ()-1), preposition)) {
			rate += END_PREPOSITION_WEIGHT;
		}
//		System.out.println(rate + " / end prep");
		
		if (Words.isEqualtoOneInWordInList ("Russia", words)) {
			rate+= RUSSIA_WEIGHT;
		}
		
//		System.out.println(rate + "  rus weight");
		
		if (Words.isEqualtoOneInWordInList ("Estonia", words)) {
			
			rate+= ESTONIA_WEIGHT;
		}
		
//		System.out.println(rate + " / estonia");
		
		if (Words.isEqualtoOneInWordInList ("China", words)) {
			rate+= CHINA_WEIGHT;
		}
		
//		System.out.println(rate + " / china");
		
		if (Words.isEqualsCompareWords (superNegative, words)) {
			rate+= SUPERNEGATIVE_WEIGHT;
		}
		
//		System.out.println(rate + " superNegat");
		
		for (String w : words) {
		
		  boolean inNegative = Words.isEqualtoOneInWordInList (w, negative);
		  boolean inPositive = Words.isEqualtoOneInWordInList (w, positive);

		//  if (inNegative) System.out.println (w + " -> negative");
		  
		  if (inNegative) {
			  rate+=NEGATIVE_WEIGHT;
		  } else {
			  if (inPositive) {
				  rate+=POSITIVE_WEIGHT;
			  }	  
		  }  
//		  System.out.println(rate + " / negat / posit");
		}
		
		return rate;
		
	}
	
	
		
}

