package ru.home.telegram.test.news.utilities;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import ru.home.telegram.test.news.utilities.DiskPaths.Read;
import ru.home.telegram.test.news.utilities.DiskPaths.ReadWrite;



public class Disk {

	
	
	public static List <String> loadLines (Read path) {
			
		return fromFile (path.toString ());
		
	}
	
	public static List <String> loadLines (ReadWrite path) {
		
		return fromFile (path.toString ());
		
	}
	
	public static void saveLines(ReadWrite path, List <String> lines) {

		toFile(path.toString(), lines);

	}
	
	public static List <String> fromFile (String path) {
		
		List <String> strings = new ArrayList<>();
		
		
		try {
		File f = new File (path);
		if (f.exists()) {
					
			try (FileInputStream istream = new FileInputStream(f)) {
							strings = new BufferedReader(new InputStreamReader(istream))
								   .lines().collect(Collectors.toList());
					istream.close();
			}
		}
		
		} catch (Exception e) {}
		
	return strings;
		
	}
	
	public static void toFile (String path, List <String> strings) {

		try {
		
		FileOutputStream fileOutputStream = new FileOutputStream(path);

	     for (String s: strings) {
	       s+="\n";
	       fileOutputStream.write(s.getBytes());
	     }

	    fileOutputStream.close();
	    
		} catch (Exception e) {System.out.println(e);}
	}

	public static int getUsersSaveDelayMin() {
		return 15;
	}
	
}