package ru.home.telegram.test.news.dto;


import java.util.List;
import java.util.stream.Collectors;

import ru.home.telegram.test.news.utilities.Lang;
import ru.home.telegram.test.news.utilities.Words;


public class NewsRate implements Comparable<NewsRate> {

	/**
	 * 
	 */
	private String line;
	private int rate;
	private Lang lang;
	
	public static List <NewsRate> instantiate (List <String> bulkNews) {
		return bulkNews.stream()
				.map(x->new NewsRate (x))
				.collect(Collectors.toList());
	}
	
	public NewsRate(String line) {
		this.line = line;
		this.rate = 0;
		this.lang = Lang.OT;
		if (line !=null) {
			if (line.contains ("-#-") && line.startsWith ("EN")) {
				this.lang = Lang.EN;
			}
	
		}
	}
	
	public NewsRate (NewsRate old, String newLine) {
	
		String start = old.line.substring(0, old.line.indexOf ("-#-")+3);
		String url = old.line.substring(old.line.lastIndexOf("("));
		
		this.line = start + newLine + " " + url;
		this.rate = old.rate;
		this.lang = Lang.TR;
		
	}
	
	public int getLength () {
		return getWords ().size ();
	}
	
	public List <String> getWords () {
		return Words.decomposeWords(Words.onlyLettersNumbers (Words.extractLine(line), null));
	}
	
	public void addRating (int additionOrReduction) {
		this.rate += additionOrReduction;
	}
	
	public void resetRating () {
		this.rate = 0;
	}
	
	public String getLine() {
		return line;
	}
	
	

	public Lang getLang() {
		return lang;
	}
	

	@Override
	public String toString() {
		return "NewsRate [line=" + line  + "]" + "\n"+" => [rate = "+ rate +"]";
	}


	@Override
	public int compareTo(NewsRate newsRate) {

	  if (this.rate == newsRate.rate) {
	  return 0;
	  }

	  if (this.rate > newsRate.rate) {
      return -1;
	  }

	  return 1;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((line == null) ? 0 : line.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NewsRate other = (NewsRate) obj;
		if (line == null) {
			if (other.line != null)
				return false;
		} else if (!Words.isDuplicating(this.line, other.line))
			return false;
		return true;
	}
	
	public boolean isRatingPositive () {
		return this.rate>30;
	}
}

