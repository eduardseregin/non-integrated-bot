package ru.home.telegram.test.game;


import java.util.Arrays;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

public class GoldHuntGame extends Game implements Persistable {

	static List <String> classCalls = Arrays.asList("замок","замк","ghost","castl", "привид", "привед");
	
	enum Artefact {NO, SWORD, POTION, SWORD_POTION}
	
	private static List <String> validMoveWords = Arrays.asList("0","1","2","3","4","5","6","7", "50", "100");
	// private int [] stack = {0, 0, 0};
	private Artefact arts=Artefact.NO;
	
	private int gold;
	private int sword;
	private int potion;
	private int potionExpireCount;
	private int ghost;
	
		
	private int yourRoom;
	private String msg;
	
	private SortedSet <Integer> lastTurns;
	
	
	public static GoldHuntGame getInstance (long chatId, String inputMsg) {
		if (isCalled(inputMsg, classCalls)) return new GoldHuntGame (chatId);
		return null;
	}
	
	
	private GoldHuntGame (long chatId) {
		
		super (chatId);
		this.gold = (int) (1+Math.random()*6);
		this.sword = (int) (1+Math.random()*6);
		this.potion = (int) (1+Math.random()*6);
		this.ghost = (int) (1+Math.random()*6);
		this.count = 0;
		this.potionExpireCount=0;
		this.yourRoom=0;
		
		this.lastTurns = new TreeSet <>();
			
		//if (this.gold ==4) this.gold = (int) (1+Math.random()*6);
		if (this.sword == this.gold) {
			while (this.sword == this.gold) this.sword = (int) (1+Math.random()*6);
		}
	
		if (this.potion == gold || this.potion == sword) {
			while (this.potion == this.gold || this.potion == this.sword) this.potion = (int) (1+Math.random()*6);
		}
		
		if (this.ghost == this.gold || this.ghost == this.sword || this.ghost == this.potion) {
			while (this.ghost == this.gold || this.ghost == this.sword || this.ghost == this.potion) this.ghost = (int) (1+Math.random()*6);
		}
	
		this.msg="";
	}
	
	
	@Override
	public String getMsg(long chatId, String inputMsg) {
	
		String msg = getGeneralMsg (inputMsg);
		if (msg!=null) return msg;
		
		// to change later
		if (inputMsg.equals("подсказка 50  👉")) inputMsg="50";
		if (inputMsg.contains("✓")) inputMsg=inputMsg.substring(0,1);
				
		int turn = 0;
		if (validMoveWords.contains(inputMsg.toLowerCase())) {
			turn = Integer.parseInt(inputMsg);
		}

		if (turn == 100) {
			return this.getRulesMsg();
		}
		if (turn == 50) {
			return this.getHint();
		}
		if (turn == this.yourRoom) {
			if (turn==0 || turn == 7) return "Вы и так находитесь в коридоре " + turn + ". Выберите комнату или коридор.";
			if (turn == 1) return "Вы и так находитесь в комнате " + turn + ". Выберите другую комнату.";
			if (turn == 2) return "Вы и так находитесь в комнате " + turn + ". Выберите другую комнату или коридор.";
			return "Вы и так находитесь в комнате " + turn + ". Выберите коридор для выхода.";
		}
		
		if (!isFinished)
		return this.turnSpecificMessage(turn);
		
		return null;

	}
	
	//MAIN
	private String turnSpecificMessage (int turn) {
		
		if (roomChallenger(yourRoom, turn) != "") {

			//msg+=roomChallenger(yourRoom, turn);
		return 	roomChallenger(yourRoom, turn);
		}
		
		if (turn!=0 && turn!=7) lastTurns.add(turn);
		yourRoom = readdress(turn);
		count++;
		
		String messageToReturn = msg;
		this.msg="";
		return messageToReturn;
	}
	
	
	private int moveGhost (int current) {
		if (ghost!=10) if (count == 2 || count == 7 || count == 12) {
			this.ghost = (int) (1+Math.random()*6);
			
			if (ghost==yourRoom && arts == Artefact.NO) { 
				current=ifGhost (current); 
			msg+="Увидев переместившееся приведение, Вы, сметая всё на пути, вышли в коридор (у Вас же нечем пока защищаться). " + "\n"
					+ "Приведение впрочем тоже возможно переместилось." + "\n";
			} 
			else msg+="Внимание! Привидение возможно переместилось!"+"\n";
			while (ghost==yourRoom) this.ghost = (int) (1+Math.random()*6);
			
		}
	return current;
	}

	private int ifGhost (int current) {
		if (current >=5) return 7;
		return 0;
	}

	private String getHint () {
		String hint="";
		if (lastTurns.size()>0) hint+=lastTurns.stream().map(x->""+x).reduce("Вы посетили комнаты: ", (subtotal, element) -> subtotal + element + ",");
		else hint+="Вы ещё не посетили комнаты";
		hint+="\n"+"в шаге от Вас ";
		
		boolean isSecond=false;
		if (roomChallenger(yourRoom, gold) == "") {hint+="золото"; isSecond=true;}
		if (roomChallenger(yourRoom, sword) == "") {if (isSecond) hint+=","; hint+="меч"; isSecond=true;}
		if (roomChallenger(yourRoom, potion) == "") {if (isSecond) hint+=","; hint+="зелье"; isSecond=true;}
		if (roomChallenger(yourRoom, ghost) == "") {if (isSecond) hint+=","; hint+="привидение"; isSecond=true;}
		
		if (!isSecond) hint+="ничего нет";
		hint+=".";
		
		return hint;
	}

	private int readdress(int turn) {
		
		if (turn !=0 && turn !=7) 
		
	{checkArtefacts (turn);}
		
		if (!isFinished && ghost!=10) turn = moveGhost (turn);
		
		if ( !isFinished) {
		msg+=getRoomMsg (turn);}
		//}
		
		return turn;
	}



	
	// GAME SPECIFIC EXTRA MESSAGES
	
	private void checkArtefacts (int current) {

		// GHOST CHECK
			if (current == ghost) {
				if (arts==Artefact.NO) {
					msg+="Повезет в другой раз. Вас лишило жизни привидение." + "\n";
					msg+=fin();
				}
				else {
					if (arts == Artefact.SWORD || arts==Artefact.SWORD_POTION) {
					    ghost=10;
						msg+="Вы использовали меч. Вы лишь чудом не погибли в страшной схватке с привидением."+"\n";
						if (current != gold) msg+="Tеперь замок Ваш, осталось найти золото!"+"\n";
						if (arts==Artefact.SWORD_POTION)   potionExpireCount=0;
					}
					else {
						
						ghost=10;
						msg+="Вы выпили зелье и только благодаря этому спаслись от привидения." + "\n";
						potionExpireCount=0;
						
						if (current !=gold) msg+="Tеперь замок Ваш, осталось найти золото!"+"\n";
					}
				}
				
			}
			
			// SWORD CHECK
			if (current == sword && !isFinished) {
				msg+="Вы получаете меч!"+"\n";
				if (ghost!=10) msg+="Tеперь Вы надежно защищены от привидения!"+"\n";
				if (arts==Artefact.POTION) {arts=Artefact.SWORD_POTION; potionExpireCount=0;}
				else arts=Artefact.SWORD;
				sword = 10;
			}
		
			
			// POTION CHECK
				if (current == potion && !isFinished) {
					msg+="Вы получаете зелье!" + "\n";
					if (ghost!=10 && arts == Artefact.NO ) {
						potionExpireCount=this.count+4; // 3+1, as count will increase in this iteration by 1
						msg+="Зелье испарится через 3 хода. Сумейте использовать его до этого!"+ "\n";
					}
					
					if (arts==Artefact.SWORD) arts=Artefact.SWORD_POTION; else arts=Artefact.POTION;
					potion = 10;
					
				}
				
				if (potionExpireCount!=0 && count>potionExpireCount && ghost!=10 && current!=ghost && arts==Artefact.POTION) {
					arts=Artefact.NO;
					potionExpireCount=0;
					msg+="Сообщение! Зелье испарилось и больше Вас не защищает."+"\n";
				}
			
			// GOLD CHECK
				if (current == gold && !isFinished) {
					msg+="Поздравляем! Вы нашли золото!" + "\n";
					gold = 0;
					msg+=fin();
				}
				
				
	}
	
	
	// MESSAGE CONFIRMING VALIDITY OF THE NEXT TURN
	
	private static String roomChallenger (int current, int turn) {
	
		if (current==turn) return "";
		int [] k0 =  {2,3,4,7}, k1 =  {2,2,2,2}, k2 =  {1,0,0,0}, k3 =  {0,0,0,0};
		int [] k4 =  {0,0,0,0}, k5 =  {7,7,7,7}, k6 =  {7,7,7,7}, k7 =  {0,5,6,0};
		
		List<int[]> option = Arrays.asList(k0,k1,k2,k3,k4,k5,k6,k7);
		
		if (turn != option.get(current) [0] && turn != option.get(current) [1] &&turn != option.get(current) [2] &&turn != option.get(current) [3]) {
			String s2 = "Ошибка! Вы не можете пройти в эту комнату напрямую. Попробуйте еще раз." + "\n";
		return s2;	
		}
		return "";
	}
	
	
	// MESSAGE DEPENDING ON CURRENT POSITION
	
	private static String getRoomMsg (int current) {
		
		List <String> msges = Arrays.asList(
				"Вы в холле 1 этажа (0). Куда пойдете?"+"\n"+"  2. На склад"+"\n"+"  3. На кухню"+"\n"+"  4. В библиотеку"+"\n"+"  7. В холл второго этажа"+"\n",
				"Вы в подвале (1). Куда пойдете?"+"\n"+"  2. Вернуться на склад"+"\n",
				"Вы на складе (2). Куда пойдете?"+"\n"+"  0. Вернуться в холл 1 этажа"+"\n" +"  1. В подвал"+"\n",
				"Вы на кухне (3). Куда пойдете?"+"\n"+"  0. Вернуться в холл 1 этажа"+"\n",
				"Вы в библиотеке (4). Куда пойдете?"+"\n"+"  0. Вернуться в холл 1 этажа"+"\n",
				"Вы в спальне (5). Куда пойдете?"+"\n" + "  7. Вернуться в холл 2 этажа"+"\n",
				"Вы в гостевой спальне (6). Куда пойдете?"+"\n" + "  7. Вернуться в холл 2 этажа"+"\n",
				"Вы в холле 2 этажа. Куда пойдете?"+"\n"+"  0. Вернуться в холл 1 этажа"+"\n"+"  5. В спальню"+"\n"+"  6. В гостевую спальню"+"\n"
				);
		
		return msges.get(current);
	}
	
	// STANDARD MESSAGES
	
	protected String initialMsg () {
		String string = "";
		string+="ИГРА 'ЗАМОК С ПРИВЕДЕНИЕМ'! Задача - найти в комнатах замка - ЗОЛОТО. "+"\n";
		string+="ИГРАЕТЕ В ПЕРВЫЙ РАЗ? ---> Введите 'Правила', чтобы прочитать их и посмотреть карту."+"\n"+"\n";
		string+="Цифрой от 0 до 7 вводите номер следующей (ближайшей) комнаты. Другие значения приравниваются к 0"; 
		string+="!!! Напишите 'Выход', если закончили !!! "+"\n"+"\n";
		string+="Для получения подсказки - запросите 50. Удачи!!! "+"\n"+"\n";
		
		string+=turnSpecificMessage(0);
		return string;
	}
	
	
	protected String getRulesMsg () {
		
		List <String> rooms = Arrays.asList(" * 0 - холл 1 этажа \n",
											" * 1 - подземелье \n",
											" * 2 - склад \n",
											" * 3 - кухня \n",
											" * 4 - библиотека \n",
											" * 5 - гостевая спальня \n",
											" * 6 - спальня \n",
											" * 7 - холл 2 этажа" +"\n"
				);
		
		String string="В 4х из 5 разных комнатах замка размещаются золото, меч, зелье и привидение."+"\n";
		string+="К счастью, привидение не появляется в холлах."+"\n";
		string+="От приведения можно защититься зельем или мечом. Зелье теряет защиту через 3 хода."+"\n";
		string+="Привидение перемещается. В том числе и в комнату с золотом / мечом / зельем."+"\n";
		string+="Об этом мы Вас предупредим..." +"\n";
				
		string+="Карта замка: " +"\n"+"\n";
		string+="Комнаты: 6  _  4  _  _  _" + "\n";
		string+="Коридор: 7  7  0  0  0  _" + "\n";
		string+="Комнаты: 5  _  3  _  2  1" + "\n"+"\n";

		for (String r : rooms) {string+=r;}
				
		string+="\n ---> Ваша отправная точка - " + rooms.get(yourRoom) + ".";
	
		return string;
	}




	
	
}

