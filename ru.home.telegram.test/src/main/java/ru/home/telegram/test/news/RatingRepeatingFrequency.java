package ru.home.telegram.test.news;


import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import ru.home.telegram.test.news.dto.NewsRate;
import ru.home.telegram.test.news.utilities.Words;

public class RatingRepeatingFrequency implements RatingGiver {

	public static final int FOUR_TIMES_WEIGHT = 120;
	public static final int THREE_TIMES_WEIGHT = 90;
	public static final int TWO_TIMES_WEIGHT = 50;
	
	private Set<String> allWordsZero;
	private Set<String> allWordsFirst;
	private Set<String> allWordsSecond;
	private Set<String> allWordsThird;

	private static int applyCoeff (int length) {
		return length / 3;
	}
	
	
	public RatingRepeatingFrequency() {
		this.allWordsZero = new TreeSet<>();
		this.allWordsFirst = new TreeSet<>();
		this.allWordsSecond = new TreeSet<>();
		this.allWordsThird = new TreeSet<>();
	}

	@Override
	public List<NewsRate> giveRate(List<NewsRate> newsRates) {
		
		int totalSize = newsRates.size();
		
		registerWords (newsRates);
		
//		System.out.println("--3--");
//		allWordsThird.stream ().forEach (x->System.out.println(x));
//		System.out.println("--2--");
//		allWordsSecond.stream ().forEach (x->System.out.println(x));
//		System.out.println("--1--");
//		allWordsFirst.stream ().forEach (x->System.out.println(x));
//		System.out.println("--0--");
//		allWordsZero.stream ().forEach (x->System.out.println(x));
		
		for (NewsRate newsRate : newsRates) {
			newsRate.addRating (getRepRate (newsRate.getLine(), totalSize));
			
//			System.out.println(newsRate.getLine ());
//			System.out.println(getRepRate (newsRate.getLine(), totalSize));
		}
		
		return newsRates;
	}

	private int getRepRate (String l, int totalSize) {

		int coeff = applyCoeff (l.length());
	//	System.out.println("coeff = " + coeff);

		if (Words.isContainedInLongLine (l,allWordsThird)) {
			return FOUR_TIMES_WEIGHT * coeff / totalSize;
		}
		if (Words.isContainedInLongLine (l,allWordsSecond)) {
			return THREE_TIMES_WEIGHT * coeff / totalSize;
		}
		if (Words.isContainedInLongLine (l,allWordsFirst)) {
			return TWO_TIMES_WEIGHT * coeff / totalSize;
		}
	return 0;
	}
	
	private void registerWords (List<NewsRate> newsRates) {

		for (NewsRate newsRate : newsRates) {
		
		  List <String> singleNews = newsRate.getWords ();
		
		  List <String> threeWords = new ArrayList <>();
		  for (int i = 0; i<singleNews.size () - 2; i++) {
			  threeWords.add (singleNews.get (i) + " " + singleNews.get (i+1) + " " + singleNews.get (i+2));
			  
		  }
			
			  for (String s : threeWords) {
			if (allWordsSecond.contains(s)) {
				allWordsThird.add(s);
			} 
			if (allWordsFirst.contains(s)) {
				allWordsSecond.add(s);
			} 
			if (allWordsZero.contains(s)) {
				  allWordsFirst.add(s);
			} 
			
			        allWordsZero.add(s);
		  }
		 }  
	}
		
}
