package ru.home.telegram.test.news;

import java.util.List;

import ru.home.telegram.test.news.dto.NewsRate;

public interface RatingGiver {

	public List <NewsRate> giveRate (List <NewsRate> newsRates);
	
}

