package ru.home.telegram.test.game;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.Connection.Method;
import org.jsoup.nodes.Document;


public class DictionaryYandex extends Dictionary {
	
	private final String Dic_Path = "C:\\Temp\\Production\\yandexDictionaryCategories.txt";
	protected List <Map <String, String>> dictionary;
	protected List <String> langs;

	@Override
	public String getDescription () {
		
		String desc="Вы получаете слова и подсказки на следующих языках:"+"\n";
		desc+="  1. "+langSerie (2,4)+"\n";
		desc+="  2. "+langSerie (5,7)+"\n";
		desc+="  3. "+langSerie (8,10)+"\n";
		desc+="  4. "+langSerie (11,13)+"\n";
		desc+="Подсказка 1: "+langSerie (14,16)+"\n";
		desc+="Подсказка 2: "+langSerie (17,19)+"\n";
		
		desc+=" >>> Запросите цифрами от 1 до 4 - синоним указанного под цифрой иностранного слова"+"\n";
		desc+="на соответствующем языке (если доступен)"+"\n";
		
		return desc;
	}

	@Override
	public Map<Word, String> getSample(int line) {
		if (dictionary==null) return null;
		
	
		Map<Word, String> sample = new HashMap <> ();

					
		sample.put(Word.LineNumber, ""+line);
		sample.put(Word.Russian, extract (line,0,0));
		sample.put(Word.English, extract (line,1,1));
		
		sample.put(Word.First, extract (line,2,4));
		sample.put(Word.Second, extract (line,5,7));
		sample.put(Word.Third, extract (line,8,10));
		sample.put(Word.Fourth, extract (line,11,13));
		sample.put(Word.Hint_Lat, extract (line,14,16));
		sample.put(Word.Hint_Cyrilic, extract (line,17,19));

		sample.put(Word.FirstSyn, synonymInLang (sample.get(Word.First), sample.get(Word.Russian) ));
		sample.put(Word.SecondSyn, synonymInLang (sample.get(Word.Second), sample.get(Word.Russian) ));
		sample.put(Word.ThirdSyn, synonymInLang (sample.get(Word.Third), sample.get(Word.Russian) ));
		sample.put(Word.FourthSyn, synonymInLang (sample.get(Word.Fourth), sample.get(Word.Russian) ));
		
		return sample;
	}
	
	private String extract (int line, int langIndStart, int langIndEnd) {
		
		if (langIndStart==langIndEnd) return dictionary.get(line).get(langs.get(langIndStart));
		
		if (langIndStart>=0 && langIndStart<langIndEnd && langIndEnd<langs.size()) {
			int indDiff = (int) (Math.random()*(langIndEnd-langIndStart+1));
			String wordAndLang = dictionary.get(line).get(langs.get(langIndStart+indDiff));
			wordAndLang+=" (" + langs.get(langIndStart+indDiff)+")";
			return wordAndLang;
		}
		return "-";
	}
	
	
	protected DictionaryYandex ()  {
		this.langs=getSortedLangNames ();
		this.dictionary=getDictFromFile (Dic_Path);
	}
	
	private List <Map <String, String>> getDictFromFile (String path) {
		
		dictionary = new ArrayList<>();
		List <String> linesOfWords = fromFile (path);
		
		
		for (String bulkLine : linesOfWords) {
			
			Map <String, String> languages = new HashMap <> ();
			
			for (String lang: langs) {
				String translated = "";

				final String spliter = "-#-";
				int splitInd = bulkLine.indexOf(spliter);
				if (splitInd>=0) {
					translated =bulkLine.substring(0, splitInd);
					bulkLine = bulkLine.substring(splitInd+3);

					languages.put(lang, translated);
				}
			}
					
			dictionary.add(languages);
			
		}	
		
		
		return dictionary;
	}
	
	private static List <String> fromFile (String path) {
		
		List<String> strings = new ArrayList<>();

		try {

			File f = new File(path);
			if (f.exists()) {

				try (FileInputStream istream = new FileInputStream(f)) {
					strings = new BufferedReader(new InputStreamReader(istream)).lines().collect(Collectors.toList());
					istream.close();
				}
			}
		} catch (Exception e) {
		}
	return strings;
		
	}
	
	private List <String> getSortedLangNames () {
		return Arrays.asList("русский","английский", "эстонский","финский","венгерский",
				"датский","голландский","норвежский",
				"испанский","итальянский","португальский","греческий","латышский","чешский", 
				"немецкий", "французский", "польский",
				"беларусский","болгарский","украинский", "категория"
				);
	}

	private List <String> getSortedYandLangs () {
		return Arrays.asList("ru-ru", "ru-en", "ru-et", "ru-fi", "ru-hu", "ru-da", "ru-nl", "ru-no",
				"ru-es", "ru-it", "ru-pt", "ru-el", "ru-lt", "ru-cs", "ru-de", "ru-fr", "ru-pl", "ru-be", "ru-bg",
				"ru-uk");
	}
	
	private String synonymInLang (String word, String rusWord) {
		
		String dict = getYandexLangName (word);
		String synLang = "синоним_недоступен";
		List <String> synonims = getRuSynonyms(rusWord);
		
		for (String ru : synonims) {
			ru = ru.toLowerCase();
			String transl = yandexDictSearch (ru, dict).toLowerCase();
			if (transl!="нет_перевода" && transl!="" && !word.contains(transl)) {
				synLang = transl; break;
			}
		}
		
		if (synLang!="синоним_недоступен") 
			synLang+=" (" + getSortedLangNames().get(getSortedYandLangs().indexOf(dict)) +")";
		
		return synLang;
	}
	
	private String getYandexLangName(String word) {

		List<String> yDictRu = getSortedYandLangs ();

		int index = -1;
		List<String> langs = getSortedLangNames();

		for (String lang : langs) {
			if (word.contains(lang))
				index = langs.indexOf(lang);
		}

		if (index == -1)
			return "error";
		return yDictRu.get(index);
	}
	
	
	private String langSerie (int start, int end) {
		if (start<=end && end<langs.size() && start>=0) {
		String s="";
		
		for (int i=start; i<=end; i++) {s+=langs.get(i)+", ";}
		s=s.substring(0,s.lastIndexOf(","));
		return s;
		}
		return null;
	}

	@Override
	public int getDictSize() {
		return dictionary.size();
	}

	private String yandexDictSearch (String text, String dict) {

		String result = "";
		String apiKey = "dict.1.1.20210609T091755Z.f7b1aaf487deada3.717d2cdd1d7684c1e6f18f5997f7c8f65c68551b";
		// "%20dict.1.1.20210608T212522Z.9e24f2cb7fbddfd3.7f992cd4178daaf0f665b38b40309419d708f5bf";

		String url = "https://dictionary.yandex.net/api/v1/dicservice.json/lookup?key=" + apiKey + "&lang=" + dict
				+ "&text=" + text;

		try {
			Connection conn = Jsoup.connect(url).cookie("cookiereference", "cookievalue").method(Method.GET)
					.ignoreContentType(true);

			Document doc = Jsoup.parse(new String(conn.execute().bodyAsBytes()));

			byte[] charset = doc.body().text().getBytes();

			String textContents = new String(charset, "UTF-8");

			//System.out.println(textContents);

			int indBeg = textContents.indexOf("tr");
			indBeg = textContents.indexOf("text", indBeg) + 7;
			int indEnd = textContents.indexOf("pos", indBeg) - 3;

			String transl = "-";

			try {
				transl = textContents.substring(indBeg, indEnd);
			} catch (Exception e) {			}

			if (transl == "-") {
				result = "нет_перевода";
			} else
				result = transl;
		} catch (Exception e) {		}

		return result;
			
	}

	@Override
	public List<String> getCategories() {
		return new ArrayList<>();
	} 
	
}



