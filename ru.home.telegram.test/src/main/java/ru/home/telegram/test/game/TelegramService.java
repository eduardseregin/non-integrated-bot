package ru.home.telegram.test.game;


import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public abstract class TelegramService {

	private static List <Persistable> gamers=new ArrayList<>();
	
	abstract protected String getMsg (long chatId, String inputMsg);
	
	
	public static String getBotResponse (long chatId, String inputMsg) throws FileNotFoundException, IOException {
		updateAllLists ();
		
		Persistable existingOrNewGamer = getPersistableIfExist (chatId, inputMsg);
		if (existingOrNewGamer!=null) {
			String log = existingOrNewGamer.getMsg(chatId, inputMsg);
			if (log==null) System.out.println("Im in TelServ: final msg - null");
			return log;}
		
		// ... here checks and applies other services
		
		System.out.println("Im in TelServ: it gets to null as other conditions are not met");
		
		return null;
	}
	

	private static Persistable getPersistableIfExist (long chatId, String inputMsg) {

		// If in Game - gets a Gamer
		
		Persistable existingGamer = gamers.stream().filter (x->x.getChatId()==chatId).findFirst().orElse(null);
		if (existingGamer!=null) 
			
			{ if (existingGamer.isFinished()) gamers=gamers.stream() // removes finished from the list
			.filter(x->x.getChatId()!=existingGamer.getChatId())
			.collect(Collectors.toList());
			else return existingGamer;
		}
		
		// If not in game, checks if the request for a new game and if yes, returns a new Gamer

		Persistable potentialGamer = Game.getInstance (chatId,inputMsg);
		
		if (potentialGamer!=null) {
			gamers.add(potentialGamer);
			return potentialGamer;
		}
		
		return null;
	}

	private static void updateAllLists () {
		gamers = updateList (gamers);
		// ... other lists if appear
	}
	
	private static List <Persistable> updateList (List <Persistable> list) {
		Date now = new Date ();
		return list.stream()
		.filter(x->(now.compareTo(x.getExpiryDate()))<0)
		.collect(Collectors.toList());
	}
	
	protected static boolean isCalled (String msg, List <String> callWords) {
		
		msg=msg.toLowerCase();
		
		System.out.println(msg);
		
		for (String g: callWords) {
			g=g.toLowerCase();
			if (msg.contains(g)) {System.out.println(g + " - " + msg.contains(g));  return true; }
		}
			return false;
		
	}
	
}
