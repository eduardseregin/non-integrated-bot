package ru.home.telegram.test.news;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.List;
import javax.swing.Timer;
import java.util.stream.Collectors;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import ru.home.telegram.test.news.dto.NewsRate;
import ru.home.telegram.test.news.utilities.Disk;
import ru.home.telegram.test.news.utilities.DiskPaths.ReadWrite;
import ru.home.telegram.test.news.utilities.Lang;
import ru.home.telegram.test.news.utilities.Words;

public class MicrosoftTranslator {

	private static int charsTranslated =0;
	private static Timer saveTimer;
	
	MicrosoftTranslator () {}
	
	public List <NewsRate> translate (List <NewsRate> newsRates) {
		
		if (charsTranslated==0) charsTranslated = Initialiser.charInitialiser();
		if (saveTimer == null) Initialiser.timerInitialiser();
		
		List <NewsRate> notTranslated = newsRates.stream().filter(x->x.getLang()==Lang.EN).collect(Collectors.toList());
		List <NewsRate> translated = newsRates.stream()
				.filter(x->x.getLang()!=Lang.EN)
				.map(x-> new NewsRate (x,translate (Words.extractLine(x.getLine()))))
				.collect(Collectors.toList());
		
		translated.addAll(notTranslated);
		newsRates = translated;
		
		return newsRates;
	}
	
	public static int getTranslatedQty () {
		return charsTranslated;
	}
	
	public String translate (String toTranslate)  {

		charsTranslated+=toTranslate.length();
		if (!saveTimer.isRunning()) saveTimer.start();
		
		// String toTranslate = "Come la  missione  'Dalla  Russia  con  Amore'  ha  permesso  al  Cremlino  di  difendersi  dal  covid  e  realizzare  Sputnik-V";
		String result = toTranslate;
		
		// Second line protection as Microsoft does not accept special chars
		char [] allowed = {',', '.', '!', '?'};
		toTranslate = Words.onlyLettersNumbers(toTranslate, allowed );
		
		try {
		
		final String apiKey = MicCredentials.apiKey;
		
				
		final String url = "https://api.cognitive.microsofttranslator.com/translate?api-version=3.0&to=en";
		
		String fHeaderName = "Ocp-Apim-Subscription-Key";
		String fHeaderValue = apiKey;
		String sHeaderName = "Content-Type";
		String sHeaderValue = "application/json; charset=UTF-8";
		
		Connection.Response execute = Jsoup.connect(url)
				.header(fHeaderName, fHeaderValue)
				.header("Ocp-Apim-Subscription-Region", "westeurope")
				.header(sHeaderName, sHeaderValue)
	//			.data("Subscription-Key", "{"+apiKey+"}")
				.ignoreHttpErrors(true)
				   .method(Connection.Method.POST)
				   .requestBody("")
				.requestBody("[{'Text':'"+toTranslate+"' }]")
				.ignoreContentType(true).execute();
		
		Document doc = execute.parse();
		
		result = parseTranslation (doc.text());
		if (result == "") System.out.println("Not translated: " + toTranslate);
		
		} catch (Exception e) {}
		
	System.out.println("Translated:" + result);

	return result;
	
	}

	private String parseTranslation (String text) {
		
		if (text.contains("[{\"text\":")) text = text.substring(text.indexOf("[{\"text\":")+10);
		if (text.contains(",")) text = text.substring(0,text.lastIndexOf(",")-1);
		
		return text;
	}

	
	final class MicCredentials {
		private static final String apiKey = "32b1431472b9440597bb318bdb28c9c4";
		
	}

	static class CharCountSaver implements ActionListener {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			Disk.saveLines(ReadWrite.MICROSOFT_CHARS, Arrays.asList(""+charsTranslated));
			System.out.println(" - > number of translated chars is stored on the disc (" +charsTranslated + " pcs.)" );
		}
	}

	static class Initialiser {

			
		static int charInitialiser () {
			String charNumber = Disk.loadLines (ReadWrite.MICROSOFT_CHARS)
					.stream().filter(x->x.length()>0).findFirst().orElse(null);
			int chars = 0;
			
			if (charNumber ==null) return chars;
			if (charNumber.length()==0) return chars;
			
			try {
				chars = Integer.parseInt(charNumber);
			} catch (Exception e) {
				
			}
		return chars;
			
		}
		
		static void timerInitialiser () {
			saveTimer = new Timer (2*60*1000, new CharCountSaver ());
			saveTimer.setRepeats(false);
		}
		
	}

}


