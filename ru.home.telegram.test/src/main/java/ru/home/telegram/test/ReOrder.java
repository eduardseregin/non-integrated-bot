package ru.home.telegram.test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ReOrder {

	public static void main(String[] args) throws FileNotFoundException, IOException {
		
		List <String> asItIs = Arrays.asList("русский","английский", "чешский","шведский",
				"польский","эстонский","финский","венгерский",
				"датский","голландский","латвийский",
				"испанский","итальянский","португальский","греческий",
				"беларусский","болгарский", "немецкий", "французский", "украинский"
				);
		
		List <String> toBe = Arrays.asList("русский","английский", "эстонский","финский","венгерский",
				"датский","голландский","шведский",
				"испанский","итальянский","португальский","греческий","латвийский","чешский", 
				"немецкий", "французский", "польский",
				"беларусский","болгарский","украинский"
				);
		
		
		List <Map <String, String>> dictionary = new ArrayList<> ();
		final String spliter = "-#-";
		
		String path = "C:\\Temp\\finalDict.txt";
		String pathToWrite = "C:\\Temp\\ReOrderedDict.txt";
			
		List <String> rus = fromFile (path);
		
		for (String bulkLine : rus) {
			Map <String, String> languages = new HashMap <> ();
			for (String lang: asItIs) {
				String translated = "";
				
				int splitInd = bulkLine.indexOf(spliter);
				if (splitInd>=0) {
					translated =bulkLine.substring(0, splitInd);
					languages.put(lang, translated);
					bulkLine = bulkLine.substring(splitInd+3);
				}
			}
			dictionary.add(languages);
		}		
				
					List <String> rusChecked=new ArrayList<>();
	// re-order logic	
				
				for (Map <String, String> translations : dictionary) {
					String toWrite = "";
					for (String lang : toBe) {
						toWrite+=translations.get(lang)+spliter;
					}
					rusChecked.add(toWrite);
				}
			
				
				toFile (pathToWrite, rusChecked);
				
				rusChecked.stream().limit(5).forEach(x->System.out.println(x));
				System.out.println (rusChecked.size());
		

	}

	private static List <String> fromFile (String path) throws FileNotFoundException, IOException {
		
		List <String> strings = new ArrayList<>();
		
		File f = new File(path);
		if (f.exists()) {
					
			try (FileInputStream istream = new FileInputStream(f)) {
							strings = new BufferedReader(new InputStreamReader(istream))
								   .lines().collect(Collectors.toList());
					istream.close();
			}
		}
	return strings;
		
	}
	
	private static void toFile (String path, List <String> strings) throws FileNotFoundException, IOException {

		FileOutputStream fileOutputStream = new FileOutputStream(path);

	     for (String s: strings) {
	       s+="\n";
	       fileOutputStream.write(s.getBytes());
	     }

	    fileOutputStream.close();
	}
	
}
