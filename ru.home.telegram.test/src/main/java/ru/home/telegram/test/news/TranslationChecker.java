package ru.home.telegram.test.news;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import ru.home.telegram.test.news.dto.NewsRate;
import ru.home.telegram.test.news.utilities.Lang;
import ru.home.telegram.test.news.utilities.Words;

public class TranslationChecker {

	private Lang language;
	
	private static final int MIN_WORDS = 6;
	private static final int MIN_LETTERS = 4;
	final static List <String> dicts = Arrays.asList("en");
			//, "es", "fr", "de");
	
public TranslationChecker () {}

public List <NewsRate> ensureEnglish (List <NewsRate> newsRates) {

	List <NewsRate> other = newsRates.stream().filter(x->x.getLang()==Lang.TR).collect (Collectors.toList());
	newsRates = newsRates.stream()
			.filter(x->x.getLang()==Lang.TR)
			    .filter(x->getEnglishChecked (x.getLine()))
					.collect (Collectors.toList());
	newsRates.addAll(other);
	return newsRates;
}

public boolean getEnglishChecked (String text) {
		
		List <String> sortedWords =  getSortedWords(text);
	
		String test = null;
		if (sortedWords!=null) {
			test = testLang (sortedWords);
		}
		
		if (test!=null) {
			language=Lang.EN;
			return true;
		} else {
			language = Lang.OT;
		}
		return false;
		
	}
	

	
	public Lang getLanguage() {
	return language;
}


	public List <String> getSortedWords (String text) {
		if (text == null) return null;
		if (text.length () < 2) return null;
		
		List <String> words = Words.decomposeWords(Words.onlyLettersNumbers(text, null));
		
		if (words.size ()< MIN_WORDS) return null;
		
		words = words.stream()
				.filter(x->x.length()>=MIN_LETTERS)
				//.filter(x->!isContained(x, searchWords))
				.sorted(Comparator.comparing(String :: length).reversed())
				.collect(Collectors.toList());
		
		// words.stream().forEach (x->System.out.println (x));
		
		return words;
	} 
	
	

	private String testLang (List <String> wordsSorted) {
		
		List <Integer> rating = new ArrayList<>();
		for (int i = 0; i<dicts.size(); i++) {rating.add(0);}
		
		for (String dict : dicts) {
			
			for (int i = 0; i<Math.min(3, wordsSorted.size()); i++) {
				
			String response = getDict(wordsSorted.get(i), dict);
			
				if (response!=null) {
					
					int index = dicts.indexOf(response);
									
					rating.set(index, rating.get (index) +1);
					
									
					if (rating.get(index)>=3) return dicts.get(index).toUpperCase();
				}
			}
		}
		
		return null;
	}
	
	
	private String getDict (String word, String dict) {
		
		List <String> dicNames = Arrays.asList("anglais");
				//, "espagnol", "français", "allemand");

		dict = dict.toLowerCase(); if (!dicts.contains(dict)) return null;
		word.toLowerCase();

		final String url = "https://synonyms.reverso.net/synonyme/"+ dict+"/" + word;
		final String elem = "synonym  relevant";
		
		
		List <String> list=null;
		String title = "";
		
		try {
		Document doc = Jsoup.connect(url).get();
			list= doc.getElementsByClass(elem).eachText();
			 title = doc.title();
		} catch (Exception e) {}
		
	
				
		if (!isContained(title, dicNames)) return null;

		if (list.size()>=1)
			if (title.contains(dicNames.get(dicts.indexOf(dict)))) return dict;
			else {
		
					for (int i = 0; i<dicNames.size(); i++) {
						if (title.contains(dicNames.get(i))) {return dicts.get(i);}
			}   
		}
		
		return null;
	}
	
	private static boolean isContained (String s, List <String> list) {
		if (list==null) return false;
		if (list.size()==0) return false;

			
		for (String l:list) {
			if (s.contains(l)) return true;
		}
		
		return false;
	}
	
	@Override
	public String toString () {
		if (language==Lang.EN) return "EN";
		else return "OT";
	}
}
