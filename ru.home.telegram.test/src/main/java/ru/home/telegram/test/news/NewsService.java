package ru.home.telegram.test.news;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.stream.Collectors;

import javax.swing.Timer;

import ru.home.telegram.test.news.dto.FinalNews;
import ru.home.telegram.test.news.utilities.Disk;
import ru.home.telegram.test.news.utilities.DiskPaths.ReadWrite;
import ru.home.telegram.test.news.utilities.Words;


public class NewsService {

	private static final List<String> userCalls = Arrays.asList ("новости", "новость", "в мире", "планете", "газетах", "news", "new",
			"russia", "estonia", "Новости (News in English)  " + new String(Character.toChars(0x1F310)));
	private static List <NewsService> users;
	
	private static Timer saveTimer;
	

	private static FinalNews news;
	private static NewsRefresher refresher = new NewsRefresher ();
	
	
	private long chatId;
	private boolean never;
	private boolean twice;
	

	
	public static void checkRefresher() {
		 refresher.checkForUpdates();
		 updateUsersList ();
		 createTimer ();
	}

	public static NewsService instance (long chatId) {
		
		if (news == null) {
			news = new FinalNews ();
		}
		
		return users.stream ().filter (x->x.getChatId () == chatId).findFirst ().orElse (new NewsService (chatId)); 
		
	}
	
	private static void updateUsersList () {
		if (users == null) {
			System.out.println("Initiate NewsService");
			
			users = new ArrayList <> ();
			List <String> usersIds = Disk.loadLines (ReadWrite.USERS);
			// two times applied
					
			List <Integer> userIdsInt = usersIds.stream().map(x->Words.parseInt(x.trim())).collect(Collectors.toList());
			
			for (int i : userIdsInt) {

				int count = (int) userIdsInt.stream().filter (x->x==i).count();
				boolean isNoneCreated = users.stream().noneMatch(x->x.chatId==i);
				
				if (isNoneCreated && count >1) {
				  	users.add(new NewsService (i,true));
				  } 
				
				if (isNoneCreated && count ==1) {
					users.add(new NewsService (i,false));
				}
				
			}
			
			
			
		}
		
	}
	
	public static boolean isNewsCall (String msg) {
		msg = msg.toLowerCase ();
	    
		return Words.isContainedInLongLine(msg, userCalls);
	}
	
	public static void reset () {
		users = new ArrayList<>();
				
		System.out.println("Users list of those received news is now RESET. News are reloaded");
		
		createTimer ();
		
		if (!saveTimer.isRunning()){
			saveTimer.start();
			System.out.println("Users received news RESET timer is started running: " + new Date ());
		}
	}
	
	private NewsService(long chatId) {
		
		this.chatId = chatId;
		this.never = true;
		this.twice = false;
		
		getUsers().add (this);
	}

	private NewsService(long chatId, boolean twice) {
		this.chatId = chatId;
		this.never = false;
		this.twice = twice;
	}

	public String newsMessage (String msg) {
		
		if (msg == null) return null;
			
		if (msg.contains("100")) {
			userReset ();
			return "Обновлена возможность повторно получать те же сообщения";
		}
		
		if (!saveTimer.isRunning()){
			saveTimer.start();
			System.out.println("Users received news timer is started running: " + new Date ());
		}
		
		
		if (never) {
			never = false;
			if (news.getNews().stream().limit(15).collect(Collectors.counting())==0) {
				return "Новости в настоящий момент недоступны.";
			}
			return  getInitialMsg () + 
					news.getNews().stream()
					    .limit(15)
					        .map(x->x.toString())
					            .reduce("", 
							        (partialString, element) -> partialString + element + "\n");
		}
		
		if (!twice) {
			twice = true; 
			if (news.getNews().stream().limit(30).skip (15).collect(Collectors.counting())==0) {
				return "Дополнительные новости в настоящий момент недоступны.";
			}
			return news.getNews().stream()
					 .limit(30).skip (15)
					     .map(x->x.toString())
					         .reduce("", (partialString, element) -> partialString + element + "\n");

		}
			return "Обновленные новости можно читать после 16.30 вечером и 7.30 утром";
		
	}

	
	
	public long getChatId() {
		return chatId;
	}
	
	
	public boolean isTwice() {
		return twice;
	}
	

	public boolean isNever() {
		return never;
	}


	

	public static List <NewsService> getUsers() {
		return users;
	}
	
	private void userReset () {
		users = users.stream().filter(x->x.chatId!=this.chatId).collect(Collectors.toList());
	}
	
	public static void createTimer () {
		
		if (saveTimer == null) {
		
		saveTimer = new Timer(Disk.getUsersSaveDelayMin() *60 * 1000,
				new SaveUsers ());
		saveTimer.setRepeats(false);
	
		}
	}
	
	private String getInitialMsg () {
		String msg="";
		msg+="Новости на англ. языке по данным анализа 350 новостных сайтов, " + "\n";
		msg+="отобранных по тематике из 38 тыс. зарегистрированных в мире СМИ." + "\n";
		msg+="Ранжированы по ключевым словам. Перевод с других языков: Microsoft Translator" + "\n"+"\n";
		
		return msg;
		
	}
}

class SaveUsers implements ActionListener {

	
	@Override
	public void actionPerformed(ActionEvent e) {
		
		List<String> usersToSave = NewsService.getUsers().stream()
											   .filter(x -> !x.isNever())
											   .map(x -> x.getChatId())
											   .map(x -> Long.toString(x))
											   .collect(Collectors.toList());
		
		usersToSave.addAll(NewsService.getUsers().stream()
												 .filter(x -> x.isTwice())
												 .map(x -> x.getChatId())
												 .map(x -> Long.toString(x))
												 .collect(Collectors.toList()));

		Disk.saveLines(ReadWrite.USERS, usersToSave);

	}
}

class NewsRefresher {
	
	private Timer newsLaunchTimer;
	private ActionListener newsUpdater;
	private LastUpdate lastUpdate;
	
	public NewsRefresher () {
		newsUpdater = new NewsBox ();
		setTimer (newsUpdater);
		lastUpdate = new LastUpdate ();
	}
	
	public void checkForUpdates () {
		if (lastUpdate.isUpdateTime() && !newsLaunchTimer.isRunning()) {
			newsLaunchTimer.start();
			System.out.println("News Timer has been launched: news update will start in 2 mins");
			lastUpdate = new LastUpdate (true);
		}
	}
	
	
	private void setTimer (ActionListener newsUpdater) {
	newsLaunchTimer = new Timer(2 * 60 * 1000,    // 2 minutes for cooling-off
			newsUpdater);
	newsLaunchTimer.setRepeats(false);
	}
}
	


class LastUpdate {
	
	// YEAR-MONTH-DATE-HOUR
	private final int updateHourMorning = 7;
	private final int updateHourEvening = 16;
	private final int updateMinutes = 15;
	
	private int year;
	private int month;
	private int day;
	private int hour;
	
	private Date nextDate;
	
	public LastUpdate(boolean updated) {
	
		GregorianCalendar cal = (GregorianCalendar) GregorianCalendar.getInstance();
		
		this.year = cal.get(Calendar.YEAR);
		this.month = cal.get(Calendar.MONTH);
		this.day = cal.get(Calendar.DATE);
		this.hour = cal.get(Calendar.HOUR_OF_DAY);
		
		this.nextDate = getNextDate();
		System.out.println("News next update set: " + nextDate);
		save (this);
	}
	
	
	public LastUpdate() {
		List<String> lines = Disk.loadLines(ReadWrite.LAST_UPDATE);
		
		if (lines!=null && lines.size()>0) {
		
		String fromFile = lines.get(0);
		
		if (fromFile !=null && fromFile.length()>=10 && fromFile.contains("-") && fromFile.indexOf("-")<fromFile.lastIndexOf("-")) { 
		
			int [] elements = new int [3];
			
			for (int i=0; i<3; i++) {
				if (fromFile.indexOf("-")<0) { 
					break;
				}
				elements [i] = Words.parseInt (fromFile.substring(0,fromFile.indexOf("-")));
				if (fromFile.indexOf("-")<fromFile.length()-1) {
					fromFile = fromFile.substring(fromFile.indexOf("-")+1);
				} else {
					break;
				}
			}
			
		  this.hour = Words.parseInt (fromFile);
			
		  this.year = elements [0];
		  this.month = elements [1];
		  this.day = elements [2];;
		  		
		}
		
		this.nextDate = getNextDate();
		System.out.println("News next update set: " + nextDate);
		}
	}

	
	public boolean isUpdateTime () {
		
		if (this.nextDate==null) return true;
		
		Date now = new Date ();
		
		return now.compareTo(nextDate)>0;
		
	}
	
	private boolean isDefined () {
		return year!=0 && month!=0 && day!=0;
	}
	
	private Date getNextDate () {
		if (isDefined ()) {

			GregorianCalendar current = (GregorianCalendar) GregorianCalendar.getInstance();
					
			GregorianCalendar next = (GregorianCalendar) GregorianCalendar.getInstance();
			next.set(Calendar.MINUTE, updateMinutes);
			next.set(Calendar.SECOND, 0);
			
			if (current.get(Calendar.YEAR) == this.year && current.get(Calendar.MONTH) == this.month
					&& current.get(Calendar.DATE) == this.day 
					    && current.get(Calendar.HOUR_OF_DAY) >= updateHourEvening 
					        && this.hour>=updateHourEvening-3) {
				
				next.add(Calendar.DATE, 1);
				next.set(Calendar.HOUR_OF_DAY, updateHourMorning);
				return next.getTime();
			}
			
			if (current.get(Calendar.YEAR) == this.year && current.get(Calendar.MONTH) == this.month
					&& current.get(Calendar.DATE) == this.day 
					    && current.get(Calendar.HOUR_OF_DAY) < updateHourEvening 
					        && current.get(Calendar.HOUR_OF_DAY) >= updateHourMorning 
					            && this.hour>=updateHourMorning-3) {
				
				next.set(Calendar.HOUR_OF_DAY, updateHourEvening);
				return next.getTime();
			}
			
			current.add(Calendar.DATE, -1);
			if (current.get(Calendar.YEAR) == this.year && current.get(Calendar.MONTH) == this.month
					&& current.get(Calendar.DATE) == this.day 
					    && current.get(Calendar.HOUR_OF_DAY) < updateHourMorning 
					        && this.hour>=updateHourEvening-3) {
				
				next.set(Calendar.HOUR_OF_DAY, updateHourMorning);
				return next.getTime();
			}
		}
		
		return null;
	}
	
	private void save (LastUpdate update) {
		String s = update.toString();
		List <String> toSave = new ArrayList <> ();
		toSave.add(s);
		Disk.saveLines(ReadWrite.LAST_UPDATE, toSave);
		
	}
	

	@Override
	public String toString() {
		return year + "-" + month + "-" +  day + "-" +  hour;
	}
	

}

