package ru.home.telegram.test.saveservice;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import javax.swing.Timer;

import ru.home.telegram.test.news.utilities.Disk;
import ru.home.telegram.test.news.utilities.DiskPaths.ReadWrite;
import ru.home.telegram.test.news.utilities.Words;

public class SaveService {

	private static final List<String> userCalls = Arrays.asList ("save", "add", "show", "delete", "delete all", "replace");
	private static long validChatId = 244162579;
	private static List <SaveItem> items;
	
	private static Timer saveTimer;
	
	public static List <SaveItem> getItems () {
		return items;
	}
	
	public static boolean isNewCall (String msg, long chatId) {
		if (chatId!=validChatId) return false;
		if (items == null) items = SaveItem.load();
		if (items == null) items = new ArrayList<>();
		
		if (saveTimer == null) createTimer ();
		msg = msg.toLowerCase ();
	    
		return Words.isContainedInLongLine(msg, userCalls);
	}
	
	
	public static String newsMessage (String msg) {
		if (msg == null) {
			return null;
		}
		
		String msgToCheck = msg.toLowerCase();
		if (msgToCheck.contains("service") || msgToCheck.contains("rules")) {
			return getInitialMsg();
		}
		
		
		if (msgToCheck.contains("show")) {
			if (items.size()==0) return "So far no items to show.";
			return items.stream().reduce("",(remainder, incr)->""+remainder+incr + "\n", String::concat);
		}
		
		if (!saveTimer.isRunning()) {
			saveTimer.start();
			System.out.println("SaveItem timer started running: save in 2 min.");
		}
		
		if (msgToCheck.contains("delete all")) {
			items = new ArrayList<>();
			return "All items have been delete.";
			
		}
		
		if (msgToCheck.contains("delete")) {
			int indexD = delete (msg);
			if (indexD<0)
			return "Input is incorrect, no valid delete index identified.";
			return "Successfully delete: " + (indexD +1) + ".";
		}
		
		
		if (msgToCheck.contains("replace")) {
			int indexR = replace (msg);
			if (indexR<0)
				return "Replacement is incorrect, no valid replacement index identified.";
				return "Successfully replace: " + (indexR +1) + ".";
		}
		
		if (!msgToCheck.contains("save") && !msgToCheck.contains("add")) return "Попробуйте ещё раз";
		
		
		int start;
		if (msgToCheck.contains("save")) {
		
			start = msgToCheck.indexOf("save");
			msg = msg.substring(0, start) + msg.substring(Math.min(msg.length(), start+5));
		} else {
			start = msgToCheck.indexOf("add");
			msg = msg.substring(0, start) + msg.substring(Math.min(msg.length(), start+4));
		}
		
				
		if (items.size()>0) {
			items.add(new SaveItem (msg, items.get(items.size()-1)));
		} else {
			items.add(new SaveItem (msg, null));
		}
		return "Saved: " + msg;
		
	}

	private static int replace(String msg) {
		if (msg == null) return -1;
		if (msg.length()<11 || !msg.toLowerCase().contains("replace ") 
				|| msg.indexOf("replace ") + 9 > msg.length()) return -1;
		
		List <String> decomposed = Words.decomposeWords(msg.toLowerCase());
		
		decomposed.stream().map(x->decomposed.indexOf(x)).forEach(System.out::println);
		
		decomposed.stream().forEach(System.out::println);
		int indexOfReplace = decomposed.indexOf("replace");
		System.out.println("Index of replace = " +indexOfReplace);
		
		int index = Words.parseInt(decomposed.get(indexOfReplace+1))-1; // number to replace is next to "replace", index starts from 0, therefore, number minus 1
		System.out.println("index = "+index);
		if (index<0 || index>=items.size()) return -1;
		
		
		List <String> decomposedIgnorCase = Words.decomposeWords(msg);
		
		msg = decomposedIgnorCase.stream()
				.filter(x->decomposedIgnorCase.indexOf(x)!=indexOfReplace && decomposedIgnorCase.indexOf(x)!=indexOfReplace+1)
				.reduce(""+(index+1) + ". ", (remainder, increment)->remainder + increment + " ");

		
		
		items.set(index, new SaveItem (msg));
		return index;
	}

	private static int delete (String msg) {
		if (msg == null) return -1;
		if (msg.length()<5) return -1;
		String pos = msg.chars().mapToObj(i->(char)i).filter(x->Character.isDigit(x)).map(Object::toString).collect(Collectors.joining());
		int index = Words.parseInt(pos)-1;
		
		if (index<0 || index>=items.size()) return -1;
		
		items = IntStream.range(0,items.size())
				.filter(x->x!=index)
				.mapToObj(x->items.get(x))
				.collect(Collectors.toList());
		
		// changes numbering
		items = IntStream.range(0,items.size())
				.peek(x->items.get(x).setId(x+1))
				.mapToObj(x->items.get(x))
				.collect(Collectors.toList());
		
		return index;
	}
	
	public static void createTimer () {
		
		if (saveTimer == null) {
		
		saveTimer = new Timer(2 *60 * 1000,
				new SaveStore ());
		saveTimer.setRepeats(false);
		}
	}
	
	private static String getInitialMsg () {
		String msg="Опции: save (any String), show (all), delete (порядковый номер), delete (all)";
	
		return msg;
		
	}
}


class SaveStore implements ActionListener {
	
	
	@Override
	public void actionPerformed(ActionEvent e) {
		
		List <SaveItem> items = SaveService.getItems();
		
		List <String> toSave = IntStream.range(0,items.size())
				.peek(x->items.get(x).setId(x+1)).mapToObj(x->items.get(x)).map (x->x.toString()).collect(Collectors.toList());
		
		Disk.saveLines(ReadWrite.SAVE_SERVICE, toSave);
		System.out.println("SaveItems successfully stored");
		
	}
}
	


class SaveItem {
	
		private int id;
	private String item;
	
	
	public static List <SaveItem> load () {
		List <String> lines = Disk.loadLines(ReadWrite.SAVE_SERVICE);
	
		return lines.stream().map(x->new SaveItem (x)).collect(Collectors.toList());
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getItem() {
		return item;
	}

	public SaveItem(String item, SaveItem previous) { // by user
		if (previous != null) {
			this.id = previous.id+1;
		} else {
			this.id = 1;
		}
		this.item = item;	
		 
	}
	
	public SaveItem(String item) {  // from file
		this.id = Words.parseInt(item.substring(0,item.indexOf(". ")));
		
		if (item.contains(". "))
		this.item = item.substring(item.indexOf(". ")+2);
		else this.item = item;
	}


	@Override
	public String toString() {
		return id + ". " + item;
	}
	

}

