package ru.home.telegram.test.news;

import java.util.List;

import ru.home.telegram.test.news.utilities.Disk;
import ru.home.telegram.test.news.utilities.DiskPaths.Read;
import ru.home.telegram.test.news.dto.NewsRate;

public class RatingTopicsRelevance implements RatingGiver {

	public static final int TOPIC_LEVEL_WEIGHT = 220;
	public static final int TIRE_ZERO_WEIGHT = 156;
	public static final int TIRE_ONE_WEIGHT = 80;
	public static final int TIRE_TWO_WEIGHT = 30;

	
	
	private static List<String> topics;


	
	
	public RatingTopicsRelevance() {
		if (topics == null) {
			topics = Disk.loadLines (Read.TOPICS);
		}
	}

	@Override
	public List<NewsRate> giveRate(List<NewsRate> newsRates) {
		
			
		for (NewsRate newsRate : newsRates) {
			newsRate.addRating (getTopicRate (newsRate.getWords ()));
			
//			System.out.println(newsRate.getLine ());
//			System.out.println(getTopicRate (newsRate.getWords ()));
		}
		
		return newsRates;
	}

	private int getTopicRate(List<String> words) {

		int rate = 0;

		for (String w : words) {

			int index = topics.indexOf(w);

			int result = choice(index);
			
//			if (index>2) System.out.println(w + ": " + result + "(for in): " + topics.get (index-1));
			
			rate += result;
		}
		return rate;

	}
	
	private int choice (int index) {
	
		final int topic = 15;
		final int tireOne = 270;
		final int tireTwo = 2700;
		
		
		if (index > tireTwo) return TIRE_TWO_WEIGHT;
		if (index > tireOne) return TIRE_ONE_WEIGHT;
		if (index > topic) return TIRE_ZERO_WEIGHT;
		if (index>=0) return TOPIC_LEVEL_WEIGHT;
		
		return 0;
		
	}
	
		
}
