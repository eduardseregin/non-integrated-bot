package ru.home.telegram.test.game;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DictionaryYandexCategory extends DictionaryYandex {

	protected DictionaryYandexCategory() {
		super();
		}

	@Override
	public List <String> getCategories () {
		if (dictionary==null) return null;
		
		List <String> categories = new ArrayList<>();
		
		for (int line=0; line<dictionary.size(); line++) {
		
			String catCoding = dictionary.get(line).get(langs.get(langs.size()-1));
			String cat=getCatNames().get(0);
			
			if (getCatCoding ().contains(catCoding)) {
				int index = getCatCoding ().indexOf(catCoding);
				cat=getCatNames().get(index) + "  " + getCatEmodji().get(index);
				}
				
			categories.add(cat);
		}
		
		return categories;
	}
	
	public List <String> getCatNames () {
		return Arrays.asList("Жизненные события", "Люди (профессии и статусы)", "Природа и окружение", 
				"Институты и экономика", "Юриспруденция", "Техника");
	}
	
	private List <String> getCatCoding () {
		return Arrays.asList("LIVE", "PEOPLE", "ENVIRON", "ECINST", "LAW", "MADE");
	}
	
	private List <String> getCatEmodji () {
		return Arrays.asList(new String(Character.toChars(0x2618)), new String(Character.toChars(0x1F38E)), new String(Character.toChars(0x1F41D)), 
				new String(Character.toChars(0x1F5FD)), new String(Character.toChars(0x2694)), new String(Character.toChars(0x1F9EF)));
	}
	
	
}
