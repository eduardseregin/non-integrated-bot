package ru.home.telegram.test.news;
import java.util.Arrays;
import java.util.List;

import ru.home.telegram.test.news.utilities.Words;

public class StringNormaliser {

	
	private String string;

	
	public StringNormaliser(String string) {
		char [] allowed = {'.', ',', ':', ';', '!', '?', '(', ')', '-', '%', '$', '\''};
		this.string = Words.onlyLettersNumbers(removeExtraSpace(removeCode (addSpaceAndRemoveSpecialNoDuplication  (string))), 
				allowed);
		
	}

	@Override
	public String toString () {
		return string;
	}
	
	
	private String addSpaceAndRemoveSpecialNoDuplication (String text) {
	
		List <String> special = Arrays.asList(">","<","http","=","&", "mejs", "false", "(this);", "js-","Image", "tsr-",
				"true", "error", "#", "type", "quo;", "@", "[", "]", "{", "}","img-", "thumbnail", "aria-", "data-", "click",
				"'share'", "'ht site'", "'send'", "'event'", "items ()", ".queue", "commentCount", "QUERY");
		
		List <String> noDuplication = Arrays.asList(":", "(", "%");
		
		text = removeJson (text);
		
		text = addSpacesWhereNeeded(text);
		
		if (text.contains("to myFt for later")) {
			text = text.substring(0,text.lastIndexOf("to myFt for later"));
		}
		
	    List <String> words = Words.decomposeWords (text);
		
		
		String result = "";
		
		for (String w : words) {
		
			if (w.endsWith("-")) w = w.substring(0, w.lastIndexOf("-"));
			
			if (!Words.isContainedInLongLine(w, special) && w.length()<20)
			result+=w+ " ";
			else result += "... ";

		}
		
		if (result!="") result = result.substring(0, result.length()-1);

		for (String c: noDuplication) {
			
			if (result.contains(c) && result.indexOf(c)!=result.lastIndexOf(c)) {
				
				int index = result.indexOf(c); 
				index = result.indexOf(c,index+1);
				result = result.substring(0, index);
			}
		}
		
		return result;	
	}
	
	
	private String removeJson(String text) {

		if (text==null) return "";
		if (text.length()<2) return text;

		while (text.contains("[") && text.contains("]") && text.indexOf("[")<text.indexOf("]", text.indexOf("[")) && text.indexOf("]")<text.length()-1) {
			
			text = text.substring(0, text.indexOf("[")) + text.substring(text.indexOf("]", text.indexOf("[")+1)+1); 
		}

		while (text.contains("{") && text.contains("}") && text.indexOf("{")<text.indexOf("}", text.indexOf("{")) && text.indexOf("}")<text.length()-1) {
			text = text.substring(0, text.indexOf("{")) + text.substring(text.indexOf("}", text.indexOf("{")+1)+1); 
		}
		
		
		
		return text;
	}

	private String addSpacesWhereNeeded (String text) {
		
		if (text==null) return "";
			if (text.length()<3) return text;
		
		List<Character> spaceBefore = Arrays.asList( '(', '<');
		List<Character> spaceAfter = Arrays.asList( ')', '-', '%', ',', '>');
		
		String result = "";
		for (int i = 0; i<text.length()-1; i++) {
			if (spaceBefore.contains(text.charAt(i)) || spaceAfter.contains(text.charAt(i))) {
				
				if (spaceBefore.contains(text.charAt(i))) result+=" "+text.charAt(i);
				else result+=""+text.charAt(i)+" ";
				
			}
			
			else result+=""+text.charAt(i);
		}
		
		result+="" + text.charAt (text.length()-1);
		
		return result;
	}
	
	private String removeCode (String text) {
		
		for (int y = 1; y<=4; y++) {
		
		if (text.contains("$") && text.contains("(") && text.contains(".") && text.indexOf("$")<text.indexOf("(") && text.indexOf("(")<text.indexOf(".")) {
			
			int index = text.indexOf(".", text.indexOf("("));
			
			
			if (index+1<text.length()) if (Character.isLetter(text.charAt(index+1))) {

				
				
				boolean noOther = true;
				for (int i=text.indexOf("$")+1; i<index; i++) {
					if (text.charAt(i)!=' ' && text.charAt(i)!='(') noOther=false;
				}
				
				if (noOther) 
					if (text.indexOf(";", text.indexOf("$"))>0 && text.indexOf(";", text.indexOf("$"))<text.length()-2 )
					text = text.substring(0, text.indexOf("$")) + text.substring(text.indexOf(";", text.indexOf("$"))+1);
					else 
						text = text.substring(0, text.indexOf("$"));
			}
		}
		}
		
		return text;
	} 
	
	private String removeExtraSpace(String text) {

		List<Character> noSpaceBefore = Arrays.asList(',', ':', ';', '!', '?');

		if (text.length() < 2)
			return text;

		String result = "" + text.charAt(0);
		char prev = text.charAt(0);

		for (int i = 1; i < text.length(); i++) {

			if (noSpaceBefore.contains(text.charAt(i)) && prev == ' ' && result.length() > 2) {
				result = result.substring(0, result.length() - 1);
			}

			if (text.charAt(i) != ' ' || prev != ' ') {

				result += text.charAt(i);
			}

			if (result.length() > 2 && noSpaceBefore.contains(result.charAt(result.length() - 1))
					&& noSpaceBefore.contains(result.charAt(result.length() - 2))) {
				result = result.substring(0, result.length() - 1);
			}

			prev = text.charAt(i);

		}

		while (result.length() > 2 && !Character.isLetter(result.charAt(0)))
			result = result.substring(1);

		if (result.length() > 3) {
			String firstLetter = "" + result.charAt(0);
			firstLetter = firstLetter.toUpperCase();
			result = firstLetter + result.substring(1);
		}

		return result;
	}
	
}