package ru.home.telegram.test.news;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import ru.home.telegram.test.news.dto.FinalNews;
import ru.home.telegram.test.news.dto.NewsRate;
import ru.home.telegram.test.news.utilities.Disk;
import ru.home.telegram.test.news.utilities.DiskPaths.ReadWrite;
import ru.home.telegram.test.news.utilities.Words;

public class NewsBox implements ActionListener {

	
	private NewsInitialFormer searcher;
	private MicrosoftTranslator translator;
	private TranslationChecker checker;
	private List <RatingGiver> raters;
	
	
	public NewsBox () {
		
		searcher = new NewsInitialFormer ();
		translator = new MicrosoftTranslator ();
		checker = new TranslationChecker ();
		raters = Arrays.asList(new RatingPositiveNegative(), 
				    new RatingFrequency(), 
				        new RatingRepeatingFrequency(), 
				            new RatingTopicsRelevance());
		
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {

	//	searcher.getBulkNews()
		// 
		List <NewsRate> newsToAdd = NewsRate.instantiate(searcher.getBulkNews());
				// Disk.loadLines(ReadWrite.LOGS_BULK));
		
		newsToAdd = newsToAdd.stream().distinct().filter (x->Words.wordsCount(x.getLine())>4).collect(Collectors.toList());
		
		for (RatingGiver r : raters) {
			newsToAdd = r.giveRate(newsToAdd);
		}
		
		newsToAdd = newsToAdd.stream().sorted().collect(Collectors.toList());
		
		newsToAdd.stream().limit (20).forEach(x->System.out.println(x));
		
		//checker.ensureEnglish(
		
		newsToAdd = (translator.translate(newsToAdd.stream().filter (x->x.isRatingPositive()).collect(Collectors.toList())));
		
		System.out.println(newsToAdd.size() + " (before checker)");
		
		// newsToAdd = checker.ensureEnglish(newsToAdd);
		
		System.out.println(newsToAdd.size()+ " (after checker)");
		
		newsToAdd.stream().forEach(x->x.resetRating());
		
		for (RatingGiver r : raters) {
			newsToAdd = r.giveRate(newsToAdd);
		}
		
		newsToAdd = newsToAdd.stream().sorted().collect(Collectors.toList());
		
		FinalNews.addNews( newsToAdd, Math.min(newsToAdd.size(), 30));

		System.out.println("Translated chars: " + MicrosoftTranslator.getTranslatedQty() + ", that is: " + 
				(int) (MicrosoftTranslator.getTranslatedQty()*100000 / 2000000) / 1000 + " %" );
		
	}

}
