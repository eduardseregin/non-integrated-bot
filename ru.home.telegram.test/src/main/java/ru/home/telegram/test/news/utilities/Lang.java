package ru.home.telegram.test.news.utilities;

public enum Lang {
	OT, // Non-English
	EN, // English
	TR; // Potentially English
	
@Override
	public String toString () {
		if (this == Lang.EN) return "EN";
		return "OT";
	}
};