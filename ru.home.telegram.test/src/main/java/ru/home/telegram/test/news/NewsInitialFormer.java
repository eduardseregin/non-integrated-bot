package ru.home.telegram.test.news;



import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import ru.home.telegram.test.news.utilities.Disk;
import ru.home.telegram.test.news.utilities.DiskPaths.ReadWrite;
import ru.home.telegram.test.news.utilities.Words;
import ru.home.telegram.test.news.utilities.DiskPaths.Read;

public class NewsInitialFormer {

	// private List <String> bulkNews;
	
	private static List <String> allSearchWords = new ArrayList <>();
	
	public NewsInitialFormer() {
		
	}


	public List <String> getBulkNews() {

		List<String> results = new ArrayList<>();

		List<String> urls = Disk.loadLines (Read.URLS);

		int count = 1;

		for (String s : urls) {

		  if (count % 50 == 0) {
			  System.out.println(count);
		  }
			
		  count++;
		
			// String s = "FR-#-http://lexpansion.lexpress.fr/";

		  String beginning = s.substring(0, 5);
		  s = s.substring(5);

		  System.out.println(s);

		  Document doc = null;

		  try {
				doc = Jsoup.connect(s).get();
		  } catch (Exception e) {
			    }

		  if (doc != null) {

				doc = doc.normalise();

				String htmlDoc = doc.html();

				List<String> searchWords = getSearchWord(htmlDoc);

				// String descTitle = doc1.select("h1").text();
				if (searchWords != null) {

					for (String searchWord : searchWords) {

						String result = getBySearchWord(searchWord, htmlDoc);

						result = new StringNormaliser(result).toString();

						if (!Words.isDuplicatingInList(result, results)) {
							results.add(beginning + result + " (" + s + ")");

						}
					}

				}

			}
		}
		results = filterAndSave (results);
		
		// temporary
		Disk.saveLines(ReadWrite.LOGS_FIRST, allSearchWords);
		
		return results;

	}

	private List <String> filterAndSave (List <String> results) {
	
		List <String> oldNews = Disk.loadLines (ReadWrite.LOGS_BULK_OLD);
		//Temporary
		//List <String> oldNews = oldNewsUnfilterd.stream().filter(x->x.length()>60).distinct().collect(Collectors.toList());
		
		// check for duplication with old
		//results = results.stream (). filter (x->!Words.isDuplicatingInList (x,oldNews)).collect (Collectors.toList());
		
		results = results.stream().filter (x->x.length()>55).collect(Collectors.toList());
	//.filter(x->!Words.isDuplicatingInList(x, oldNews))
	
		oldNews.addAll (results);
		Disk.saveLines (ReadWrite.LOGS_BULK_OLD, oldNews);
		
		
		Disk.saveLines (ReadWrite.LOGS_BULK, results);
		
		results.stream().limit(5).forEach(x -> System.out.println(x));
		System.out.println("Total (non-duplicating, more than 60 chars) bulk news collected: " + results.size ());
		
		return results;
	}
	
	private static String getBySearchWord (String searchWord, String sss) {
		
		  int index = sss.indexOf(searchWord);
		  while (sss.substring(index+5).contains(searchWord)) {
		  if (sss.charAt(index)-1!=' ') index = sss.indexOf(searchWord,index+searchWord.length()-1);
		  else break;}
		  
		  
		  int endIndex = sss.length();
		  
		  if (sss.indexOf(">", index)>0) {
			  endIndex =  Math.min(endIndex, sss.indexOf(">", index));
		  }
		  
		  if (sss.indexOf("<", index)>0) {
			  endIndex =  Math.min(endIndex, sss.indexOf("<", index));
		  }
		
		  if (sss.indexOf(".", index)>0) {
		  endIndex = Math.min(endIndex, sss.indexOf(".", index));
		  }
		  if (sss.indexOf("/", index)>0) {
		  endIndex = Math.min(endIndex, sss.indexOf("/", index));
		  }
		  sss = sss.substring(0, endIndex);
		  
		  int begIndex = 0;
		  
		  begIndex = Math.max(begIndex, sss.lastIndexOf("<", index));
				  
		  begIndex = Math.max (begIndex, sss.lastIndexOf("=", index));
		  begIndex = Math.max(begIndex, sss.lastIndexOf("/", index));
		  begIndex = Math.max(begIndex, sss.lastIndexOf(">", index));
		  
		  sss = sss.substring(begIndex);
		  
		  
			// temporary
			
			allSearchWords.add("("+searchWord+") "+sss);
		  
		  return sss;
		
	}
	
	
	private static List <String> getSearchWord (String s) {
		List <String> searchWords = Disk.loadLines (Read.SEARCH_WORDS);
		
		List <String> result = new ArrayList <> ();
		
		for (String w: searchWords) {
			if (s.contains(w)) {
				result.add (w);
				
			}
		}
		
		if (result.size()>0) {
			return result;
		}
		
		return null;
	}
	

}

