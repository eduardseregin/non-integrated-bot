package ru.home.telegram.test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.Connection.Method;
import org.jsoup.nodes.Document;

public class YandexDictFormer {

	static List <String> dictionaries = getSortedDicList ();
	static List <String> dictionariesEn= Arrays.asList(
			"en-ru","en-cs","en-da","en-de",
			"en-el","en-en","en-es","en-et",
			"en-fi","en-fr","en-it","en-lt",
			"en-lv","en-nl","en-no","en-pt",
			"en-sk","en-sv","en-tr",
			"en-uk");
	
	static List <String> dictionariesRu= Arrays.asList(
			"ru-en","ru-cs","ru-da","ru-de",
			"ru-el","ru-es","ru-et",
			"ru-fi","ru-fr","ru-hu","ru-it",
			"ru-lt","ru-lv",
			"ru-nl","ru-no","ru-pl","ru-pt",
			"ru-sk","ru-sv","ru-tr",
			"ru-tt", "ru-uk");
	
	public static void main(String... args) throws Exception {
		
	String path = "C:\\Temp\\__.txt";
	String pathToWrite = "C:\\Temp\\___.txt";
		
	List <String> rus = new ArrayList <> ();
			rus = fromFile (path);
			
			//rus = rus.stream().limit(5).collect(Collectors.toList());
			
			List <String> rusChecked=new ArrayList<>();
	
			for (String s: rus) {
				if (!s.endsWith("ть") && !s.endsWith("ться") && !s.endsWith("й") 
						&& !s.endsWith("тка") && !s.endsWith("тка") && s.length()>6
						&& !s.endsWith("тo")&& !s.endsWith("но") && !s.endsWith("ко") 
						&& !s.endsWith("ка") &&  !s.endsWith("от") &&  !s.endsWith("ец")
						&&  !s.endsWith("ия") &&  !s.endsWith("ее") &&  !s.endsWith("ие") &&  !s.endsWith("не") 
						&&  !s.endsWith("дь") && !s.endsWith("ть")
						&& !s.endsWith("до") && !s.endsWith("то") && !s.endsWith("ти")
						&& !s.endsWith("бо") && !s.endsWith("ди") && !s.endsWith("во") && !s.endsWith("ую")
						&& !s.contains("-") && !s.endsWith("да") && !s.endsWith("сь") && !s.endsWith("и")
						&& !s.endsWith("о") && !s.endsWith("ое") && !s.endsWith("те") && !s.endsWith("та")
						&& !s.endsWith("я")
						) {
					rusChecked.add(s);
				}
			}
			
			
//			for (String r:rus) {
//				boolean first=true;
//				String result = yandexDictSearch (r);
//				System.out.println(" - "+result);
//				if (!result.contains("@")) {
//					if (first) {result=r+"-#-"+result+"-#-"; first=false; }
//					else result+="-#-";
//					rusChecked.add(result);
//				}
//				
//			}
			
			
			toFile (pathToWrite, rusChecked);
			
			rusChecked.stream().limit(5).forEach(x->System.out.println(x));
			System.out.println (rusChecked.size());

	}
	
	private static List <String> getSortedDicList () {
		
		String [] s1 = {"ru-cs","ru-sk","ru-pl"};
		String [] s2 = {"ru-et","ru-fi","ru-hu"};
		String [] s3 = {"ru-da","ru-nl","ru-lv"};
		String [] s4 = {"ru-es","ru-it","ru-pt"};
		String [] s5 = {"ru-el","ru-be","ru-bg"};
	
		
		String [] hint = {"ru-de","ru-fr","ru-uk"};
		
		List <String[]> langs = Arrays.asList(s1,s2,s3,s4,s5,hint);
		
		List <String> rightOrderDicts = new ArrayList<>();
		rightOrderDicts.add("ru-en");
		for (String [] array : langs) {
			rightOrderDicts.add(array [0]);
			rightOrderDicts.add(array [1]);
			rightOrderDicts.add(array [2]);
			
		}
		
		
		List <String> descrRu= Arrays.asList(
				"анг","бел",
				"болг","чеш","дат","нем",
				"греч","исп","эст",
				"фин","фр","венг","итал",
				"лит","лат","марий","марий-луг",
				"гол","норв","польск","порт",
				"словац","шведс","тур",
				"татар","укр","кит");
	
		return rightOrderDicts;
	}
	
	
	private static String yandexDictSearch (String word) throws IOException {

		boolean isPresent=true;
		String result="";
		
		
				
		for (int i=0; i<dictionaries.size();i++) {
			
			String dict = dictionaries.get(i);
			//System.out.println(dict);
			String text=word;
			String apiKey = "dict.1.1.20210609T091755Z.f7b1aaf487deada3.717d2cdd1d7684c1e6f18f5997f7c8f65c68551b";
					//"%20dict.1.1.20210608T212522Z.9e24f2cb7fbddfd3.7f992cd4178daaf0f665b38b40309419d708f5bf";
			
			String url = "https://dictionary.yandex.net/api/v1/dicservice.json/lookup?key="+ apiKey+ "&lang=" + dict+ "&text=" +text;
			
//			Document doc = Jsoup.connect(
//					"https://dictionary.yandex.net/api/v1/dicservice.json/lookup?key="+ apiKey+ "&lang=" + dict+ "&text=" +text)
//					.ignoreContentType(true).get(); 
			
			Connection conn = Jsoup.connect(url)
					.cookie("cookiereference", "cookievalue")
					.method(Method.GET).ignoreContentType(true);
			
	Document doc = Jsoup.parse(new String(
					conn.execute().bodyAsBytes()));
		//	Document doc = Jsoup.parse(new URL(url).openStream(), "UTF-8", url);
			
			byte[] charset = doc.body ().text ().getBytes();
			
	    	String textContents = new String(charset, "UTF-8");
	  
	    	
	    	int indBeg = textContents.indexOf("tr");
	    	indBeg = textContents.indexOf("text", indBeg)+7;
	    	int indEnd = textContents.indexOf("pos", indBeg)-3;
	    	
	    	String transl="-";
	    	
	    	try {transl=textContents.substring(indBeg, indEnd); }
	    	catch (Exception e) {}
	    	
	    	if (transl=="-") {isPresent=false; result="@-"+dict; break;}
	    	//System.out.println(i+"   ="+ transl);
			result+=transl +"-#-";
			}

		return result;
		
	} 
	
	
	private static List <String> fromFile (String path) throws FileNotFoundException, IOException {
		
				
		List <String> strings = new ArrayList<>();
		
		File f = new File(path);
		if (f.exists()) {
					
			try (FileInputStream istream = new FileInputStream(f)) {
				
						
						strings = new BufferedReader(new InputStreamReader(istream))
								   .lines().collect(Collectors.toList());
						
					istream.close();
			}
		}
	return strings;
		
	}
	
	private static void toFile (String path, List <String> strings) throws FileNotFoundException, IOException {

		FileOutputStream fileOutputStream = new FileOutputStream(path);

	     for (String s: strings) {
		 

	       s+="\n";
	       fileOutputStream.write(s.getBytes());
	     }

	    fileOutputStream.close();
		
	}
	
}
