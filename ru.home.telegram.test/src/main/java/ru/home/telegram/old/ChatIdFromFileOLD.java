package ru.home.telegram.old;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public abstract class ChatIdFromFileOLD implements Serializable {

	private static final long serialVersionUID = 1L;
	private static final String filePath = "C:\\Temp\\serializedObject.txt";
	
	private static Calendar timeLimit = initializeDateCounter ();
	private static String lDate=setNewLastDate ();
	
	// MAIN FUNCTION
	public static List<Long> updateFile(List<Long> chatIds) throws FileNotFoundException, IOException {
		
		if (chatIds ==null) {chatIds = new ArrayList<>(); 
		chatIds = readListFromFile (); 
		}
		
		chatIds = saveThisListToFile (chatIds);
		
		return chatIds;
	};
	
	
	private static List <Long> saveThisListToFile (List <Long> chatIds) throws FileNotFoundException, IOException {
		
	if (timeLimit.get(Calendar.DAY_OF_YEAR)<Calendar.getInstance().get(Calendar.DAY_OF_YEAR) ||
			timeLimit.get(Calendar.HOUR_OF_DAY)<=Calendar.getInstance().get(Calendar.HOUR_OF_DAY)) {
		
		timeLimit.set(Calendar.HOUR_OF_DAY, timeLimit.get(Calendar.HOUR_OF_DAY)+1);
		
		chatIds = nulifyChatIdList (chatIds);
		
		try (FileOutputStream fos = new FileOutputStream(filePath); 
				ObjectOutputStream oos = new ObjectOutputStream(fos)) {
			
			if (chatIds.size()==0) {oos.writeLong (1111111); System.out.println("In ChatIdFromFile:50 - next line writen- : " + 111111111);}
			
			for (long l : chatIds) {
				oos.writeLong (l); System.out.println("In ChatIdFromFile:53 - next line writen- : " + l);
				System.out.println("Next writing time: " +timeLimit);
			}
			oos.close();	
		}
		
	}	
		
		return chatIds;
	}

	private static List <Long> nulifyChatIdList (List <Long> chatIds) {
		
		DateFormat simpleDateFormat = new SimpleDateFormat("dd");
		Date date = new Date();
		
		String curDate = simpleDateFormat.format(date);
		
		if ((!lDate.equals(curDate))) {
		    long updateId=0;
		    try {updateId=chatIds.get(0);}
		    catch (Exception e) {}
		    
			chatIds = new ArrayList<>();
			chatIds.add(updateId);
		    lDate=curDate;
			System.out.println("In ChatIdFromFile:73 - Nulified chatIds list");

		}
		return chatIds;
	}


	private static List<Long> readListFromFile() throws FileNotFoundException, IOException {
		
		List<Long> list = new ArrayList<>();
		
		File f = new File(filePath);
		if (f.exists()) {
			
			DateFormat simpleDateFormat = new SimpleDateFormat("dd");
			System.out.println("In ChatIdFromFile:88 - file was last modified " + simpleDateFormat.format(f.lastModified()));
			
			try (FileInputStream istream = new FileInputStream(f)) {
				try (ObjectInputStream ois = new ObjectInputStream(istream)) {
					while (ois.available() > 0) {
						list.add(ois.readLong());
					}
					ois.close();
				}
				istream.close();
			}
		}
		
		for (long l : list) {
			System.out.println("In ChatIdFromFile:102 - read from file: " + l);
		}
		return list;
	}
	
	private static String setNewLastDate () {
		DateFormat simpleDateFormat = new SimpleDateFormat("dd");
		return simpleDateFormat.format(new Date());		 
	}


	private static Calendar initializeDateCounter () {
		Calendar counterPlusHour = Calendar.getInstance();
		counterPlusHour.set(Calendar.HOUR_OF_DAY, counterPlusHour.get(Calendar.HOUR_OF_DAY)+1);
		return counterPlusHour;
		
	}

	
}

