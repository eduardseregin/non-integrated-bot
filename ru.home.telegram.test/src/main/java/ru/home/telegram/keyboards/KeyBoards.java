package ru.home.telegram.keyboards;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.checkerframework.checker.nullness.qual.NonNull;

import ru.home.telegram.test.news.utilities.Factory;


public enum KeyBoards {
	
	REMOVE (0),
	GENERAL (1),
	EXCHANGE (2),
	CHART (3),
	LINGVO (4),
	CASTLE (5),
	WEATHER (6);

	private static List <KeyBoard> keyBoards = initialiseKeys (); 
	
	private int index;

	private KeyBoards(int index) {
		 this.index = index;
		 		
		if (index<0) {
			throw new IllegalArgumentException ("index should be positive");
		}
		
	}

	public String [] [] getKeyBoard () {
		
		if (keyBoards.get(this.index)==null) return null;
		
		return keyBoards.get(this.index).getKeyBoard();
	}
	
	private static List<KeyBoard> initialiseKeys() {
		List <KeyBoard> boards = Factory.emptyList(KeyBoard.class);
		
		boards.add(null);
		
		boards.add(
				new KeyBoard.BoardBuilder("1 - General")
				    .add (new Key.Builder ("Курсы").emojiString("20AC").level(Level.UP).priority(Priority.HIGH).build())
				        .add (new Key.Builder ("Чарт").emojiString("266B").level(Level.UP).build())
				            .add (new Key.Builder ("Удача").emojiString("1F91E").level(Level.UP).build())
				    
				    .add (new Key.Builder ("Лингвомания").emojiString("1F3A7").level(Level.MIDDLE).priority(Priority.HIGH).build())
				        .add (new Key.Builder ("Замок").emojiString("269C").level(Level.MIDDLE).build())
				    
				    .add (new Key.Builder ("Прогноз погоды").emojiString("1F324").level(Level.DOWN).priority(Priority.HIGH).build())
				        .add (new Key.Builder ("Новости (News in English)").emojiString("1F310").level(Level.DOWN).build())
				
				.buildBoard()
				);
		
		boards.add(
				new KeyBoard.BoardBuilder("2 - Exchange")
				    .add (new Key.Builder ("RUB в Эстонии").emojiString("20BD").level(Level.UP).priority(Priority.HIGH).build())
				        .add (new Key.Builder ("EUR в России").emojiString("20AC").level(Level.UP).build())
				  
				    .add (new Key.Builder ("USD в России и Эстонии").emojiString("1F4B2").level(Level.MIDDLE).build())
				    
				    .add (new Key.Builder ("Выход из меню").emojiString("1F6AA").level(Level.DOWN).build())
				
				.buildBoard()
				);
		
		boards.add(
				new KeyBoard.BoardBuilder("3 - Chart")
				    .add (new Key.Builder ("Чарт 50").emojiString("23E9").level(Level.UP).priority(Priority.HIGH).build())
				        .add (new Key.Builder ("Чарт Новое").emojiString("23FA").level(Level.UP).build())
				         				    
				    .add (new Key.Builder ("Чарт первые 15").emojiString("23EF").level(Level.MIDDLE).build())
				    
				    .add (new Key.Builder ("Выход из меню").emojiString("1F6AA").level(Level.DOWN).build())
				
				.buildBoard()
				);
		
		boards.add(
				new KeyBoard.BoardBuilder("4 - Lingvo")
				    .add (new Key.Builder ("Правила").emojiString("1F4DA").level(Level.UP).priority(Priority.HIGH).build())
				        .add (new Key.Builder ("Мой счёт 50").emojiString("1F9EE").level(Level.UP).build())
				          				    
				    .add (new Key.Builder ("Синоним 1").emojiString("2713").level(Level.MIDDLE).priority(Priority.HIGH).build())
				        .add (new Key.Builder ("2").emojiString("2713").level(Level.MIDDLE).build())
				            .add (new Key.Builder ("3").emojiString("2713").level(Level.MIDDLE).build())
				                .add (new Key.Builder ("4").emojiString("2713").level(Level.MIDDLE).build())
				    
				    .add (new Key.Builder ("Скрыть меню").emojiString("2608").level(Level.DOWN).priority(Priority.HIGH).build())
				        .add (new Key.Builder ("ВЫХОД ИЗ ИГРЫ").emojiString("1F64B").level(Level.DOWN).build())
				
				.buildBoard()
				);
		
		
		boards.add(
				new KeyBoard.BoardBuilder("5 - Castle")
				    .add (new Key.Builder ("Правила").emojiString("1F4DA").level(Level.UP).priority(Priority.HIGH).build())
				        .add (new Key.Builder ("Подсказка 50").emojiString("1F449").level(Level.UP).build())
				            .add (new Key.Builder ("0").emojiString("2713").level(Level.UP).build())
				            				    
				    .add (new Key.Builder ("1").emojiString("2713").level(Level.MIDDLE).priority(Priority.HIGH).build())
				        .add (new Key.Builder ("2").emojiString("2713").level(Level.MIDDLE).build())
				            .add (new Key.Builder ("3").emojiString("2713").level(Level.MIDDLE).build())
				                .add (new Key.Builder ("4").emojiString("2713").level(Level.MIDDLE).build())
				    
				    .add (new Key.Builder ("5").emojiString("2713").level(Level.DOWN).priority(Priority.HIGH).build())
				        .add (new Key.Builder ("6").emojiString("2713").level(Level.DOWN).build())
				            .add (new Key.Builder ("7").emojiString("2713").level(Level.DOWN).build())
				                .add (new Key.Builder ("ВЫХОД ИЗ ИГРЫ").emojiString("1F64B").level(Level.DOWN).build())
				
				.buildBoard()
				);
		
		boards.add(
				new KeyBoard.BoardBuilder("6 - Weather")
				    .add (new Key.Builder ("Прогноз погоды в Москве").emojiString("1F3D9").level(Level.UP).priority(Priority.HIGH).build())
				        .add (new Key.Builder ("Погода в Екатеринбурге").emojiString("1F3D4").level(Level.UP).build())
				            				    
				    .add (new Key.Builder ("Погода в Таллине").emojiString("1F306").level(Level.MIDDLE).priority(Priority.HIGH).build())
				        .add (new Key.Builder ("Прогноз погоды в Бургасе").emojiString("1F304").level(Level.MIDDLE).build())
				    
				    .add (new Key.Builder ("Выход из меню").emojiString("1F6AA").level(Level.DOWN).priority(Priority.HIGH).build())
				    .add (new Key.Builder ("Погода в Софии").level(Level.DOWN).build())
				    
				
				.buildBoard()
				);
		
		
				
		return boards;
	}
	
	private static class Key implements Comparable <Key> {

		@NonNull
		private String  keyName;
		private String emojiString;
		private Level level;
		private Priority priority;
		
		
		public static class Builder {
			
			private String keyName;
			private String emojiString;
			private Level level;
			private Priority priority;

			
			public Builder(@NonNull String keyName) {
				keyName = keyName.trim();
				if (!Objects.nonNull(keyName)) {
					throw new IllegalArgumentException ("@keyName should not be null");
				}
				if (keyName.length()==0) {
					throw new IllegalArgumentException ("@keyName should not be empty");
				}
				
				this.keyName = keyName;
				this.emojiString = "";
				this.level = Level.DOWN;
				this.priority = Priority.NORMAL;
			}
			
			public Builder emojiString (String emoji) {
				if (emoji!=null) {
				  emoji = emoji.trim();
				  this.emojiString = emoji;
				}
				return this;
			}
			
			public Builder level (Level val) {
				if (val!=null) {
				  this.level=val;
				}
				return this;
			}
			
			public Builder priority (Priority val) {
				if (val!=null) {
				  this.priority=val;
				}
				return this;
			}
			
			public Key build () {
				return new Key (this);
			}
			
			
		}
		
		
		
		private Key(Builder builder) {
		
			this.keyName = builder.keyName;
			this.emojiString = builder.emojiString;
			this.level = builder.level;
			this.priority = builder.priority;
		}


		
		
		public Level getLevel() {
			return level;
		}




		public String getKey () {
			if (this.emojiString!=null && this.emojiString.length()>1) {
				String space = "";
				if (keyName.length()>11) {
					space = "  ";
				} else {
					space = "   ";
				}
				return keyName + space + new String(Character.toChars(Integer.decode("0x"+emojiString)));
			}
			
			return keyName;
		}
		
		@Override
		public String toString() {
			return "Key [keyName=" + keyName + ", emojiString=" + emojiString + ", level=" + level + ", priority="
					+ priority + "]";
		}


		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((emojiString == null) ? 0 : emojiString.hashCode());
			result = prime * result + ((keyName == null) ? 0 : keyName.hashCode());
			result = prime * result + ((level == null) ? 0 : level.hashCode());
			result = prime * result + ((priority == null) ? 0 : priority.hashCode());
			return result;
		}


	@Override
	public int compareTo (Key other) {
		
		if (this.level == other.level && this.priority == other.priority) {
			return 0;
		}
		
		if (this.level==Level.UP && this.priority==Priority.HIGH) {
			return -1;
		}
		if (this.level == Level.UP && other.level!=Level.UP) {
			return -1;
		}
		if (this.level == Level.MIDDLE && other.level==Level.DOWN) {
			return -1;
		}
		
		if (this.level == other.level && this.priority==Priority.HIGH) {
			return -1;
		}
		
		return 1;

	}

	
	}
	
	private static class KeyBoard {
		
		private String name;
		private List <Key> keys;
		
		
		public static class BoardBuilder {
			
			private String name;
			private List <Key> keys;
			
			public BoardBuilder(String name) {
				this.name = name;
				this.keys = Factory.emptyList(Key.class);
			}
			
			public BoardBuilder add (Key keyMaxNine) {
				
				if (keys.size()==12) {
					throw new IllegalArgumentException ("Only 12 keys are allowed to add");
				}
				
				if ((keys.stream().filter(x->x.getLevel()==Level.UP).count()==4 
				    && keyMaxNine.getLevel() == Level.UP) ||
				        (keys.stream().filter(x->x.getLevel()==Level.MIDDLE).count()==4 
				            && keyMaxNine.getLevel() == Level.MIDDLE)||
				                (keys.stream().filter(x->x.getLevel()==Level.DOWN).count()==4) 
				                    && keyMaxNine.getLevel() == Level.DOWN) {
		
					throw new IllegalArgumentException ("Only 4 keys are allowed in any given level");
				}
				
				if (keyMaxNine!=null) {
				keys.add(keyMaxNine);
				}
				return this;
			}
			
			public KeyBoard buildBoard () {

			    this.keys = this.keys.stream().limit(12).sorted().collect(Collectors.toList());
				List <Key> toAdd = keys.stream().filter(x->x.getLevel()==Level.UP).limit (4).collect (Collectors.toList()); 
				toAdd.addAll(keys.stream().filter(x->x.getLevel()==Level.MIDDLE).limit (4).collect (Collectors.toList()));
				toAdd.addAll(keys.stream().filter(x->x.getLevel()==Level.DOWN).limit (4).collect (Collectors.toList()));
				
				
				this.keys = toAdd;
						
				return new KeyBoard (this);
			}
		}
		
		
		public KeyBoard(BoardBuilder builder) {
		   this.name = builder.name;
		   this.keys = builder.keys;
		}


		public String [] [] getKeyBoard () {
			
			int rows = (int) keys.stream().map(x->x.getLevel()).distinct().count();		
			
			String [] [] array = new String [rows] [];
			
			int count = 0;
			
			for (Level l: Level.values()) {
				
				if (keys.stream().anyMatch(x->x.getLevel()==l)) {
					  List <Key> ofLevel = keys.stream().filter (x->x.getLevel()==l).sorted().collect(Collectors.toList());
					  array [count] = ofLevel.stream().map (x->x.getKey()).toArray(String[]::new);
					  count++;
					} 
			}
			
			
			return array;
		}
		
		@Override
		public String toString() {
			String msg = "KeyBoard [name=" + name + ", keys: " + "\n";
			return keys.stream().map(x->x.toString()).reduce(msg,  (x,y)->""+x+y+"\n");
		}
		
		
	}
}

enum Level {UP, MIDDLE, DOWN}

enum Priority {HIGH, NORMAL}
