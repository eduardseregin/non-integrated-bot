package ru.home.telegram.copy;

import java.util.Arrays;
import java.util.List;

public class User {

	private long chatId;
	private String greeting;
	private boolean isChartMusic;
	private int calendarNumb;
	
	private String city;
	
	private static List <User> users = Arrays.asList(new User (244162579,"Добрый день мне!", "Москве", 1), 
			new User (788570444,"Добрый день, Полюша!", "Москве", false),
			new User (547945689,"Добрый день, Носянечек!","Москве", 2),
			new User (1065740906,"Добрый день, Андрей!", "Таллине", true),
			new User (948017138,"Добрый день, Сергей Юрьевич, рад возвращению!", "Москве", true),
			new User (1502612360,"Папа, привет!", "Екатеринбурге", true),
			new User (1888952688,"Добрый день, мамочка!", "Екатеринбурге", true),
			new User (36136026,"Тимур, добрый день!", "Екатеринбурге", true),
			new User (146795319,"Эля, добрый день!", "Екатеринбурге", true),
			new User (178670924,"Света, привет!", "Екатеринбурге", true),
			new User (139840151,"Добрый день, Олег Юрьевич, приветствую !", "Москве", true),
			new User (374468942,"Добрый день, рады Вашему возвращению !", "Москве", true),
			new User (293770774,"Добрый день, Макс Владимирович!", "Калуге", true)
						
			);
	
	public static String getCity (long chatId) {
		return users.stream().filter (x->x.chatId==chatId).map(x->x.city).findFirst().orElse("Москва");
		
	}
	
	
	//  || chatId == 244162579 - Eduard
	
	// 788570444 - Polysha
	// 547945689 - Alyona
	// 1065740906 - Andrei
	
	
	private User(long chatId, String greeting, String city, boolean isChartMusic) {
		super();
		this.chatId = chatId;
		this.greeting = greeting;
	    this.isChartMusic = isChartMusic;
	    this.calendarNumb=0;
	    this.city=city;
		}
	
	private User(long chatId, String greeting, String city, int calendarNumb) {
		super();
		this.chatId = chatId;
		this.greeting = greeting;
		this.isChartMusic = true;
		this.calendarNumb=calendarNumb;
		this.city=city;
		}
	
	public static String getGreeting (long chatId) {
		String nameOfUser = "";
		nameOfUser = users.stream().filter (x->x.chatId==chatId).map(x->x.greeting).findFirst().orElse("");
		return nameOfUser;
	}
	
	public long getChatId() {
		return chatId;
	}
	
	public String getGreeting() {
		return greeting;
	}
	

	public boolean isChartMusic() {
		return isChartMusic;
	}
	
	
	public static boolean isMusic (long chatId) {
		return users.stream().filter(x->x.chatId==chatId).map(x->x.isChartMusic).anyMatch(x->x);
	}
	
	public static int calendarNumber (long chatId) {
		return users.stream().filter(x->x.chatId==chatId).map(x->x.calendarNumb).findFirst().orElse(0);
	

	}
	
	public static String greeting (long chatId) {
		return users.stream().filter(x->x.chatId==chatId).map(x->x.greeting).findFirst().orElse ("");
	}
}
