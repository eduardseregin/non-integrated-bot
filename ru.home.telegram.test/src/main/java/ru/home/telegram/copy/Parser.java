package ru.home.telegram.copy;

import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

public class Parser {

	private String name;
	private String url;
	private String elem;
	private String div;
	private int relPos;
	private String attr;
	
	public static class Builder {

		private String name;
		private String url="";
		private String elem="";
		private String div="°";
		private int relPos=-1;
		private String attr="";

		public Builder(String name) {this.name=name;}

		public Builder url (String val) {
			this.url = val;
			return this;}

		public Builder elem (String val) {
			this.elem = val;
			return this;}

		public Builder div (String val) {
			div = val;
			return this;}

		public Builder relPos (int val) {
			relPos = val;
			return this;}
		
		public Builder attr (String val) {
			attr = val;
			return this;}

		public Parser build() {
			return new Parser (this);}
	}

	private Parser (Builder builder) {
		name=builder.name;
		url = builder.url;
		elem = builder.elem;
		div = builder.div;
		relPos = builder.relPos;
		attr = builder.attr;
	}
	
	public List <String> getElements () {
		List <String> list=null;
		
		try {
		Document doc = Jsoup.connect(url).get();
		if (this.attr=="")
		list= doc.getElementsByClass(elem).eachText();
		else list= doc.getElementsByClass(elem).eachAttr(this.attr);}
		catch (Exception e) {}
		
				
		return list;
	}
	
	public List <Integer> toIntList ()  {
		
		List <String> parseList = getElements ();
		List <Integer> results = new ArrayList <> ();
		
		// finding up to 3 letters of the number: going backwards
		for (String s: parseList) {
			int endIndex = s.indexOf(div)+relPos;
			if (endIndex>=0) {
				
			String stringToParse="";
				for (int y=0;y<3;y++) {
					if (endIndex-y>=0) 
						if (Character.isDigit(s.charAt(endIndex-y)) || s.charAt(endIndex-y)=='-') {
						stringToParse+=s.charAt(endIndex-y);
					}	
				}
				
			// check if minus sign is ok	
			if (stringToParse.contains("-") && stringToParse.charAt(stringToParse.length()-1)!='-') {
				stringToParse.replace("-", "");
				}
			// change backwards to onwards order of the letters (as the length is <=3)
			if (stringToParse.length()>1) {
				char repl = stringToParse.charAt(stringToParse.length()-1);
				stringToParse = repl + stringToParse.substring(0,stringToParse.length()-1);
				
			}
					
			int parsed = Integer.parseInt(stringToParse);
			results.add(parsed);
				//full stop
			}	
		}
		
		return results;
	}
	
	public String getName () {
		return this.name;
	}


	
}
 
 
