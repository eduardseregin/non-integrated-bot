package ru.home.telegram.copy;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ExchangeRate {

	
private static ExchangeRate cbrRates;
	
	private Map <String, Double> exchangeRates;
	private String establishedDate;
	private Date nextUpdate;
	

	public static String getEurGbpUsdMsg () {
		String msg = "";
		ExchangeRate rates = ExchangeRate.instance();
		
		if (rates.exchangeRates!=null) {
		
		msg+="Банком России (cbr.ru) установлены курсы на " + rates.establishedDate + "\n";
		msg+="  EUR - " + rates.exchangeRates.get("EUR") + "руб."+ "\n";
		msg+="  GBP - " + rates.exchangeRates.get("GBP") + "руб."+ "\n";
		msg+="  USD - " + rates.exchangeRates.get("USD") + "руб."+ "\n";
		}
		
		else msg = "Информация недоступна";
		return msg;
	}
	
	public static String getSpecificRateToUpperCase (String curr) {
		
		if (curr.equals("EUR В РОССИИ  €")) curr = "EUR";
		if (curr.equals("USD В РОССИИ И ЭСТОНИИ  💲")) curr = "USD";
		
		curr=curr.toUpperCase();
		ExchangeRate rates = ExchangeRate.instance();
		
		if (rates.exchangeRates!=null)
		return "Банк России (cbr.ru): с "+ rates.establishedDate +": " + curr + " - " + rates.exchangeRates.get(curr) + "руб.";
		return "Информация недоступна";
	}
	
	private static ExchangeRate instance () {
		if (cbrRates != null && new Date ().compareTo(cbrRates.nextUpdate)<0) {
			return cbrRates;
		}
		return new ExchangeRate ();
	}
	
	private ExchangeRate () {
		
		GregorianCalendar cal = (GregorianCalendar) GregorianCalendar.getInstance();
		cal.add(Calendar.HOUR_OF_DAY, 1);
		this.nextUpdate = cal.getTime();
		
		// gets inf from cbr.ru site
		   List <String> lines = new Parser.Builder("Exchange Rates")
		    		.url("https://cbr.ru/currency_base/daily/")
		    		.elem("data")
		    		.build().getElements();

	if (lines!=null) {
		   // prepares the line and Map
		    String cbRFRates = lines.get(0);
		    cbRFRates=cbRFRates.substring(posEnglCapitalLetterFirst (cbRFRates));
		    exchangeRates = new HashMap <>();
		    
		    // fills the map with data
		    while (posEnglCapitalLetterFirst (cbRFRates)!=0) {

		    	String individualLine = cbRFRates.substring(0, posEnglCapitalLetterFirst (cbRFRates)-1);
		    	
		    	exchangeRates.put(individualLine.substring(0, 3), extractRate(individualLine));
		    	cbRFRates=cbRFRates.substring(posEnglCapitalLetterFirst (cbRFRates));
		   }
		   // adds-up the last line of the inf 
		    if (cbRFRates.length()!=0) exchangeRates.put(cbRFRates.substring(0, 3), extractRate(cbRFRates+" 1"));
			// DONE    
	}
		    this .establishedDate=getEstablishedDate();
		    
		    cbrRates = this;
	}
	
	
	private int posEnglCapitalLetterFirst(String r) {

		int pos = r.length() - 1;

		for (int i = 3; i < r.length(); i++) {
			if (pos == r.length() - 1) {
				for (int y = 65; y < 91; y++) {
					if (r.charAt(i) == ((char) (y))) {
						pos = i;
						break;
					}
				}
			}
		}

		if (pos != r.length() - 1)
			return pos;

		return 0;
	}
    
	private double extractRate(String s) {

		String rate = s.substring(0, s.lastIndexOf(" ") - 1);
		rate = rate.substring(rate.lastIndexOf(" ") + 1);
		rate = rate.replace(',', '.');

		String qtyString = s.substring(s.indexOf(" ") + 1, s.indexOf(" ", s.indexOf(" ") + 1));

		int qty = 1;

		try {
			qty = Integer.parseInt(qtyString);
		} catch (Exception e) {
		}

		try {
			int increasedDouble = (int) (Math.round(Double.parseDouble(rate) * 100 / qty));
			return (double) increasedDouble / 100;

		} catch (Exception e) {
			return 0;
		}
	}

	private static String getEstablishedDate () {
		
	    List <String> date = new Parser.Builder("for Date")
	    		.url("https://cbr.ru/currency_base/daily/")
	    		.elem("datepicker-filter")
	    		.attr("data-default-value")
	    		.build().getElements();
	    
	    if (date!=null)
	    return date.get(0);
	    return "";
	}
}
