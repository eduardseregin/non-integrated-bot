package ru.home.telegram.copy;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import com.pengrad.telegrambot.TelegramBot;
import com.pengrad.telegrambot.model.Message;
import com.pengrad.telegrambot.model.Update;
import com.pengrad.telegrambot.model.request.ParseMode;
import com.pengrad.telegrambot.model.request.ReplyKeyboardMarkup;
import com.pengrad.telegrambot.model.request.ReplyKeyboardRemove;
import com.pengrad.telegrambot.request.GetUpdates;
import com.pengrad.telegrambot.request.SendMessage;
import com.pengrad.telegrambot.response.GetUpdatesResponse;

import ru.home.telegram.copy.files.ChatIdFromFile;
import ru.home.telegram.keyboards.KeyBoards;
import ru.home.telegram.test.game.TelegramService;
import ru.home.telegram.test.news.NewsService;
import ru.home.telegram.test.news.utilities.Words;
import ru.home.telegram.test.saveservice.SaveService;



public class Telegram extends Update implements Credentials {

	private static final long serialVersionUID = 1L;

	static TelegramBot bot = new TelegramBot (Credentials.getTelegramCred());

	static List <Long> chatIds;
	static long updateId=97481483;

	static List <String> songsCalls = Arrays.asList("песни", "songs", "чарт", "list", "радио", "radio", "рок", "наше", "чарт   ♫", "Чарт 50   ⏩", "Чарт Новое   ⏺", "Чарт первые 15  ⏯");
	static List <String> calendarCalls = Arrays.asList("календарь", "дел", "дела");
	// static List <String> weatherCalls = Arrays.asList("погода", "температура", "weather", "temp", "temps", "погоду", "какая сейчас погода", "какая погода", "какая температура");
	static List <String> exchRatesCalls = Arrays.asList("курс", "курсы", "курсы   €", "валюты", "валюта", "цб", "курс валюты");
	static List <String> exchRatesCallsEP = Arrays.asList("exchange", "exchange rate", "eesti pank", "currency", "bank", "rates", "rate");
	static List <String> gameCalls = Arrays.asList("игра","играть","замок","play","game","ghost","castle", "игр");
	static List <String> magicCalls = Arrays.asList("кубик","судьба","magic","шар","магический шар","магический","предсказание", "удачу", "удачи", "удача", "шанс", 
			"шансы", "шансов", "удачный", "удачливый", "удачку", "удачка", "судьбы", "предскажи", "покажи", "предсказывай", "удача   🤞");
	static boolean isFirst=true;
	static List <String> specificRateCalls=Arrays.asList(
			"AUD", "AZN", "AMD", "BYN", "BGN", "BRL", "HUF", "KRW", "HKD", "DKK", "EUR", "INR", "KZT", "CAD", "KGS", "CNY", "MDL", "TMT", "NOK", "PLN", "RON", 
			"SGD", "TJS", "TRY", "UZS", "UAH", "GBP", "CZK", "SEK", "CHF", "ZAR", "JPY", "USD", "EUR в России  €", "USD В РОССИИ И ЭСТОНИИ  💲", "EUR В РОССИИ  €" );
	
	
	static boolean isFirstThisDay = true;
	
	public static void updatesListener () throws Exception {
	
		
		
		chatIds = ChatIdFromFile.updateFile (chatIds);
		if (chatIds !=null) if (chatIds.size()>0)
		 {
			if (User.getGreeting(chatIds.get(0))!="") {
				updateId=(long) 97479108; chatIds.add (0,updateId);
				} else updateId=chatIds.get(0);
		} else
		{chatIds = new ArrayList<>(); updateId = ((long) 97479108); chatIds.add (updateId);}
		
		
		GetUpdates getUpdates = new GetUpdates().limit(200).offset((int) updateId).timeout(0).allowedUpdates("text");

		GetUpdatesResponse updatesResponse = bot.execute(getUpdates);
		List<Update> updates = updatesResponse.updates();
		if (updates==null) updates = new ArrayList<>();
		
		updates = updates.stream().filter(x->x.message().text()!=null).collect(Collectors.toList());
		
		for (Update update : updates) {
				boolean notInService=true;
			
			Message message = update.message();
			Long chatId = message.chat().id();
			
			// chatId++
			if (updateId<=update.updateId()) {updateId=update.updateId()+1;chatIds.set (0,updateId);}
			
			// for debugging only
			if (isFirst) {
				Date msgDate = new Date ();
				msgDate.setTime((long)message.date()*1000);
				System.out.println(update.updateId() +"   " +msgDate +"  "+chatId+":  " + message.text());
			}
			
		if (isRecent (message.date())) {
			
			// HANDLING USER'S REQUESTS LOGIC
			
			if (chatIds.contains(chatId)) {
				System.out.println(update.updateId () + "  "+ chatId + ":  " + message.text());
				
				
//if (GoldHuntGame.isInGame(chatId)) {sendMessage (chatId, GoldHuntGame.gameMessage(chatId, message.text()));} 
//else {
//				if (gameCalls.contains(message.text().toLowerCase())) sendMessage (chatId,GoldHuntGame.gameMessage(chatId, message.text()));
				
				
				
				
				if (message.text().equals("Скрыть меню   ☈") || message.text().toLowerCase().contains("меню") ) {
					if (message.text().equals("Скрыть меню   ☈")) {
					sendMessage (chatId,"Меню скрыто. Введите 'Меню' - для возврата, 'Выход' - для выхода из игры", KeyBoards.REMOVE);
					}
					else {
						if (!message.text().equals("Выход из меню  🚪")) {
						sendMessage (chatId,"Запрошено отображение меню", KeyBoards.LINGVO);
						}
					}
				} else {
				    
					String gameService = TelegramService.getBotResponse(chatId, message.text().toLowerCase());
					if (gameService!=null) {
					  
						if (message.text().equals("ВЫХОД ИЗ ИГРЫ  🙋") 
							   || message.text().toLowerCase().contains("выход") 
							       || gameService.contains("Игра окончена.")) {
						   sendMessage (chatId, gameService, KeyBoards.GENERAL); notInService=false;
				       
					   } else {
				    	   
				    	     if (gameService.contains("ИГРА 'ЛИНГВОМАНИЯ!")) {
				    	    	 sendMessage (chatId, gameService, KeyBoards.LINGVO); notInService=false;
				    	     } else {
				    	    	 if (gameService.contains("ИГРА 'ЗАМОК С ПРИВЕДЕНИЕМ'!")) {
					    	    	 sendMessage (chatId, gameService, KeyBoards.CASTLE); notInService=false;
					    	     } else {
					    	    	 sendMessage (chatId, gameService, Nashlepka.getLast(chatId).board); notInService=false;
					    	     }
				    	     }
				    	 }
				    }
				 }
				
		
				// (songsCalls.contains(message.text().toLowerCase()) && notInService) {if (chatId!=788570444) sendMessage (chatId,RadioMusic.radioMessage());}
				String radio = message.text().toLowerCase();
				if (radio.contains(" ")) radio = radio.substring(0, radio.indexOf(" "));
				if (songsCalls.contains(radio) && notInService) {
					sendMessage (chatId,AdvancedRadioMusic.radioMessage(message.text()),true,KeyBoards.CHART);
					}
				
				if (AdvancedTemperature.isWeatherCall(message.text().toLowerCase()) && !AdvancedTemperature.isWeatherLongCall(message.text().toLowerCase()) && notInService) {
					sendMessage (chatId, AdvancedTemperature.instance(chatId,message.text()).toShortString(),KeyBoards.WEATHER);
				}
				if (AdvancedTemperature.isWeatherCall(message.text().toLowerCase()) && AdvancedTemperature.isWeatherLongCall(message.text().toLowerCase()) && notInService) {
					sendMessage (chatId, AdvancedTemperature.instance(chatId,message.text()).toString(), KeyBoards.WEATHER);
				}
				
				if (exchRatesCalls.contains(message.text().toLowerCase()) && notInService) {
					sendMessage (chatId, ExchangeRate.getEurGbpUsdMsg(), KeyBoards.EXCHANGE);
				}
				if (exchRatesCallsEP.contains(message.text().toLowerCase()) && notInService) {
					sendMessage (chatId, ExchangeRateEP.getEurGbpUsdMsg(), KeyBoards.EXCHANGE);
				}
				
				if (specificRateCalls.contains(message.text().toUpperCase()) && notInService) {
					sendMessage (chatId, ExchangeRate.getSpecificRateToUpperCase(message.text().toUpperCase()), KeyBoards.EXCHANGE);
				}
				if (ExchangeRateEP.getSpecificRateToUpperCase(message.text())!="" && notInService) {
					sendMessage (chatId, ExchangeRateEP.getSpecificRateToUpperCase(message.text()), KeyBoards.EXCHANGE);
				}
				
				if (convertCurrencyCall (message.text().toLowerCase())>=0) {
					sendMessage (chatId, ExchangeRate.getSpecificRateToUpperCase(specificRateCalls.get(convertCurrencyCall (message.text().toLowerCase()))), KeyBoards.EXCHANGE);
				}
				
				if (chatId==244162579 && message.text().toLowerCase().contains("пользоват") && notInService) {
				  sendMessage (chatId,getUsers ());
				}
				if (Words.isContainedInLongLine(message.text().toLowerCase(), magicCalls) && notInService) {
					System.out.println("Im in magic: ");
					sendMessage (chatId, MagicBall.getMagic(), KeyBoards.GENERAL);
				}
				if (NewsService.isNewsCall(message.text().toLowerCase())&& notInService) {
					sendMessage (chatId, NewsService.instance(chatId).newsMessage (message.text().toLowerCase()), KeyBoards.GENERAL);
				}
				if (SaveService.isNewCall(message.text(), chatId)&& notInService) {
					sendMessage (chatId, SaveService.newsMessage(message.text()));
				}
				
//				if (calendarCalls.contains(message.text().toLowerCase())) {
//					
//					int calendarNumber = User.calendarNumber(chatId);
//					String calendarString="";
//					if (calendarNumber!=0) {
//						calendarString=GoogleCalendarService.getAppointments(Credentials.getCredentials(calendarNumber).getCredentials(), 
//						Credentials.getCredentials(calendarNumber).getToken());
//						sendMessage (chatId, calendarString);
//					}
//				}
				
				if (message.text().toLowerCase().contains("сброс")  && notInService) {resetId (chatId);}
			    if (message.text().equals("Выход из меню  🚪") && notInService) sendMessage (chatId,"Возврат в главное меню", KeyBoards.GENERAL);
			   
//}
			//Скрыть меню   ☈
			    
			}	
			else {
					// First introductory message
				chatIds.add(chatId);
				System.out.println(message.text());
					
				if (User.greeting(chatId)!="") {
					if (chatId==547945689)sendFirstMessage (chatId,SpecialGreetings.getGreeting(chatId));
					else sendFirstMessage (chatId,User.greeting(chatId));
				}
				else sendFirstMessage (chatId,"Добрый день!");
					
				sendMessage (chatId, getDateMessage (), KeyBoards.GENERAL);
				sendMessage (chatId, AdvancedTemperature.instance(chatId, User.getCity(chatId)).toMorningString(), KeyBoards.GENERAL);
			}
			
			

			// END OF HANDLING USER'S REQUESTS LOGIC
			
//			DeleteMessage delete = new DeleteMessage(chatId, message.messageId()-1);
//			bot.execute(delete);
		 }
		
			// message.messageAutoDeleteTimerChanged().messageAutoDeleteTime();
		}
		
		isFirst=false;
	}
	
	
public static String sendMessage (Long chatId, String text, KeyBoards board) {
	
	Nashlepka.getLast(chatId).board=board;
	
	if (text!="") {
		SendMessage msg = new SendMessage(chatId,
		          text);
		
		if (board == KeyBoards.REMOVE) {
		
		  ReplyKeyboardRemove remove = new ReplyKeyboardRemove ();
		  msg.replyMarkup(remove);
		
		} else {
		
          String [] [] s = board.getKeyBoard();
			
            
		  ReplyKeyboardMarkup im = new ReplyKeyboardMarkup (s);
		  im.oneTimeKeyboard(true); im.resizeKeyboard(true);
		  im.resizeKeyboard(true); im.selective(false);
		
		  msg.replyMarkup(im); 
			
		}
		
		String msgSentParams = bot.execute(msg).toString();
		return msgSentParams;
	}
		return null;
		
		
}

public static String sendMessage (Long chatId, String text, boolean isHtml, KeyBoards board) {
	
	Nashlepka.getLast(chatId).board=board;
	
	if (text!="") {
	ParseMode html = ParseMode.HTML;
	SendMessage msg = new SendMessage(chatId,
	          text).parseMode (html);
            
	
	if (board == KeyBoards.REMOVE) {
		
		  ReplyKeyboardRemove remove = new ReplyKeyboardRemove ();
		  msg.replyMarkup(remove);
		
		} else {
		
        String [] [] s = board.getKeyBoard();
			
          
		  ReplyKeyboardMarkup im = new ReplyKeyboardMarkup (s);
		  im.oneTimeKeyboard(true); im.resizeKeyboard(true);
		  im.resizeKeyboard(true); im.selective(false);
		
		  msg.replyMarkup(im); 
			
		}
	
	String msgSentParams = bot.execute(msg).toString();
	return msgSentParams;
	}
	return null;
}
	
	public static String sendMessage (Long chatId, String text) {
		
		// if (isFirstThisDay) return sendFirstMessage (chatId,text);
		
		
		
		if (text!="") {
		SendMessage msg = new SendMessage(chatId,
		          text);
		
	
		String msgSentParams = bot.execute(msg).toString();
		return msgSentParams;
		}
		return null;
	}
	
	public static String sendMessage (Long chatId, String text, boolean isHtml) {
		if (text!="") {
		ParseMode html = ParseMode.HTML;
		SendMessage msg = new SendMessage(chatId,
		          text).parseMode (html);
                
		
//		String [] [] s = {{""}};
//		ReplyKeyboardMarkup im = new ReplyKeyboardMarkup (s);
//		im.oneTimeKeyboard(true); im.resizeKeyboard(true);
//		im.resizeKeyboard(true); im.selective(true);
//		
//		msg.replyMarkup(im); 
		
		String msgSentParams = bot.execute(msg).toString();
		return msgSentParams;
		}
		return null;
	}
	
	public static String sendFirstMessage (Long chatId, String text) {
		if (text!="") {
		SendMessage msg = new SendMessage(chatId,
		          text);
		
		
		
		String [] [] s = KeyBoards.GENERAL.getKeyBoard();
			
//			{{"Курсы   " +  new String(Character.toChars(0x20AC)), "Чарт   " + new String(Character.toChars(0x266B)), 
//			"Удача    " +  new String(Character.toChars(0x1F91E))}, 
//				{"Лингвомания   " + new String(Character.toChars(0x1F3A7)), "Замок   " + new String(Character.toChars(0x269C))}, 
//				{"Прогноз погоды  " + new String(Character.toChars(0x1F324)),"Новости (News in English)  " + new String(Character.toChars(0x1F310))}};
            
		ReplyKeyboardMarkup im = new ReplyKeyboardMarkup (s);
		im.oneTimeKeyboard(true); im.resizeKeyboard(true);
		im.resizeKeyboard(true); im.selective(false);
		
		msg.replyMarkup(im); 
		
		String msgSentParams = bot.execute(msg).toString();
		return msgSentParams;
		}
		return null;
	}
	
		
	// Can delete message (sent and received - by Id)
//	private static void deleteMessage (Long chatId, int messageId) {
//		if (messageId!=0) {
//		System.out.println("delete: "+messageId);
//		DeleteMessage delete = new DeleteMessage(chatId, messageId);
//		bot.execute(delete);}
//	}
//	

	// Potentially if needed can get sentMsg Id
//	private static int getSentMessageId (String botExecuteMsg) {
//		
//		int start = botExecuteMsg.indexOf("id=")+3;
//		int end = botExecuteMsg.indexOf(",", start);
//		if (start!=-1 && end != -1)
//		botExecuteMsg = botExecuteMsg.substring (botExecuteMsg.indexOf("id=")+3, botExecuteMsg.indexOf(","));
//		
//		int msgId = 0;
//		try {msgId = Integer.parseInt(botExecuteMsg);}
//		catch (Exception e) {};
//		
//		return msgId;
//	}
	
	private static String getDateMessage () {
		DateFormat dateFormat = new SimpleDateFormat("dd MM yyyy");
		return "Сегодня " + dateFormat.format(new Date ());
	}
	
	
	private static boolean isRecent (Integer dateUnix) {
		
		Date dateOfMessage = new Date ();
		dateOfMessage.setTime((long)dateUnix*1000);
		
		Date modified = new Date(System.currentTimeMillis() - 3500);
		
		return modified.compareTo(dateOfMessage)<0;
	}
	
	private static String getUsers () {
		String users="";
		for (long chId: chatIds) {
			if (User.getGreeting(chId)!="") users+=User.getGreeting(chId);
			else users+=""+chId;
			users+="\n";
		}
		return users;
	}
	
	private static void resetId (long chatId) {

		if (chatIds.contains(chatId)) {
			
			chatIds=chatIds.stream().filter(x->x!=chatId).collect(Collectors.toList());
			}
	}

	private static int convertCurrencyCall (String message) {
		List <String> currNames = Arrays.asList("австралийский доллар", "манат", "драм", "белорусский рубль", "лев", 
				"реал", "форинт", "вон", "гонконгский доллар", "датская крона", "евро", "рупий", "тенге", "канадский доллар", "сом", "юань", "молдавский лей", "манат", "норвежская крона", "злотый", "румынский лей", "сингапурский доллар", "сомони",
				"лира", "сум", "гривна", "фунт", "чешская крона", "шведская крона", "франк", "рэнд", "йена", "доллар" );
	
		int i=0;
		
		for (String s : currNames) {
			
			if (message.contains(s)) {return i;}
			i++;
		}
		
		return -1;
	}
	
	static class Nashlepka {
		
		public static List <Nashlepka> list = new ArrayList<>();
		public long chatId;
		public KeyBoards board;
		
		private Nashlepka(long chatId, KeyBoards board) {
			super();
			this.chatId = chatId;
			this.board = board;
			list.add(this);
		}
		
		public static Nashlepka getLast (long chatId) {
			return list.stream().filter(x->x.chatId==chatId).findFirst().orElse(new Nashlepka (chatId, KeyBoards.GENERAL));
		}
		
	}
	
}
