package ru.home.telegram.copy;


import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.GeneralSecurityException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.DateTime;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.calendar.Calendar;
import com.google.api.services.calendar.CalendarScopes;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.Events;

public class GoogleCalendarService  {

	private static final String APPLICATION_NAME = "Google Calendar API Java Quickstart1";
	private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();

	/**
	 * Global instance of the scopes required by this quickstart. If modifying these
	 * scopes, delete your previously saved tokens/ folder.
	 */
	private static final List<String> SCOPES = Collections.singletonList(CalendarScopes.CALENDAR_READONLY);

	// List the next 10 events from the primary calendar.
	public static String getAppointments(String credentialsFilePath, String tokensDirectoryPath)
			throws IOException, Exception {

		String string="";
		Calendar service = buildAPI(credentialsFilePath, tokensDirectoryPath);
		
		DateTime morning = new DateTime(System.currentTimeMillis()-360000);
		Events events = service.events().list("primary").setMaxResults(15).setTimeMin(morning).setOrderBy("startTime")
				.setSingleEvents(true).execute();
		List<Event> items = events.getItems();
		if (items.isEmpty()) {
			string+="В календаре нет событий" +"\n";
		} else {
			// System.out.println("Upcoming events");
			
			DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
			String today = dateFormat.format(new Date ());
			
			List<String> wholeDayEvents = new ArrayList<>();
			List<String> dayEvents = new ArrayList<>();

			for (Event event : items) { 
							
				DateTime start = event.getStart().getDateTime();
				if (start == null) {
   				start = event.getStart().getDate();
   				
   				String calDate = dateFormat.format(new Date (event.getStart().getDate().getValue()));
   									
   				if (today.equals(calDate))
   				wholeDayEvents.add(event.getSummary());
				} else {
					
					String calDate = dateFormat.format(new Date (start.getValue()));
					
					if (today.equals(calDate)) {
					DateFormat timeFormat = new SimpleDateFormat("HH:mm");
					String timeString = timeFormat.format(new Date (start.getValue()));
					dayEvents.add(timeString + "  " + event.getSummary());
				}}
					
				//System.out.printf("%s (%s)\n", event.getSummary(), start);
			}
			
			if (wholeDayEvents.size()>0) {string+="На весь день:" + "\n";
			for (String s: wholeDayEvents) {string+=" * " + s;}
			string+="\n";}
			
			
			if (dayEvents.size()>0) {string+="Встречи на сегодня:";
			for (String s: dayEvents) {string+="\n"+" " + s;}}
			else string+="Встреч на сегодня нет"+"\n";
		}
		return string;
	}

	/**
	 * Creates an authorized Credential object.
	 * 
	 * @param HTTP_TRANSPORT The network HTTP Transport.
	 * @return An authorized Credential object.
	 * @throws IOException If the credentials.json file cannot be found.
	 */
	private static Credential getCredentials(final NetHttpTransport HTTP_TRANSPORT, String credentialsFilePath,
			String tokensDirectoryPath) throws IOException {
		// Load client secrets.
		InputStream in = Telegram.class.getResourceAsStream(credentialsFilePath);
		if (in == null) {
			throw new FileNotFoundException("Resource not found: " + credentialsFilePath);
		}
		GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(in));

		// Build flow and trigger user authorization request.
		GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(HTTP_TRANSPORT, JSON_FACTORY,
				clientSecrets, SCOPES)
						.setDataStoreFactory(new FileDataStoreFactory(new java.io.File(tokensDirectoryPath)))
						.setAccessType("online").build();
		LocalServerReceiver receiver = new LocalServerReceiver.Builder().setPort(8888).build();
		return new AuthorizationCodeInstalledApp(flow, receiver).authorize("user");
	}

	// Build a new authorized API client service.
	public static Calendar buildAPI(String credentialsFilePath, String tokensDirectoryPath)
			throws GeneralSecurityException, IOException {
		final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
		Calendar service = new Calendar.Builder(HTTP_TRANSPORT, JSON_FACTORY,
				getCredentials(HTTP_TRANSPORT, credentialsFilePath, tokensDirectoryPath))
						.setApplicationName(APPLICATION_NAME).build();
		return service;
	}

}

