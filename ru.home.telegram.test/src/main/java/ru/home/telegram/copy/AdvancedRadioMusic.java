package ru.home.telegram.copy;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.stream.Collectors;

public class AdvancedRadioMusic {

	private static final String Path = "C:\\Temp\\AdvancedRadio\\chartNow.txt";
	private static final String PathOLD = "C:\\Temp\\AdvancedRadio\\chartLast.txt";
	private static final int DefaultQty = 5;
	private static List <String> rating;
	private static List <String> ratingOld;
	private static GregorianCalendar trigger;
	
	public static String radioMessage (String msg) {
		
		if (msg.equals("Чарт первые 15  ⏯")) {
			msg = "чарт 15";
		}
		
		if (msg.equals("Чарт 50   ⏩")) {
			msg = "чарт 50";
		}
		
		if (msg.equals("Чарт Новое   ⏺")) {
			msg = "чарт новое";
		}
		
		updateRating ();
		if (rating.size()==0) return "";
		
		int qty = getQty (msg);
		System.out.println(qty);
		String announcement = "";
		for (int i=0; i<Math.min(qty, rating.size()); i++) {
			if (isNew (rating.get(i))) announcement +="<b>" + 
		rating.get(i) +"</b>"+  "  " +new String(Character.toChars(0x1F423)) +  "\n";
			else if (!msg.contains("нов")) announcement +=rating.get(i) + "\n"; 
		}
		
		if (qty>rating.size()) announcement+="\n"+ " -- В рейтинге меньше запрошенного числа позиций";
		announcement+="\n"+" на сегодня по данным анализа 12 000 радиостанций направления 'рок' по всему миру,";
		announcement+="  опубликованных onlineradiobox.com."+"\n"+"\n";
		announcement+="--- Запросите 'чарт ' плюс цифры от 1 до 50 для получения нужного количества позиций рейтинга."+"\n";
		
		return announcement;
	}
	
private static boolean isNew (String s)	{
	if (s.contains(".") && s.contains("("))
	s=s.substring(s.indexOf(".")+2, s.indexOf("(")-1);
	
	for (String r: ratingOld) {
		r = r.substring(r.indexOf(".")+2, r.indexOf("(")-1);
		if (r.contains(s)) return false;
	}
	return true;
}
	
private static int getQty(String msg) {
	if (msg.length()<2) return DefaultQty;
	if (msg.contains("нов")) return 50;
	
	if (Character.isDigit(msg.charAt(msg.length()-1))) {
		int qty = Integer.parseInt(msg.charAt(msg.length()-1)+"");
		if (Character.isDigit(msg.charAt(msg.length()-2))) qty+= Integer.parseInt(msg.charAt(msg.length()-2)+"")*10;
		if (qty>0 && qty<=50) return qty;
	}
		return DefaultQty;
	}


private static void updateRating() {
		updateTrigger ();
		Date now=new Date ();
		
		if (now.compareTo(trigger.getTime())>0 || rating == null) {
			
			List <String> tryLoad = fromFile (Path);
			if (tryLoad!=null || rating==null) {rating = tryLoad;}
			
			tryLoad = fromFile (PathOLD);
			if (tryLoad!=null || ratingOld==null) {ratingOld = tryLoad;}
			
			
			trigger=null; updateTrigger ();
		}
		
	}


private static boolean updateTrigger() {
	if (trigger!=null) return false;
	GregorianCalendar c = new GregorianCalendar ();

	int hh = c.get(Calendar.HOUR_OF_DAY);
	
	c.set(Calendar.HOUR_OF_DAY, 7);
	c.set(Calendar.MINUTE, 5);
	if (hh>=7) c.add(Calendar.DATE, 1);
	
	trigger=c;
	return true;
}


private static List <String> fromFile (String path) {
		
		List <String> strings = new ArrayList<>();
		
try {
		File f = new File(path);
		if (f.exists()) {
					
			try (FileInputStream istream = new FileInputStream(f)) {
							strings = new BufferedReader(new InputStreamReader(istream))
								   .lines().collect(Collectors.toList());
					istream.close();
			}
		}
} catch (Exception e) {}

	return strings;
		
	}

}

