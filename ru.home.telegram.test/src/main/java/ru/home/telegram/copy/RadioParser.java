package ru.home.telegram.copy;


import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

public class RadioParser {

	private final static String elem = "station-chart";

	public static List <String> getChartForPublic (String url)  {
		
		Elements forThisUrl = getElements (url);
		
		if (forThisUrl!=null)
			return getChartList (forThisUrl.toString());
		return new ArrayList <>();
	}

	private static Elements getElements (String url) {
		Elements elems = null;
		try {
		Document doc = Jsoup.connect(url).get();
		elems= doc.getElementsByClass(elem);
		} catch (Exception e) {}
		
		return elems;
	}
	

	private static List <String> getChartList (String str) {
		
		List <String> chart = new ArrayList<>();
		
		int startIndex=0;
		int endIndex=0;
		
		for (int i=1; i<=5;i++) {
		
		startIndex = str.indexOf("alt=", endIndex)+5;
		endIndex = str.indexOf(">",startIndex)-1;
		
		if (startIndex>=0 && endIndex>startIndex) {
		
		chart.add(str.substring(startIndex, endIndex));
		}
		}
		
		return chart;		
	}
	
	
	//TODO
	@Override
	public String toString () {
		String chart="";
		
//		chart+=this.id + "  " + this.name + ": "; 
//		try {
//			for (String s: getChartList (getElements ().toString())) {
//			chart+="\n   - " + s;}
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		
		return chart;
	}
		
}

