package ru.home.telegram.copy.radio;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import ru.home.telegram.copy.RadioParser;


public class RadioBox {

	public static boolean updateChart() {

		String path = "C:\\Temp\\AdvancedRadio\\stationsList.txt";

		String pathArchive = "C:\\Temp\\AdvancedRadio\\chartArchive.txt";
		String pathLast = "C:\\Temp\\AdvancedRadio\\chartLast.txt";
		String pathNow = "C:\\Temp\\AdvancedRadio\\chartNow.txt";

		List<String> radios = fromFile(path);
		System.out.println(radios.size());
		
		
		List<String> songs = new ArrayList<>();
		Set<String> singleSongs = new TreeSet<>();

		for (int i = 0; i < radios.size(); i++) {

			if (i % 1000 == 0)
				System.out.println(i);

			List<String> sampleChart = RadioParser.getChartForPublic(radios.get(i));
			if (sampleChart != null)
				if (sampleChart.size() >= 3) {

					songs.addAll(sampleChart);
					singleSongs.addAll(sampleChart);

				}

		}

		toFile(pathArchive, fromFile(pathLast));
		toFile(pathLast, fromFile(pathNow));

		RadioAnalyse.saveChart(songs);
		return true;
	}

	private static List <String> fromFile (String path) {
		
		List <String> strings = new ArrayList<>();
		
		try {
		File f = new File(path);
		if (f.exists()) {
					
			try (FileInputStream istream = new FileInputStream(f)) {
							strings = new BufferedReader(new InputStreamReader(istream))
								   .lines().collect(Collectors.toList());
					istream.close();
			}
		} }
		catch (Exception e) {}
	return strings;
		
	}

	private static void toFile (String path, List <String> strings) {

		try {
		
		FileOutputStream fileOutputStream = new FileOutputStream(path);

	     for (String s: strings) {
	       s+="\n";
	       fileOutputStream.write(s.getBytes());
	     }

	    fileOutputStream.close();
		} catch (Exception e) {}
	}
	
}

