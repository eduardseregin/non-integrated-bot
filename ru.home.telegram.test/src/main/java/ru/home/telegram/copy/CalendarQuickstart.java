package ru.home.telegram.copy;


import java.net.InetSocketAddress;
import java.net.Socket;

public class CalendarQuickstart {

	static boolean iStatus=true;
	
	public static void main(String[] args) throws Exception {
	
		
		boolean iConnection;
		
		while (true) {
		
		Thread.sleep(1200);
		
		iConnection = checkInternetConnection();
		if (iStatus!=iConnection) {
			iStatus=iConnection;
			if (!iConnection) {System.out.println("Internet connection is lost");}
		else {System.out.println("Internet connection is restored");}}
		
		if (iConnection) {
		
			// THE MAIN LINE
			
			
			try {
				Telegram.updatesListener();
			} catch (Exception e) {
				e.printStackTrace();
			} 
		}
		
		
		}
	
		
		
		
	}

	public static boolean checkInternetConnection()
	{
	     boolean status = false;
	     Socket sock = new Socket();
	     InetSocketAddress address = new InetSocketAddress("www.google.com", 80);

	     try
	     {
	        sock.connect(address, 3000);
	        if(sock.isConnected()) status = true;
	     }
	     catch(Exception e)
	     {
	         status = false;       
	     }
	     finally
	     {
	        try
	         {
	            sock.close();
	         }
	         catch(Exception e){}
	     }

	     return status;
	}
	
}
