package ru.home.telegram.copy;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class MagicBall {

	private String magic; 
	
	private static List <String> positive = Arrays.asList(
			"It is certain (Бесспорно)",
			"It is decidedly so (Предрешено)",
			"Without a doubt (Никаких сомнений)",
			"Yes — definitely (Определённо да)",
			"You may rely on it (Можешь быть уверен в этом)",
			"As I see it, yes (Мне кажется — «да»)",
			"Most likely (Вероятнее всего)",
			"Outlook good (Хорошие перспективы)",
			"Signs point to yes (Знаки говорят — «да»)",
			"Yes (Да)");
	private static List <String> negative = Arrays.asList(
			"Reply hazy, try again (Пока не ясно, попробуй снова)",
			"Ask again later (Спроси позже)",
			"Better not tell you now (Лучше не рассказывать)",
			"Cannot predict now (Сейчас нельзя предсказать)",
			"Concentrate and ask again (Сконцентрируйся и спроси опять)",

			"Don’t count on it (Даже не думай)",
			"My reply is no (Мой ответ — «нет»)",
			"My sources say no (По моим данным — «нет»)",
			"Outlook not so good (Перспективы не очень хорошие)",
			"Very doubtful (Весьма сомнительно)");
	
	public MagicBall () {
		List <String> ball = new ArrayList <> ();
		ball.addAll(positive);
		ball.addAll(negative);
		
		int index = (int) (Math.random()*ball.size());
		this.magic=ball.get(index);
	}
	
	public static String getMagic () {
		return new MagicBall ().magic +"\n" + "/ Источник фраз:  wikipedia.org /";
		}
	

	
	
	
	
}

