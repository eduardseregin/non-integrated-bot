    package ru.home.telegram.copy.radio;

import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

public class RadioAnalyse {

	public static void analyzeChart (List <String> songs)  {
		String pathToSave = "C:\\Temp\\AdvancedRadio\\chartNow.txt";

		List <String> songsCorrected = new ArrayList<>();
		Set <String> single = new TreeSet <> ();
		
		for (String s: songs) {
			
			if (isThreeLetters (s) && s.length()>=5 && !s.contains("station-chart") && !s.startsWith("-")
					&& s.contains(" - ")) {
				
				if (s.contains(":")) s=s.substring(0,s.indexOf(":"));
				if (s.contains(";") && s.length()>s.indexOf(";")+1) s=s.substring(s.indexOf(";")+2);
				if (s.contains("&amp;")) s=s.substring(0, s.indexOf("&amp;"));
				if (s.contains("*") && s.indexOf("*")!=0) s=s.substring(0,s.indexOf("*")) +
						s.substring(0, s.indexOf("*")+1);
				if (s.contains("*")) s=s.substring(0, s.indexOf("*")+1);
				if (s.contains("(") && s.contains(")") && s.indexOf("(")<s.indexOf(")")) {
					s=s.substring(0, s.indexOf("(")) + s.substring(s.indexOf(")")+1);
				}
				
				if (s.contains("[") && s.contains("]") && s.indexOf("[")<s.indexOf("]")) {
					s=s.substring(0, s.indexOf("[")) + s.substring(s.indexOf("]")+1);
				}
				
				if (s.contains("@")) s=s.substring(0, s.indexOf("@")) + s.substring(s.indexOf("@")+1);
				
				if (s.contains(" - - ")) s=s.substring(s.indexOf(" - - ")+5);
				if (s.contains("coronatestID")) s=s.replace("coronatestID", "Mercy");

				if (s.contains("Radio") && s.substring(0,s.indexOf("Radio")).contains("-") 
						&& s.lastIndexOf("-")>s.indexOf("Radio")) {
				
					int ind = s.indexOf("Radio")-1;
					ind = s.indexOf("-", ind);
					String lastPart = s.substring(ind);
					String firstPart = s.substring(0,s.indexOf("Radio"));
					firstPart = firstPart.substring(0, firstPart.lastIndexOf("-"));
					s = firstPart+lastPart;
				}
				
				
				if (s.length()>=3) {
				while (Character.isDigit(s.charAt(0))) s=s.substring(1);
				while (s.indexOf(" ")==0 ) s=s.substring(1);
				}


				s=deleteDigit(s);
				
				if (s.contains(" - ") && s.length()>=5) {
					
					s = s.replace(" - ", "-#-");
					s=s.strip();
					s = s.replace("-#-", " - ");
					s = replaceAllCapital(s);
				}
				
				
				if (s.length()>=3 && s.contains(" - ")) {
					
					
					songsCorrected.add(s);
				single.add(s);}
			}
			
		}
		
		songs = new ArrayList <> ();
		List <Integer> repNumber = new ArrayList <> ();
		
		for (String s: single) {
			int count = 0;
			
			for (String b: songsCorrected) {
				if (b.contains(s)) count++;
			}
			
			if (count>=15) {songs.add(s); repNumber.add(count);}
		}
		
		toFile (pathToSave,getChartSorted (repNumber,songs).stream().limit (50).collect(Collectors.toList()));
		
		//Log
		getChartSorted (repNumber,songs).stream().limit (5).forEach(x->System.out.println(x));
		
	}

	private static String replaceAllCapital (String s) {
		
		int index=s.length();
		String string = "";
		
		for (int i=s.length()-1; i>0;i--) {
			if (s.charAt(i)=='-') break;
			index=i;
			String el=s.charAt(i)+"";
			if (s.charAt(i-1)==' ') el=el.toUpperCase();
			else el=el.toLowerCase();
			string += el;	
		}
		String revString = "";
		for (int i=string.length()-1; i>=0; i--) {
			revString+=string.charAt(i)+"";
		}
		
		string = s.substring(0, index) + revString;
		
		return string;
	}
	
	private static String deleteDigit (String s) {
		if (s.length()<3) return s;
		
		String result = "";
		for (int i=0;i<s.length()-1;i++) {
			if (!Character.isDigit(s.charAt(i)) || Character.isDigit(s.charAt(i+1)) || s.charAt(i+1)==' ') {
				result+=s.charAt(i)+"";
				}
		}
		result+=s.substring(s.length()-1);
		
		return result;
		
	}
	
	private static List <String> getChartSorted (List <Integer> indexes, List <String> songs) {
		List <String> chart = new ArrayList <> ();
		
		int bubble=0; 
		String bubbleString = "";
		
		for (int i=indexes.size(); i>0;i--) {
			for (int z=1;z<i;z++) {
				if (indexes.get(z) > indexes.get(z-1)) {
					
					bubble = indexes.get(z); 
					indexes.set(z, indexes.get(z-1)); 
					indexes.set(z-1, bubble); 
					
					bubbleString = songs.get(z); 
					songs.set(z, songs.get(z-1)); 
					songs.set(z-1, bubbleString); 
				};
			}}

		for (int i=0; i<songs.size();i++) {
			if (indexes.get(i)%10!=2 && indexes.get(i)%10!=3 && indexes.get(i)%10!=4) 
				chart.add((i+1)+". " + songs.get(i) + " (встречается - " + indexes.get(i) + " раз)");
			else chart.add((i+1) +". " + songs.get(i) + " (встречается - " + indexes.get(i) + " раза)");
		}
		
		return chart;
	}
	
	private static boolean isThreeLetters (String string) {
		int count=0;
		for (int i=0; i<string.length();i++) { 
			if (Character.isLetter(string.charAt(i))) count++;
			if (count>=3) return true;
		}
		
		return false;
	}
	
	
	private static void toFile (String path, List <String> strings) {

		try {
		
		FileOutputStream fileOutputStream = new FileOutputStream(path);

	     for (String s: strings) {
	       s+="\n";
	       fileOutputStream.write(s.getBytes());
	     }

	    fileOutputStream.close();
		} catch (Exception e) {}
	}
}