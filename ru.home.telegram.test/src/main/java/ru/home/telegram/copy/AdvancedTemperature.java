package ru.home.telegram.copy;


import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import ru.home.telegram.test.news.utilities.Factory;


public class AdvancedTemperature {

	public static Map <Integer, AdvancedTemperature> singletons;

	public Date nextUpdate;
	
	private static int counter=1;
	private static int thisDayOfWeek;
	private int id;

	private String city;
	private int tempNow;
	private List <Integer> tempHourly;
	private List <Boolean> rain;
	private List <Boolean> snow;
	
	private List <Integer> highTempWeek;
	private List <Integer> lowTempWeek;
	private List <Boolean> rainWeek;
	private boolean isWeekForecastStartsFromToday;
	
	private int maxToday;
	private int minToday;
	
	
	private static List <String> refs=getRefList ();
	private static List <String> cityNames=getCityNamesList();
	private static List <String> weatherCalls = Arrays.asList("погод", "темпер", "weather", "temp");
	private static List <String> weatherLongCalls = Arrays.asList("полн", "full", "целик", "прогноз", "недел", "Прогноз погоды в Москве  🏙");
	
	public static boolean isWeatherCall (String msg) {
		
		for (String w: weatherCalls) {
			if (msg.contains(w)) return true; }
		
		return false;
		
	} 
	
public static boolean isWeatherLongCall (String msg) {
		
		for (String w: weatherLongCalls) {
			if (msg.contains(w)) return true; }
		
		return false;
		
	}

public static AdvancedTemperature instance (long chatId, String msg) {
	int cityInd = getCityIndex(msg, chatId);
	
	if (singletons==null) {
		singletons = new HashMap <> ();
	}

	if (singletons.get(cityInd)!=null) {
		if (new Date ().compareTo(singletons.get(cityInd).getNextUpdate())<0) {
			return singletons.get(cityInd);
		}
	}
	
	AdvancedTemperature sample = new AdvancedTemperature (chatId, msg);
	singletons.put(cityInd, sample);
	
return sample;
}
	
	private AdvancedTemperature (long chatId, String msg) {
		GregorianCalendar.getInstance();
		thisDayOfWeek = GregorianCalendar.getInstance().get(Calendar.DAY_OF_WEEK) - 
				GregorianCalendar.getInstance().getFirstDayOfWeek()+1;
	    
		
		this.id=counter++;
		this.tempNow=0;
		this.rain=Factory.emptyList(Boolean.class);
		this.snow = Factory.emptyList(Boolean.class);
		
		int cityInd = getCityIndex (msg, chatId);
		
		String cityRef = "ru/moscow/294021";
		if (refs!=null) if (refs.size()>0)
			cityRef = refs.get(cityInd);
		
		this.city=cityNames.get(cityInd);
		
		String endRef = cityRef.substring(cityRef.lastIndexOf("/")+1);
		
		if (!getWeather (cityRef, endRef)) {
			getWeather ("ru/moscow/294021", "294021");
			this.city="в Москве (погода по запросу '"+msg+"' - недоступна."+"\n";
		}
	   
		// temporary
		isWeekForecastStartsFromToday = isFromToday(cityRef, endRef);
		getWeekForecast(cityRef, endRef);
		
		GregorianCalendar next = (GregorianCalendar) GregorianCalendar.getInstance();
		next.add(Calendar.HOUR, 1); 
			
		this.nextUpdate = next.getTime();
	 
	}
	
	public Date getNextUpdate() {
		return nextUpdate;
	}
	
	private boolean getWeather (String cityRef, String endRef) {
		boolean isOk = false;
		
		try {
			
			 List <Integer> tempsNow = new Parser.Builder("Temperature that is now")
			    		.url("https://www.accuweather.com/ru/"+cityRef+"/weather-forecast/"+endRef)
			    		.elem("recent-location__temp")
			    		.build().toIntList();
			    if (tempsNow!=null) if (tempsNow.size()>0) this.tempNow=tempsNow.get(0); 
			    
//			    List <Integer> hours = new Parser.Builder("Hours for hourly temperature")
//			    		.url("https://www.accuweather.com/ru/ru/moscow/294021/hourly-weather-forecast/294021")
//			    		.elem("date")
//			    		.div(" ")
//			    		.build().toIntList();
			    
			    tempHourly = new Parser.Builder("Temperature for hourly temperature")
			    		.url("https://www.accuweather.com/ru/"+cityRef+"/hourly-weather-forecast/"+endRef)
			    		.elem("temp metric")
			    		.build().toIntList();
			    
			    List <Integer> weathersCurrent = new Parser.Builder("Weather for search of rain NOW")
			    		.url("https://www.accuweather.com/ru/"+cityRef+"/weather-forecast/"+endRef)
			    		.elem("header-weather-icon")
			    		.div(".svg")
			    		.attr("src")
			    		.build().toIntList();
			    
			 		    
			    if (weathersCurrent.get(0)>=12 && weathersCurrent.get(0)<=15 ) this.rain.add(true);
		    	else this.rain.add(false);
			    
			    if (weathersCurrent.get(0)==19 || weathersCurrent.get(0)==26 || weathersCurrent.get(0)==29  ) this.snow.add(true);
		    	else this.snow.add(false);
			    
			    List <Integer> weathers = new Parser.Builder("Weather for search of rain")
			    		.url("https://www.accuweather.com/ru/"+cityRef+"/hourly-weather-forecast/"+endRef)
			    		.elem("weather-icon icon")
			    		.div(".svg")
			    		.attr("data-src")
			    		.build().toIntList();
			    
			    			    
		    
			    for (int i=0; i<weathers.size(); i++) {
			    	if (weathers.get(i)>=12 && weathers.get(i)<=15 ) this.rain.add(true);
			    	else this.rain.add(false);
			    	
			    	if (weathers.get(i)==19 || weathers.get(i)==26 || weathers.get(i)==29  ) this.snow.add(true);
			    	else this.snow.add(false);
			    }
		
			isOk = true;
		} catch (Exception e) {}
		
	return isOk;
	}
	
	private boolean getWeekForecast (String cityRef, String endRef) {
		
		boolean isOk = false;
		
		try {
		
			 highTempWeek = new Parser.Builder("Temperature for days of the week high temperature")
			    		.url("https://www.accuweather.com/ru/"+cityRef+"/daily-weather-forecast/"+endRef)
			    		.elem("high")
			    		.build().toIntList();
			
			 lowTempWeek = new Parser.Builder("Temperature for days of the week low  temperature")
			    		.url("https://www.accuweather.com/ru/"+cityRef+"/daily-weather-forecast/"+endRef)
			    		.elem("low")
			    		.build().toIntList();
		
			 List <Integer> rainWeekInt = new Parser.Builder("Weather for search of rain during the week")
			    		.url("https://www.accuweather.com/ru/"+cityRef+"/daily-weather-forecast/"+endRef)
			    		.elem("weather-icon icon")
			    		.div(".svg")
			    		.attr("data-src")
			    		.build().toIntList();
			 
			 // skips today, as this information is collected differently. Limits to the upcoming week days
			
			 if (isWeekForecastStartsFromToday) {
				 
			 rainWeek = rainWeekInt.stream().map(x->x>=12&&x<=15).skip(1).limit (7).collect(Collectors.toList());
			 highTempWeek = highTempWeek.stream().skip(1).limit (7).collect(Collectors.toList());
			 lowTempWeek = lowTempWeek.stream().skip(1).limit (7).collect(Collectors.toList());
			 } else {
			
				 //not skip (1) - as accuWeather already skipped the first day
			 
			 rainWeek = rainWeekInt.stream().map(x->x>=12&&x<=15).limit (7).collect(Collectors.toList());
			 highTempWeek = highTempWeek.stream().limit (7).collect(Collectors.toList());
			 lowTempWeek = lowTempWeek.stream().limit (7).collect(Collectors.toList());
			 }	
			 
		isOk = true;
		
	} catch (Exception e) {}
		
		return isOk;
	}

	private boolean isFromToday (String cityRef, String endRef) {
		 final String url = "https://www.accuweather.com/ru/"+cityRef+"/daily-weather-forecast/"+endRef;
			
		 final String elem = "module-header sub date";
		 
		 boolean isFromToday = false;
		 
		 
			try {
				Document doc = Jsoup.connect(url).get();
				List <String> list= doc.getElementsByClass(elem).eachText();
			
				String d = list.get(0).substring(0,list.get(0).indexOf("."));
				int dateAccuWeek = Integer.parseInt(d);
				isFromToday = new GregorianCalendar ().get (Calendar.DATE)==dateAccuWeek;
			
			}
				catch (Exception e) {
					System.out.println("Testing of starting date in week forecast failed");
					}

			return isFromToday;
	}
	
	private static int getCityIndex (String msg, long chatId) {
		int cityInd = getRoughCityIndex (msg);
		
		if (cityInd==0) {
		  if (User.getCity(chatId)!="") {
			  if (cityNames.contains(User.getCity(chatId)))		{
				  cityInd = getRoughCityIndex (User.getCity(chatId)); 
				  System.out.println("getDefault city");
			  }
		  }
		}
	return cityInd;	
	}
	
	private static int getRoughCityIndex (String msg) {
		
		msg = msg.toLowerCase();
		int i=0;
		for (String c: cityNames) {
			c=c.substring(0, c.length()-1).toLowerCase();
			if (msg.contains(c)) return i; 
			i++;
		}
		
		i=0;
		for (String c: cityNames) {
			c=c.substring(0, c.length()-2).toLowerCase();
			if (msg.contains(c)) return i; 
			i++;
		}
		
		return 0;
	}

		
	@Override
	public String toString () {
		String string = ""+startMsg ();

		string += rainMsg ();
		string += currentTempMsg ();
		string += nextHoursTempMsg ();
		string += wholeDayTempMsg ();
		string += weekRainMsg ();
		string +=weekTempHighMsg ();
		string +=weekTempLowMsg ();
		string +=authorRef ();
		
		System.out.println("Line216/AdvTemp: Full temp msg sent:");
		System.out.println(string);
		
	return string;	
	}
	
	public String toMorningString () {
		String string = ""+startMsg ();

		string += rainMsg ();
		string += currentTempMsg ();
		string += nextHoursTempMsg ();
		string += wholeDayTempMsg ();
		string +=authorRef ();
		
		System.out.println("Line234/AdvTemp: Morning temp msg sent:");
		System.out.println(string);
		
	return string;	
	}

	public String toShortString () {
		String string = ""+startMsg ();

		string += rainMsg ();
		string += snowMsg ();
		string += currentTempMsg ();
		string += nextHoursTempMsg ();
		string +=authorRef ();
		
		System.out.println("Line83/AdvTemp: Short temp msg sent:");
		System.out.println(string);
		
	return string;	
	}
	
	private String authorRef () {
		return "\n"+"   /по данным сайта www.accuweather.com/"+"\n";
	}
	

	private String snowMsg() {
		String string = "";
		if (!snow.contains(true))
			string += "";
		else {
			string += snowSign(snow.indexOf(true) + 1);
			string += "\n";
		}
		
		return string;
	}

	private String rainMsg () {
		String string = "";
		if (!rain.contains(true))
			string += rainSign(0);
		else
			string += rainSign(rain.indexOf(true) + 1);
		string += "\n";
		
		return string;
	}
	
	private String currentTempMsg () {
		String string="";

		string+="Сейчас" + celciusSign (tempNow);
		string+="\n";
		
		return string;
	}
	
	private String nextHoursTempMsg () {
		String string="";
		
		minToday = Collections.min (this.tempHourly.stream().limit(5).collect(Collectors.toList()));
		maxToday = Collections.max (this.tempHourly.stream().limit(5).collect(Collectors.toList()));
		
		if (minToday==maxToday) {
		
			if (maxToday==tempNow) string+="В ближайшие часы температура не изменится";
			else string+="В ближайшие часы" + celciusSign (maxToday);

		}
		
		else 
			string+="В ближайшие часы температура от " + minToday + " до " + maxToday + " градусов";
		
		return string;
	}
	
	
	private String wholeDayTempMsg () {
		String string="";
		
		if (tempHourly.size()>5) {
			
			int min = Collections.min (this.tempHourly.stream().limit(5).collect(Collectors.toList()));
			int max = Collections.max (this.tempHourly.stream().limit(5).collect(Collectors.toList()));
			int minD = Collections.min(this.tempHourly);
			int maxD = Collections.max(this.tempHourly);
			
			if (minD!=maxD) {
				
				if (min==minD && max==maxD) string+="\n" + "В течение дня температура не изменится";
				else string+="\n" + "В течение дня температура от " + minD + " до " + maxD + " градусов";	
				
			}
		}
		
		return string;
	}
	
	private String weekRainMsg () {
		
		 if (rainWeek.stream ().anyMatch(x->x==true)) {
			
				
			String message = "\n";
			message+="Дождь ожидается в ";
			
			message = IntStream.range(0, 7)
			  .filter(x->rainWeek.get (x)==true)
			           .mapToObj (x->getDayOfWeek((x+thisDayOfWeek)))			
			               .reduce (message, (remainer, increm) -> remainer+increm+", ");
			
			if (message.lastIndexOf(",")>0) {
					message = message.substring(0,message.lastIndexOf(","));
			}
			return message + ".\n";
			
		} else {
			return "\n"+"Дождь на неделе не предвидится." + "\n";
		}
		
	}

	
	private String weekTempHighMsg () {
		if (highTempWeek.stream ().anyMatch(x->x>maxToday)) {
	
			String message = "\n";
			
			int median = getMedian (highTempWeek.stream ().filter (x->x>maxToday).collect(Collectors.toList()));
			
			if (median == 0) {
			
			  message+="Повышение температуры ожидается в ";
			
			  message = IntStream.range(0, 7)
			      .filter(x->highTempWeek.get (x)>maxToday)
			           .mapToObj (x->getDayOfWeek((x+thisDayOfWeek)) + " (" + highTempWeek.get (x) + new String(Character.toChars(0x00B0))+")")			
			               .reduce (message, (remainer, increm) -> remainer+increm+", ");
			
			  if (message.lastIndexOf(",")>0) {
					message = message.substring(0,message.lastIndexOf(","));
			}
			} else {
				
				message += "На неделе температура повысится до " + median + new String(Character.toChars(0x00B1)) + "2"+ new String(Character.toChars(0x00B0)) + ".\n";
				
				if (highTempWeek.stream ().anyMatch(x->x>median+2)) {
				  message+="Более высокая температура ожидается в ";
				
				  message = IntStream.range(0, 7)
					      .filter(x->highTempWeek.get (x)>median+2)
					           .mapToObj (x->getDayOfWeek((x+thisDayOfWeek)) + " (" + highTempWeek.get (x) + new String(Character.toChars(0x00B0))+")")			
					               .reduce (message, (remainer, increm) -> remainer+increm+", ");
				  if (message.lastIndexOf(",")>0) {
						message = message.substring(0,message.lastIndexOf(","));
				}
				  message = message+".";
				}
			}
			
			
			return message;
			
		} 
			return "Более высокая температура на неделе не предвидится." + "\n";
		
	}
	
	private String weekTempLowMsg () {
		
		if (lowTempWeek.stream ().anyMatch(x->x<minToday)) {
	
			String message = "\n";
			
			int median = getMedian (lowTempWeek.stream ().filter (x->x<minToday).collect(Collectors.toList()));
			
			if (median == 0) {
			
			  message+="Понижение температуры ожидается в ";
			
			  message = IntStream.range(0, 7)
			      .filter(x->lowTempWeek.get (x)<minToday)
			           .mapToObj (x->getDayOfWeek((x+thisDayOfWeek)) + " (" + lowTempWeek.get (x) + new String(Character.toChars(0x00B0))+")")			
			               .reduce (message, (remainer, increm) -> remainer+increm+", ");
			
			  if (message.lastIndexOf(",")>0) {
					message = message.substring(0,message.lastIndexOf(","));
			}
			} else {
				
				message += "На неделе ночью температура понизится до " + median + new String(Character.toChars(0x00B1)) + "2"+ new String(Character.toChars(0x00B0)) + ".\n";
				
				if (lowTempWeek.stream ().anyMatch(x->x<median-2)) {
				  message+="Более низкая температура ожидается в ";
				
				  message = IntStream.range(0, 7)
					      .filter(x->lowTempWeek.get (x)<median-2)
					           .mapToObj (x->getDayOfWeek((x+thisDayOfWeek)) + " (" + lowTempWeek.get (x) + new String(Character.toChars(0x00B0))+")")			
					               .reduce (message, (remainer, increm) -> remainer+increm+", ");
				  if (message.lastIndexOf(",")>0) {
						message = message.substring(0,message.lastIndexOf(","));
				}
				  message = message+".";
				}
			}
			
			
			return message;
			
		} 
			return "Более низкая температура на неделе не предвидится." + "\n";
		
	}
	
	
	private int getMedian (List <Integer> list) {
		//gets the temp where +/-1 grad locates the most days.
		
		if (list == null) return 0;
		if (list.size()<3) return 0;
		
		int maxCount =0;
		int median = 0;
		Set <Integer> countSet = list.stream().collect(Collectors.toSet());
		
		for (int temp : countSet) {
			int cnt = (int) list.stream().mapToInt(x->x).filter (x->x>=temp-2 && x<=temp+2).count();
			if (maxCount<cnt) {
				median = temp; 
				maxCount =  cnt;
			}
		}
		
//		System.out.println(" -> maxCount " + maxCount);
//		System.out.println("median = " + median);
//		System.out.println(list.size()/2);
		if (maxCount>=list.size()/2) return median;
		
		return 0;
		
	}
	
	private String rainSign (int hours) {
		if (hours == 0) return "Дождя нет";
		if (hours == 1) return "Дождь в ближайший час";
		if (hours<=4) return "Дождь в ближайшие " + hours +" часа";
		return "Дождь в ближайшие " + hours +" часов";
	}
	
	private String snowSign(int hours) {
		if (hours == 0) return "Снега нет";
		if (hours == 1) return "Снег в ближайший час";
		if (hours<=4) return "Снег в ближайшие " + hours +" часа";
		return "Снег в ближайшие " + hours +" часов";
	}

	
	private String celciusSign (int gradus) {
		if (Math.abs(gradus)%10==1 && Math.abs(gradus)!=11) return " " +gradus + " градус";
		if (Math.abs(gradus)%10>1 && Math.abs(gradus)%10<=4 && Math.abs(gradus)!=12 && Math.abs(gradus)!=13 && Math.abs(gradus)!=14) 
			return " " + gradus + " градуса";
		return " " + gradus + " градусов";
	}
	
	private String startMsg () {
		return "Погода в " + city+": ";
	}
	
	public int getId () {
		return this.id;
	}
	
	private static List <String> getRefList () {
		return Arrays.asList(
				"ru/moscow/294021",
				"ru/moscow/294021", "ru/kaluga/293006", "ee/tallinn/127964", "ru/saint-petersburg/295212", "ru/tver/296079",
				"ru/irkutsk/292712", "ee/tartu/131136", "ee/parnu/130058", "ru/yekaterinburg/295863", "ru/lipetsk/293886", "ru/voronezh/296543",
				"ru/rostov-on-don/295146", "ru/krasnodar/293686", "ru/gyelyendzhik/288685", "ru/sochi/293687", "ru/anapa/288689",
				"by/minsk/28580", "ee/johv", "ee/narva/2311089", "ru/tula/295982", "ru/vladimir/296263", 
				"ru/nizhny-novgorod/294199","ru/nizhny-novgorod/294199","ru/nizhny-novgorod/294199","ru/veliky-novgorod/294457",
				 "ua/kyiv/324505", "ru/nizhny-tagil/290714", "tr/istanbul/318251", "ru/smolensk/295475", "ru/pskov/295031",
				"ru/yaroslavl/296629", "ge/tbilisi/171705", "am/yerevan/16890", "ru/perm/294922", "ru/chelyabinsk/292332",
				"ir/tehran/210841", "jp/tokyo/226396", "ua/simferopol/322464", "ua/sevastopol/6120", "hu/budapest/187423",
				"cz/prague/125594", "at/vienna/31868", "bg/varna/51536", "ro/bucharest/287430", "bg/sofia/51097",
				"me/podgorica/300169", "gb/london/328328", "de/berlin/10178", "de/munich/80331", "fr/paris/623", "fi/helsinki/133328",
				"se/stockholm/314929", "no/oslo/254946", "dk/copenhagen/123094", "lt/vilnius/231459", "lv/riga/225780",
				"ru/maykop/291658", "ru/armavir/293688", "ru/stavropol/295608", "ru/tambov/295902", "ru/volgograd/296363",
				"ru/saratov/295382", "us/philadelphia/19102", "ua/yalta/322329", "ua/alushta/322322", "ua/feodosiia/322325",
				"ua/sevastopol/6120", "ua/yevpatoriia/322474", "tr/ankara/316938", "ua/sudak/322321", "ua/dzhankoi/322324",
				"ua/chornomorske/1218365", "ua/kerch/322468", "ru/taman/293663", "tr/antalya/316939",
				"it/rome/213490", "it/naples/212466", "it/milan/214046", "it/turin/214753", "it/venice/216711",
				"gr/athens/182536", "cy/nicosia/124697", "cy/limassol/124034", "ru/petrozavodsk/293141",
				"us/new-york/10007", "us/washington/20006", "us/baltimore/21202", "ru/izhevsk/296181", "ru/kazan/295954",
				"ru/novotroitsk/289747", "ru/orenburg/294535", "ru/lipetsk/293886", "ru/chelyabinsk/292332", "ru/ufa/292177",
				"ru/kirov/288509", "ru/cherepovets/296478", "ru/petropavlovsk-kamchatsky", "ru/tikhvin/288958",
				"ru/samara/290396", "ru/belgorod/292195", "ru/staryy-oskol/292196", "ru/astrakhan/292110", 
				"ru/arkhangelsk/292056", "bg/burgas/47424");
	}
	
	private static List <String> getCityNamesList () {
		return  Arrays.asList("_Москве","Москве", "Калуге", "Таллине", "Санкт-Петербурге", "Твери",
				"Иркутске", "Тарту", "Пярну", "Екатеринбурге", "Липецке", "Воронеже", "Ростове", "Краснодаре", "Геленджике",
				"Сочи", "Анапе", "Минске", "Йихви", "Нарве", "Туле", "Владимире", 
				"Н.Новгороде", "Нижнем Новгороде","Нижний Новгород","Новгороде", "Киеве",
				"Тагиле", "Стамбуле", "Смоленске", "Пскове", "Ярославле", "Тбиллиси", "Ереване", "Перми", "Челябинске",
				"Тегеране", "Токио", "Симферополе", "Севастополе", "Будапеште", "Праге", "Вене", "Варне", "Бухаресте",
				"Софии", "Подгорице", "Лондоне", "Берлине", "Мюнхене", "Париже", "Хельсинки", "Стокгольме", "Осло","Коппенгагине",
				"Вильнюсе", "Риге", "Майкопе", "Армавире", "Ставраполе", "Тамбове", "Волгограде", "Саратове", "Филадельфии", "Ялте", 
				"Алуште", "Феодосии","Севастополе", "Евпатории", "Анкаре", "Судаке", "Джанкое", "Черноморском",
				"Керчи", "Тамани", "Анталье", "Риме", "Неаполе", "Милане", "Турине", "Венеции", "Афинах", "Никосии",
				"Лимассоле", "Петрозаводске", "Нью-Йорке", "Вашингтоне", "Балтиморе", "Ижевске", "Казани",
				"Новотроицке", "Оренбурге", "Липецке", "Челябинске", "Уфе", "Кирове","Череповце", "Петропавловске-Камчатском",
				"Тихвине", "Самаре", "Белгороде", "Старом Осколе", "Астрахани","Архангельске", "Бургасе");
	}
	
	private static String getDayOfWeek (int day)  {
		try {
		if (day<0) throw new Exception("Day of the week is wrongly defined");
		}
		catch (Exception e) {System.out.println(e);}
		
		day = day%7;
		
		List <String> days = Arrays.asList("понедельник","вторник", "среду", "четверг", "пятницу", "субботу", "воскресенье");
		return days.get(day);
	}
}


