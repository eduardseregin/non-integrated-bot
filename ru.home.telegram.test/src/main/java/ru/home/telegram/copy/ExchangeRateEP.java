package ru.home.telegram.copy;


import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ExchangeRateEP {

	private static ExchangeRateEP estiiPankRates;
	
	private Map <String, Double> exchangeRates;
	private String establishedDate;
	private Date nextUpdate;
	
	
	public static String getEurGbpUsdMsg () {
		String msg = "";
		ExchangeRateEP rates = ExchangeRateEP.instance();
		
		if (rates.exchangeRates!=null) {
		
		msg+="EETI PANK (eestipank.ee) has established rates at " + rates.establishedDate + "\n";
		msg+="  EUR - " + rates.exchangeRates.get("RUB") + " RUB"+ "\n";
		msg+="  EUR - " + rates.exchangeRates.get("GBP") + " GBP"+ "\n";
		msg+="  EUR - " + rates.exchangeRates.get("USD") + " USD"+ "\n";
		} else msg = "Information is not available";
		return msg;
	}
	
	public static String getSpecificRateToUpperCase (String msg) {
		
		if (msg.equals("RUB в Эстонии  ₽")) msg = "RUB";
		if (msg.equals("USD в России и Эстонии  💲")) msg = "USD";
		
		int ind=-1;
		
		
		if (index (msg.toLowerCase(), getShortNames())>=0) {
			ind = index (msg.toLowerCase(), getShortNames());
		}
		
		if (index (msg.toLowerCase(), getFullNames())>=0) {
			ind = index (msg.toLowerCase(), getFullNames());
		}

		if (index (msg.toUpperCase(), getCurrCodes())>=0) {
			ind = index (msg.toUpperCase(), getCurrCodes());
		}
		
		if (ind==-1) return "";
		
		ExchangeRateEP rates = ExchangeRateEP.instance();
		
		String str = "";
		
		if (rates.exchangeRates!=null) {
		str+="Eesti Pank (eestipank.ee): from "+ rates.establishedDate +": 1 EUR = ";
		str+=rates.exchangeRates.get(getCurrCodes().get (ind)) 
				+ "  ";
		str+=getCurrCodes().get (ind) + " (" +getFullNames().get(ind) +")";
		
		} else str = "Information is not available";
		return str;
	}
	
	private static int index (String msg, List <String> names) {
		int index = 0;
		for (String n: names) {
			if (msg.contains(n)) return index; 
					index++;
		}
		
		return -1;
	}
	
	private static ExchangeRateEP instance () {
		if (estiiPankRates != null && new Date ().compareTo(estiiPankRates.nextUpdate)<0) {
		  return estiiPankRates;
		}
		 return new ExchangeRateEP ();
	}
	
	private ExchangeRateEP () {
		
		GregorianCalendar cal = (GregorianCalendar) GregorianCalendar.getInstance();
		cal.add(Calendar.HOUR_OF_DAY, 1);
		this.nextUpdate = cal.getTime();
		
//		// gets inf from cbr.ru site
		   List <String> lines = new Parser.Builder("Exchange Rates")
		    		.url("https://www.eestipank.ee/en/exchange-rates")
		    		.elem("view-content")
		    		.build().getElements();
		   

if (lines!=null) {
		   
//		   // prepares the line and Map
		    String rates = lines.get(0) + " " + lines.get(1);
		    
		    while (rates.contains(")") && rates.contains(")")) {
		    	rates = rates.substring(0, rates.indexOf("(")) + rates.substring(rates.indexOf(")")+2);
		    	
		    	
		    }
				  
			   
		    
		    exchangeRates = new HashMap <>();
		    
//		    // fills the map with data
		    while (posEnglCapitalLetterFirst (rates)!=0) {

		    	String individualLine = rates.substring(0, posEnglCapitalLetterFirst (rates)-1);
		    	
		    	
		    	exchangeRates.put(individualLine.substring(0, 3), extractRate(individualLine));
		    	rates=rates.substring(posEnglCapitalLetterFirst (rates));

		    	
		    		   }
//		   // adds-up the last line of the inf 
//		    
		    if (rates.length()!=0) exchangeRates.put(rates.substring(0, 3), extractRate(rates));
		  
		   // DONE    
}
		    this .establishedDate=getEstablishedDate();
		    
		    estiiPankRates = this;
	}
	
	
	private int posEnglCapitalLetterFirst(String r) {

		int pos = r.length() - 1;

		for (int i = 3; i < r.length(); i++) {
			if (pos == r.length() - 1) {
				for (int y = 65; y < 91; y++) {
					if (r.charAt(i) == ((char) (y))) {
						pos = i;
						break;
					}
				}
			}
		}

		if (pos != r.length() - 1)
			return pos;

		return 0;
	}
    
	private double extractRate(String s) {

		String rate = s.substring(s.lastIndexOf(" ")+1);
		rate = rate.replace(',', '.');

		
		try {
			int increasedDouble = (int) (Math.round(Double.parseDouble(rate) * 100 ));
			return (double) increasedDouble / 100;

		} catch (Exception e) {
			return 0;
		}
	}

	private static String getEstablishedDate () {
		
	    List <String> date = new Parser.Builder("for Date")
	    		.url("https://www.eestipank.ee/en/exchange-rates")
	    		.elem("form-control datepicker")
	    		.attr("value")
	    		.build().getElements();
	    
	    String dt = "";
	    if (date!=null) return date.get(0); 
	    
	    return dt;
	}
	
	public static List <String> getCurrCodes () {
		return Arrays.asList("AUD", "BGN", "BRL", "CAD", "CHF", "CNY", "CZK", "DKK", "GBP", "HKD", 
				"HRK", "HUF", "IDR", "ILS", "INR", "ISK", "JPY", "KRW", "MXN", "MYR", "NOK", "NZD", 
				"PHP", "PLN", "RON", "RUB", "SEK", "SGD", "THB", "TRY", "USD", "ZAR" );
	}
	
	public static List <String> getShortNames () {
		return Arrays.asList("australia", "lev", "real", "canada ", "franc", "yuan", "koruna", 
				"denmark", "pound", "hong kong", "kuna", "forint", "rupiah", "shekel", "rupee",
				"icelandia", "yen", "won", "mexica", "ringgit", "norwegia ", "new zealand", 
				"philippine", "zloty", "leu", "rouble", "sweden", "singapore", "baht", "lira",
				"dollar", "rand"  );
	}
	
	public static List <String> getFullNames () {
		return Arrays.asList("Australian dollar", "Bulgarian lev", "Brasilian real", "Canadian dollar", 
				"Swiss franc", "Chinese yuan renminbi", "Czech koruna", "Danish krone", "Pound sterling", 
				"Hong Kong dollar", "Croatian kuna", "Hungarian forint", "Indonesian rupiah", 
				"Israeli shekel", "Indian rupee", "Icelandic krona", "Japanese yen", "South Korean won", 
				"Mexican peso", "Malaysian ringgit", "Norwegian krone", "New Zealand dollar", 
				"Philippine peso", "Polish zloty", "New Romanian leu", "Russian rouble", "Swedish krona", 
				"Singapore dollar", "Thai baht", "Turkish lira", "US dollar", "South African rand" );
	}
	
}
