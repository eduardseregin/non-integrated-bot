package ru.home.telegram.copy;

public interface Credentials {

	static GoogleCredentials getCredentials (int user) {
		if (user==1) return new GoogleCredentials (getMyGoogleToken (),getMyGoogleCredentials ());
		if (user==2) return new GoogleCredentials (getAlenaGoogleToken (),getAlenaGoogleCredentials ());
		return null;
	}
	
	static String getAlenaGoogleToken () {
		return "tokens";
	};
	
	static String getAlenaGoogleCredentials () {
		return "/client_secret_3191403910-44l0hcln1op49pnhiqcglo265gduc80o.apps.googleusercontent.com.json";
	};
	
	static String getMyGoogleToken () {
		return "tokens_1";
	};
	
	static String getMyGoogleCredentials () {
		return "/client_secret_777355370372-lviqios5jg84er6c59gg75pe35deonpf.apps.googleusercontent.com.json";
	};
	
	static String getTelegramCred () {
		return "1719714699:AAGk351or7a3AqhJ8yxD8KgdFYhhyeq06MQ";
	}
	
}

final class GoogleCredentials {
	private final String token;
	private final String credentials;
	
	protected GoogleCredentials(String token, String credentials) {
		super();
		this.token = token;
		this.credentials = credentials;
	}
	
	public String getToken () {return this.token;}
	public String getCredentials () {return this.credentials;}
	
}