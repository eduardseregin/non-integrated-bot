package ru.home.telegram.copy.files;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import ru.home.telegram.copy.radio.RadioBox;
import ru.home.telegram.test.news.NewsService;

public abstract class ChatIdFromFile implements Serializable {

	private static final long serialVersionUID = 1L;
	private static final String filePath = "C:\\Temp\\serializedObject.txt";

	private static Date nextSave;
	private static Date nextNulify;

	// MAIN FUNCTION
	public static List<Long> updateFile(List<Long> chatIds)  {

		// temporaly here, needs to adjust
		NewsService.checkRefresher();
		
		checkDates ();
		
		Date now = new Date ();
		long updateId=0;
		
		if (chatIds == null) {
			chatIds = new ArrayList<>();
			chatIds = readListFromFile();
		}
		
		if (chatIds.size()>0) updateId = chatIds.get(0);


		if (now.compareTo(nextNulify )>0 && nextNulify!=null) {
		
		chatIds = new ArrayList<>();
		if (updateId!=0) chatIds.add(updateId);
		System.out.println("Nulified");
		
		 Runnable task = () -> RadioBox.updateChart();
		 Thread thread = new Thread(task);
	        thread.start();
		
		nextNulify=null;
		}

		
		if (now.compareTo(nextSave)>0) {
		saveThisListToFile(chatIds);
		nextSave=null;
		}
		
		return chatIds;
	};

	private static boolean checkDates () {
		
		if (nextSave!=null && nextNulify!=null) return false;
		
		GregorianCalendar today = new GregorianCalendar ();
		
		int mm = today.get (Calendar.MINUTE);
		int hh = today.get (Calendar.HOUR_OF_DAY);
		int yy = today.get (Calendar.YEAR);
		int MM = today.get (Calendar.MONTH);
		int dd = today.get (Calendar.DATE);
		
		if (nextSave==null) {
			
			if (mm<45) nextSave= new GregorianCalendar (yy,MM,dd,hh,59,0).getTime(); 
			else {
				GregorianCalendar gc = new GregorianCalendar (yy,MM,dd,hh,59,0);
				gc.add(Calendar.HOUR, 1);
				nextSave= gc.getTime(); 
				}
		}

		if (nextNulify==null) {
			
			if (hh<6) nextNulify = new GregorianCalendar (yy,MM,dd,6,40,0).getTime(); 
			else {
				GregorianCalendar gc = new GregorianCalendar (yy,MM,dd,6,40,0);
				gc.add(Calendar.DATE, 1);
				nextNulify = gc.getTime(); 
				}
		}
			
		return true;
		
	}
	
	private static boolean saveThisListToFile(List<Long> chatIds) {

		boolean success = false;
		
		try {
		
		try (FileOutputStream fos = new FileOutputStream(filePath);
				ObjectOutputStream oos = new ObjectOutputStream(fos)) {

			if (chatIds.size() == 0) {
				oos.writeLong(1111111);
				System.out.println("In ChatIdFromFile:50 - next line writen- : " + 111111111);
			}

			for (long l : chatIds) {
				oos.writeLong(l);

				System.out.println("In ChatIdFromFile:53 - next line writen- : " + l);
				success = true;

			}
			oos.close();
		}
		} catch (Exception e) {}

		return success;
	}

	private static List<Long> readListFromFile() {

		List<Long> list = new ArrayList<>();

		try {
		
		File f = new File(filePath);
		if (f.exists()) {

			DateFormat simpleDateFormat = new SimpleDateFormat("dd");
			System.out.println(
					"In ChatIdFromFile:88 - file was last modified " + simpleDateFormat.format(f.lastModified()));

			try (FileInputStream istream = new FileInputStream(f)) {
				try (ObjectInputStream ois = new ObjectInputStream(istream)) {
					while (ois.available() > 0) {
						list.add(ois.readLong());
					}
					ois.close();
				}
				istream.close();
			}
		}

		for (long l : list) {
			System.out.println("In ChatIdFromFile:102 - read from file: " + l);
		}
		
		} catch (Exception e) {}
		return list;
	}

}
